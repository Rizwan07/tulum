//
//  GalleryVideoViewController.m
//  AhauTravel
//
//  Created by Rizwan on 6/17/13.
//  Copyright (c) 2013 com. All rights reserved.
//

#import "GalleryVideoViewController.h"
#import <MediaPlayer/MediaPlayer.h>
#import <AVFoundation/AVFoundation.h>
#import <MessageUI/MessageUI.h>
#import <Social/Social.h>
#import <Accounts/Accounts.h>

@interface GalleryVideoViewController () {
    
    float _currentPlayBackTime;
    BOOL isVolumeOn;
    BOOL isPlaying;
    IBOutlet UIButton *btnPlayPause;
    IBOutlet UISlider *slider;
    NSTimer *updateTimer;
    
    int totalMinutes;
    
    ACAccountStore *_accountStore;
    IBOutlet UIActivityIndicatorView *indicatorView;
}

@property (strong, nonatomic) MPMoviePlayerController *playerController;
@property (strong, nonatomic) AVPlayer *avPlayer;

@property (strong, nonatomic) IBOutlet UIView *viewVideo;
@property (strong, nonatomic) IBOutlet UIView *viewInputTItle;
@property (strong, nonatomic) IBOutlet UIView *viewShare;
@property (strong, nonatomic) IBOutlet UITextField *txtFieldRename;
@property (strong, nonatomic) IBOutlet UILabel *lblTime;
@property (strong, nonatomic) IBOutlet UILabel *lblHeader;

- (IBAction)backBtnPressed:(id)sender;
- (IBAction)playBtnPressed:(id)sender;
- (IBAction)volumeBtnPressed:(id)sender;
- (IBAction)commentBtnPressed:(id)sender;
- (IBAction)fullScreenBtnPressed:(id)sender;

- (IBAction)twitterBtnPressed:(id)sender;
- (IBAction)fbBtnPressed:(id)sender;
- (IBAction)emailBtnPressed:(id)sender;
- (IBAction)saveBtnPressed:(id)sender;

- (IBAction)refereshBtnPressed:(id)sender;
- (IBAction)renameBtnPressed:(id)sender;

- (IBAction)cameraBtnPressed:(id)sender;

- (IBAction)sliderSlided:(id)sender;

@end

@interface GalleryVideoViewController () <MFMailComposeViewControllerDelegate>
@end

@implementation GalleryVideoViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    [slider setMaximumTrackImage:[UIImage imageNamed:@"outer-volume-bar.png"] forState:UIControlStateNormal];
    [slider setMinimumTrackImage:[UIImage imageNamed:@"inner-volume-bar.png"] forState:UIControlStateNormal];
    [slider setThumbImage:[UIImage imageNamed:@"thumb-volume-control.png"] forState:UIControlStateNormal];
    
    
    _accountStore = [[ACAccountStore alloc] init];

    [self setMediaView];
	// Do any additional setup after loading the view.
}

-(void)viewDidAppear:(BOOL)animated {
    [super viewDidAppear:animated];
    
    [_playerController setControlStyle:MPMovieControlStyleNone];

}

- (void)setMediaView {
    
    isVolumeOn = YES;
    isPlaying = NO;
    
    [slider setMinimumValue:0.0];
    [slider setValue:0.0];
    
    [_lblHeader setText:[NSString stringWithFormat:@"Video_%02d", _selectedIndex+1]];
    
    //NSString *strVideoPath = [[NSBundle mainBundle] pathForResource:@"demo" ofType:@"mp4"];
    NSString *strVideoPath = [[[[Singleton sharedSingleton] arrData] objectAtIndex:_selectedIndex] name];
    NSURL *url = [NSURL fileURLWithPath:strVideoPath];
    _playerController = [[MPMoviePlayerController alloc] initWithContentURL:url];
    
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(movieFinished:) name:MPMoviePlayerPlaybackDidFinishNotification object:_playerController];
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(MPMoviePlayerLoadStateDidChange:)
                                                 name:MPMoviePlayerLoadStateDidChangeNotification
                                               object:nil];

    //[_playerController.view setFrame:CGRectMake(0, 0, 290, _viewVideo.frame.size.height-30)];
    
    if ([[UIDevice currentDevice] userInterfaceIdiom] == UIUserInterfaceIdiomPhone) {
        
        if ([[UIScreen mainScreen] bounds].size.height == 480) {
            [_playerController.view setFrame:CGRectMake(0, 0, 290, 268-30)];
        } else {
            [_playerController.view setFrame:CGRectMake(0, 0, 290, 356-30)];
        }
    }
    else {
        [_playerController.view setFrame:CGRectMake(0, 0, 728, 730-60)];
    }
    
    //[_playerController.view setFrame:CGRectMake(0, 0, 290, 100)];
    [_playerController setControlStyle:MPMovieControlStyleNone];
    //[_playerController prepareToPlay];
    _currentPlayBackTime = _playerController.currentPlaybackTime;
    [_playerController currentPlaybackTime];
    [_viewVideo addSubview:_playerController.view];
    
    updateTimer = [NSTimer scheduledTimerWithTimeInterval:.1 target:self selector:@selector(updateCurrentTime) userInfo:_playerController repeats:YES];
    
    totalMinutes = [_playerController duration]/60;
    
    NSLog(@"min %f, max %f, movie duration %f", slider.minimumValue, slider.maximumValue, _playerController.duration);
}

- (void)MPMoviePlayerLoadStateDidChange:(NSNotification *)notification
{
    if ((_playerController.loadState & MPMovieLoadStatePlaythroughOK) == MPMovieLoadStatePlaythroughOK)
    {
        NSLog(@"content play length is %g seconds", _playerController.duration);
        [slider setMaximumValue:(float)[_playerController  duration]];
    }
}

- (void)updateCurrentTime {
    
	slider.value = _playerController.currentPlaybackTime;
    [slider setNeedsDisplay];
    
    //NSLog(@"current time %f", slider.value);
    
    int currentMinutes = [_playerController currentPlaybackTime]/60;
    
    [_lblTime setText:[NSString stringWithFormat:@"%02d:%02.f / %02d:%02.f", currentMinutes, ([_playerController currentPlaybackTime]-(currentMinutes*60)), totalMinutes, ([_playerController duration]- (totalMinutes*60))]];
}


- (IBAction)sliderSlided:(id)sender {
    
    [_playerController setCurrentPlaybackTime:slider.value];
    
    //_audioPlayer set
}

-(void)updateCurrentTimeForPlayer:(MPMoviePlayerController *)p
{
	//currentTime.text = [NSString stringWithFormat:@"%d:%02d", (int)p.currentTime / 60, (int)p.currentTime % 60, nil];
    
    NSLog(@"current time %f", p.currentPlaybackTime);
    
	slider.value = p.currentPlaybackTime;
}

- (void)setMediaPlayerLayerView {
    
    NSString *strUrl = [[NSBundle mainBundle] pathForResource:@"demo" ofType:@"mp4"];
    NSURL *url = [NSURL fileURLWithPath:strUrl];
    self.avPlayer = [AVPlayer playerWithURL:url];
    
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(movieFinished:) name:MPMoviePlayerPlaybackDidFinishNotification object:_avPlayer];
    
    AVPlayerLayer *layer = [AVPlayerLayer playerLayerWithPlayer:_avPlayer];
    
    [layer setFrame:CGRectMake(0, 0, 320, 198)];
    [_avPlayer setActionAtItemEnd:AVPlayerActionAtItemEndNone];
    [_viewVideo.layer addSublayer:layer];
    
    [_avPlayer play];
    
}

- (void)movieFinished:(MPMoviePlayerController *)playerContorller {
    
    [[NSNotificationCenter defaultCenter] removeObserver:self name:MPMoviePlayerPlaybackDidFinishNotification object:playerContorller];
    NSLog(@"End Video");
    isPlaying = NO;
    [slider setValue:0.0];
    [updateTimer invalidate];
    [btnPlayPause setSelected:NO];
}

- (UIStatusBarStyle)preferredStatusBarStyle
{
    return UIStatusBarStyleLightContent;
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (IBAction)backBtnPressed:(id)sender {
    [self.navigationController popViewControllerAnimated:YES];
}

- (IBAction)playBtnPressed:(id)sender {
    
    if (!isPlaying) {
        [btnPlayPause setSelected:YES];
        updateTimer = [NSTimer scheduledTimerWithTimeInterval:.1 target:self selector:@selector(updateCurrentTime) userInfo:_playerController repeats:YES];
        slider.value = _playerController.currentPlaybackTime;
        [_playerController play];
    }
    else {
        [btnPlayPause setSelected:NO];
        [_playerController pause];
    }
    isPlaying = !isPlaying;
}

- (IBAction)volumeBtnPressed:(id)sender {
    
    if (isVolumeOn) {
        [(UIButton *)sender setSelected:YES];
        [[MPMusicPlayerController applicationMusicPlayer] setVolume:0.0];
    }
    else {
        [(UIButton *)sender setSelected:NO];
        [[MPMusicPlayerController applicationMusicPlayer] setVolume:1.0];
    }
    isVolumeOn = !isVolumeOn;
}

- (IBAction)commentBtnPressed:(id)sender {
}

- (IBAction)fullScreenBtnPressed:(id)sender {
    
    //[_playerController.view setFrame:self.view.frame];
    [_playerController setControlStyle:MPMovieControlStyleDefault];
    [_playerController setFullscreen:YES animated:YES];
}


- (IBAction)twitterBtnPressed:(id)sender {
    
    if ([SLComposeViewController isAvailableForServiceType:SLServiceTypeTwitter]) {
        
    }
    else {
        SIAlertView *alertView = [[SIAlertView alloc] initWithTitle:@"Sorry" andMessage:@"You can't send a tweet right now, make sure your device has an internet connection and have at least one twitter account setup."];
        [alertView addButtonWithTitle:@"OK"
                                 type:SIAlertViewButtonTypeDefault
                              handler:^(SIAlertView *alertView) {
                              }];
        
        [alertView show];
    }
    
}

- (void)postVideo:(NSData *)videoData withStatus:(NSString *)msg
{
    ACAccountType *twitterType = [_accountStore accountTypeWithAccountTypeIdentifier:ACAccountTypeIdentifierTwitter];
    
    SLRequestHandler requestHandler =
    ^(NSData *responseData, NSHTTPURLResponse *urlResponse, NSError *error) {
        if (responseData) {
            NSInteger statusCode = urlResponse.statusCode;
            
            NSLog(@"response data %@", [[NSString alloc] initWithData:responseData encoding:NSUTF8StringEncoding]);
            
            if (statusCode >= 200 && statusCode < 300) {
                NSDictionary *postResponseData =
                [NSJSONSerialization JSONObjectWithData:responseData
                                                options:NSJSONReadingMutableContainers
                                                  error:NULL];
                NSLog(@"[SUCCESS!] Created Tweet with ID: %@", postResponseData[@"id_str"]);
            }
            else {
                NSLog(@"[ERROR] Server responded: status code %d %@", statusCode,
                      [NSHTTPURLResponse localizedStringForStatusCode:statusCode]);
            }
        }
        else {
            NSLog(@"[ERROR] An error occurred while posting: %@", [error localizedDescription]);
        }
    };
    
    ACAccountStoreRequestAccessCompletionHandler accountStoreHandler =
    ^(BOOL granted, NSError *error) {
        if (granted) {
            NSArray *accounts = [_accountStore accountsWithAccountType:twitterType];
            NSURL *url = [NSURL URLWithString:@"https://api.twitter.com"
                          @"/1.1/statuses/update_with_media.json"];
            NSDictionary *params = @{@"status" : msg};
            SLRequest *request = [SLRequest requestForServiceType:SLServiceTypeTwitter
                                                    requestMethod:SLRequestMethodPOST
                                                              URL:url
                                                       parameters:params];
//            NSData *imageData = UIImageJPEGRepresentation(image, 1.f);
//            [request addMultipartData:imageData
//                             withName:@"media[]"
//                                 type:@"image/jpeg"
//                             filename:@"image.jpg"];
            
            [request addMultipartData:videoData withName:@"media[]" type:@"image/png" filename:@"AhauTravel.png"];
            
            [request setAccount:[accounts lastObject]];
            [request performRequestWithHandler:requestHandler];
        }
        else {
            NSLog(@"[ERROR] An error occurred while asking for user authorization: %@",
                  [error localizedDescription]);
        }
    };
    
    [_accountStore requestAccessToAccountsWithType:twitterType
                                               options:NULL
                                            completion:accountStoreHandler];
}

- (IBAction)fbBtnPressed:(id)sender {
    
    
    if ([SLComposeViewController isAvailableForServiceType:SLServiceTypeFacebook]) {
        
        [indicatorView startAnimating];

        // Request access to the Twitter accounts
        _accountStore = [[ACAccountStore alloc] init];
        ACAccountType *accountType = [_accountStore accountTypeWithAccountTypeIdentifier:ACAccountTypeIdentifierFacebook];
        
        NSDictionary *options = @{
                                  ACFacebookAppIdKey: @"218220394994244",
                                  ACFacebookPermissionsKey: @[@"publish_stream", ],
                                  ACFacebookAudienceKey: ACFacebookAudienceFriends
                                  };
        
        [_accountStore requestAccessToAccountsWithType:accountType options:options completion:^(BOOL granted, NSError *error){
            if (granted) {
                NSArray *accounts = [_accountStore accountsWithAccountType:accountType];
                // Check if the users has setup at least one Twitter account
                if (accounts.count > 0)
                {
                    
                    
                    ACAccount *fbAccount = [accounts objectAtIndex:0];
        
                    NSURL *videourl = [NSURL URLWithString:@"https://graph.facebook.com/me/videos"];
                    
                    NSString *strVideoPath = [[[[Singleton sharedSingleton] arrData] objectAtIndex:_selectedIndex] name];
                    //NSString *strVideoPath = [[NSBundle mainBundle] pathForResource:@"demo" ofType:@"mp4"];
                    NSURL *videoPathURL = [NSURL fileURLWithPath:strVideoPath isDirectory:NO];
                    NSData *videoData = [NSData dataWithContentsOfFile:strVideoPath];
                    
                    NSDictionary *params = @{
                                             @"title": @"Tulum",
                                             @"description": @"Tulum captured video"
                                             };
                    
                    SLRequest *uploadRequest = [SLRequest requestForServiceType:SLServiceTypeFacebook
                                                                  requestMethod:SLRequestMethodPOST
                                                                            URL:videourl
                                                                     parameters:params];
                    [uploadRequest addMultipartData:videoData
                                           withName:@"source"
                                               type:@"video/quicktime"
                                           filename:[videoPathURL absoluteString]];
                    
                    uploadRequest.account = fbAccount;
                    
                    [uploadRequest performRequestWithHandler:^(NSData *responseData, NSHTTPURLResponse *urlResponse, NSError *error) {
                        NSString *responseString = [[NSString alloc] initWithData:responseData encoding:NSUTF8StringEncoding];
                        if(error){
                            NSLog(@"Error %@", error.localizedDescription);
                        }else {
                            NSLog(@"%@", responseString);
                            SIAlertView *alertView = [[SIAlertView alloc] initWithTitle:nil andMessage:@"Video posted on Facebook"];
                            [alertView addButtonWithTitle:@"OK"
                                                     type:SIAlertViewButtonTypeDefault
                                                  handler:^(SIAlertView *alertView) {
                                                  }];
                            
                            [alertView show];

                        }
                        [indicatorView performSelectorOnMainThread:@selector(stopAnimating) withObject:nil waitUntilDone:NO];
                    }];
                }
            }
            else {
                [indicatorView performSelectorOnMainThread:@selector(stopAnimating) withObject:nil waitUntilDone:NO];
            }
        }];
    }
    else {
        SIAlertView *alertView = [[SIAlertView alloc] initWithTitle:@"Sorry" andMessage:@"You can't share Video right now, make sure your device has an internet connection and have at least one facebook account setup."];
        [alertView setMessageFont:[UIFont fontWithName:@"Papyrus" size:18]];
        [alertView addButtonWithTitle:@"OK"
                                 type:SIAlertViewButtonTypeDefault
                              handler:^(SIAlertView *alertView) {
                              }];
        
        [alertView show];
    }
}

- (IBAction)emailBtnPressed:(id)sender {
    
    if ([MFMailComposeViewController canSendMail]) {
        // Initialization
        MFMailComposeViewController *vc = [[MFMailComposeViewController alloc] init];
        
        [vc setSubject:@"Ahau Travel"];
        NSString *strVideoPath = [[[[Singleton sharedSingleton] arrData] objectAtIndex:_selectedIndex] name];
        //UIImage *img = [self loadImage:[NSURL fileURLWithPath:strVideoPath]];
        //NSData *imageData = UIImagePNGRepresentation(img);
        NSData *imageData = [NSData dataWithContentsOfFile:strVideoPath];
        
        [vc addAttachmentData:imageData mimeType:@"video/quicktime" fileName:@"AhauTravel.MOV"];
        
        [vc setMailComposeDelegate:self];
        
        [self presentViewController:vc animated:YES completion:nil];
    }
}

- (IBAction)saveBtnPressed:(id)sender {
    
    NSString *strVideoPath = [[[[Singleton sharedSingleton] arrData] objectAtIndex:_selectedIndex] name];
    //NSString *strVideoPath = [[NSBundle mainBundle] pathForResource:@"demo" ofType:@"mp4"];

    if (UIVideoAtPathIsCompatibleWithSavedPhotosAlbum(strVideoPath)) {
        UISaveVideoAtPathToSavedPhotosAlbum(strVideoPath, self, @selector(video:didFinishSavingWithError:contextInfo:), nil);
    }
    
}

- (UIImage*)loadImage:(NSURL *)url {
    
    AVURLAsset *asset = [[AVURLAsset alloc] initWithURL:url options:nil];
    AVAssetImageGenerator *generate = [[AVAssetImageGenerator alloc] initWithAsset:asset];
    generate.appliesPreferredTrackTransform = YES;
    NSError *err = NULL;
    CMTime time = CMTimeMake(1, 60);
    CGImageRef imgRef = [generate copyCGImageAtTime:time actualTime:NULL error:&err];
    //NSLog(@"err==%@, imageRef==%@", err, imgRef);
    
    return [UIImage imageWithCGImage:imgRef];
    
}

- (IBAction)refereshBtnPressed:(id)sender {
    
    [UIView transitionWithView:_viewInputTItle duration:1.0 options:UIViewAnimationOptionTransitionCrossDissolve animations:^{
        [_viewInputTItle setHidden:YES];
    } completion:^(BOOL finished) {
        [UIView transitionWithView:_viewShare duration:1.0 options:UIViewAnimationOptionTransitionCrossDissolve animations:^{
            [_viewShare setHidden:NO];
        } completion:^(BOOL finished) {
            
        }];
    }];
}

- (IBAction)renameBtnPressed:(id)sender {
    
    
}

#pragma mark --
#pragma mark UIImageWriteToSavedPhotosAlbum Delegate

- (void)video:(NSString *)videoPath didFinishSavingWithError:(NSError *)error contextInfo:(void *)contextInfo {
    
    if (error != nil) {
        NSLog(@"Error %@", error.localizedDescription);
    }
    else {
        SIAlertView *alertView = [[SIAlertView alloc] initWithTitle:nil andMessage:@"Saved to Video Gallery"];
        [alertView addButtonWithTitle:@"OK"
                                 type:SIAlertViewButtonTypeDefault
                              handler:^(SIAlertView *alertView) {
                              }];
        
        [alertView show];
    }

}

//- (void)image:(UIImage *)image didFinishSavingWithError:(NSError *)error contextInfo: (void *) contextInfo {
//    
//    if (error != nil) {
//        NSLog(@"Error %@", error.localizedDescription);
//    }
//    else {
//        SIAlertView *alertView = [[SIAlertView alloc] initWithTitle:nil andMessage:@"Saved to photo album"];
//        [alertView addButtonWithTitle:@"OK"
//                                 type:SIAlertViewButtonTypeDefault
//                              handler:^(SIAlertView *alertView) {
//                              }];
//        
//        [alertView show];
//    }
//    
//}


#pragma mark --
#pragma mark MFMailComposerViewController Delegate

- (void)mailComposeController:(MFMailComposeViewController*)controller didFinishWithResult:(MFMailComposeResult)result error:(NSError*)error
{
    // Notifies users about errors associated with the interface
    switch (result)
    {
        case MFMailComposeResultCancelled:
            break;
        case MFMailComposeResultSaved:
            break;
        case MFMailComposeResultSent:
        {
            [controller dismissViewControllerAnimated:YES completion:nil];
            break;
        }
        case MFMailComposeResultFailed:
            break;
        default:
        {
            SIAlertView *alertView = [[SIAlertView alloc] initWithTitle:@"Email" andMessage:@"Sending Failed - Unknown Error :-("];
            [alertView addButtonWithTitle:@"OK"
                                     type:SIAlertViewButtonTypeDefault
                                  handler:^(SIAlertView *alertView) {
                                  }];
            
            [alertView show];
        }
            break;
    }
    [self dismissViewControllerAnimated:YES completion:nil];
}


#pragma mark --
#pragma mark UITextField Delegate

- (BOOL)textFieldShouldReturn:(UITextField *)textField {
    [textField resignFirstResponder];
    return YES;
}

- (IBAction)cameraBtnPressed:(id)sender {
    
    SIAlertView *alertView = [[SIAlertView alloc] initWithTitle:nil andMessage:@"Choose type"];
    [alertView addButtonWithTitle:@"Take Picture"
                             type:SIAlertViewButtonTypeDefault
                          handler:^(SIAlertView *alertView) {
                              
                              PickerViewController *pickerViewController = [self.storyboard instantiateViewControllerWithIdentifier:@"PickerViewController"];
                              [pickerViewController setIsMediaTypeMovie:NO];
                              [self presentViewController:pickerViewController animated:YES completion:nil];
                          }];
    [alertView addButtonWithTitle:@"Make Video"
                             type:SIAlertViewButtonTypeDefault
                          handler:^(SIAlertView *alertView) {
                              
                              PickerViewController *pickerViewController = [self.storyboard instantiateViewControllerWithIdentifier:@"PickerViewController"];
                              [pickerViewController setIsMediaTypeMovie:YES];
                              [self presentViewController:pickerViewController animated:YES completion:nil];
                          }];
    [alertView addButtonWithTitle:@"Cancel"
                             type:SIAlertViewButtonTypeCancel
                          handler:^(SIAlertView *alertView) {
                          }];
    
    [alertView show];
}

@end
