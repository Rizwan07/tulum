//
//  GalleryVideoViewController.h
//  AhauTravel
//
//  Created by Rizwan on 6/17/13.
//  Copyright (c) 2013 com. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface GalleryVideoViewController : UIViewController

@property (nonatomic, assign) int selectedIndex;

@end
