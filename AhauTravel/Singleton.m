//
//  Singleton.m
//  AhauTravel
//
//  Created by Rizwan on 6/10/13.
//  Copyright (c) 2013 com. All rights reserved.
//

#import "Singleton.h"
#import "UIImage+fixOrientation.h"

#import "AhauDB.h"

@interface Singleton () {
    
    //NSData *data;
    UIImage *image;
    NSURL *url;
}

@end

@implementation Singleton

static Singleton *singleton;

+ (Singleton *)sharedSingleton {
    
    @synchronized(self) {
        
        if (singleton == nil) {
            singleton = [[self alloc] init];
        }
        return singleton;
    }
}

- (void)imagePickerController:(UIImagePickerController *)picker didFinishPickingMediaWithInfo:(NSDictionary *)info {
    
    NSString *strMsg;
    if (_isMovieMediaType) {
        url = [info objectForKey:UIImagePickerControllerMediaURL];
        
        //data  = [NSData dataWithContentsOfURL:url];
        strMsg = @"Would you like to save Video in your app Gallery ?";
    }
    else {
        
//        dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^{
        
            UIImage *img = [info objectForKey:UIImagePickerControllerOriginalImage];
            
            image = [img fixOrientation];
//            data = UIImagePNGRepresentation(img);
//        });
        strMsg = @"Would you like to save image in your app Gallery ?";

    }
    
    SIAlertView *alertView = [[SIAlertView alloc] initWithTitle:nil andMessage:strMsg];
    [alertView addButtonWithTitle:@"NO"
                             type:SIAlertViewButtonTypeDefault
                          handler:^(SIAlertView *alertView) {
                          }];
    [alertView addButtonWithTitle:@"YES"
                             type:SIAlertViewButtonTypeDefault
                          handler:^(SIAlertView *alertView) {
                              [self saveDataInToDocumentDirectory];
                          }];
    [alertView show];
    
    [picker dismissViewControllerAnimated:YES completion:nil];
    
//    NSString *strMsg;
//    if (_isMovieMediaType) {
//        NSURL *url = [info objectForKey:UIImagePickerControllerMediaURL];
//        
//        data  = [NSData dataWithContentsOfURL:url];
//        strMsg = @"Would you like to save Video in your app Gallery ?";
//        
//        dispatch_sync(dispatch_get_main_queue(), ^{
//            SIAlertView *alertView = [[SIAlertView alloc] initWithTitle:nil andMessage:strMsg];
//            [alertView addButtonWithTitle:@"NO"
//                                     type:SIAlertViewButtonTypeDefault
//                                  handler:^(SIAlertView *alertView) {
//                                  }];
//            [alertView addButtonWithTitle:@"YES"
//                                     type:SIAlertViewButtonTypeDefault
//                                  handler:^(SIAlertView *alertView) {
//                                      [self saveDataInToDocumentDirectory];
//                                  }];
//            
//            [alertView show];
//        });
//    }
//    else {
//        
//        data = nil;
//        
//        dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^{
//            
//            UIImage *img = [info objectForKey:UIImagePickerControllerOriginalImage];
//            
//            img = [img fixOrientation];
//            data = UIImagePNGRepresentation(img);
//            
//            dispatch_sync(dispatch_get_main_queue(), ^{
//                
//                SIAlertView *alertView = [[SIAlertView alloc] initWithTitle:nil andMessage:@"Would you like to save image in your app Gallery ?"];
//                [alertView addButtonWithTitle:@"NO"
//                                         type:SIAlertViewButtonTypeDefault
//                                      handler:^(SIAlertView *alertView) {
//                                      }];
//                [alertView addButtonWithTitle:@"YES"
//                                         type:SIAlertViewButtonTypeDefault
//                                      handler:^(SIAlertView *alertView) {
//                                          [self saveDataInToDocumentDirectory];
//                                      }];
//                
//                [alertView show];
//                
//            });
//            
//        });
//        strMsg = @"Would you like to save image in your app Gallery ?";
//        
//    }
}

- (void)imagePickerControllerDidCancel:(UIImagePickerController *)picker {
    
    [picker dismissViewControllerAnimated:YES completion:nil];
}

- (void)saveDataInToDocumentDirectory {
    
    NSTimeInterval timeInterval = [NSDate timeIntervalSinceReferenceDate];
    
    double rand = timeInterval;
    
    NSString *picName = [NSString stringWithFormat:@"%f",rand];
    picName = [picName stringByReplacingOccurrencesOfString:@"." withString:@"0"];
    
    NSArray *docspaths=NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
    NSString *docsdir=[docspaths objectAtIndex:0];
    
   // NSData *data = nil;
    
    BOOL isSave = NO;
    
    if (!_isMovieMediaType) {
        
        picName = [picName stringByAppendingString:@".png"];
        picName = [docsdir stringByAppendingPathComponent:picName];
        
        isSave = [UIImagePNGRepresentation(image) writeToFile:picName options:NSDataWritingAtomic error:nil];
        if (isSave) {
            AhauDB *ahauDb = [[AhauDB alloc] init];
            [ahauDb addAhauWithname:picName title:@"image"];
        }
    }
    else {
        picName = [picName stringByAppendingString:@".MOV"];
        picName = [docsdir stringByAppendingPathComponent:picName];
        
        isSave = [[NSData dataWithContentsOfURL:url] writeToFile:picName options:NSDataWritingAtomic error:nil];
        if (isSave) {
            AhauDB *ahauDb = [[AhauDB alloc] init];
            [ahauDb addAhauWithname:picName title:@"video"];
        }
    }
    
    //BOOL isSave = [data writeToFile:picName atomically:YES];
    
    if (isSave) {
        
        if ([_delegate respondsToSelector:@selector(done)]) {
            [_delegate done];
        }
        else {
            SIAlertView *alertView = [[SIAlertView alloc] initWithTitle:nil andMessage:@"Saved !"];
            [alertView addButtonWithTitle:@"OK"
                                     type:SIAlertViewButtonTypeDefault
                                  handler:^(SIAlertView *alertView) {
                                  }];
            [alertView show];
        }
    }
    else {
        SIAlertView *alertView = [[SIAlertView alloc] initWithTitle:nil andMessage:@"Could not Save"];
        [alertView addButtonWithTitle:@"OK"
                                 type:SIAlertViewButtonTypeDefault
                              handler:^(SIAlertView *alertView) {
                              }];
        [alertView show];

    }
    
}

@end
