//
//  PickerViewController.m
//  AhauTravel
//
//  Created by Rizwan on 7/2/13.
//  Copyright (c) 2013 com. All rights reserved.
//

#import "PickerViewController.h"

#import "Singleton.h"
#import <MobileCoreServices/UTCoreTypes.h>

@interface PickerViewController ()

@end

@implementation PickerViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    [[Singleton sharedSingleton] setIsMovieMediaType:_isMediaTypeMovie];
    
#if !TARGET_IPHONE_SIMULATOR
    [self setSourceType:UIImagePickerControllerSourceTypeCamera];
    if (_isMediaTypeMovie) {
        [self setMediaTypes:[NSArray arrayWithObject:(NSString *)kUTTypeMovie]];
    }
    [self setWantsFullScreenLayout:YES];
#endif

    [self setDelegate:[Singleton sharedSingleton]];
    
	// Do any additional setup after loading the view.
}

//- (void)imagePickerController:(UIImagePickerController *)picker didFinishPickingImage:(UIImage *)image editingInfo:(NSDictionary *)editingInfo {
//    
//}

- (UIStatusBarStyle)preferredStatusBarStyle
{
    return UIStatusBarStyleLightContent;
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (IBAction)backBtnPressed:(id)sender {
    
    [self dismissViewControllerAnimated:YES completion:nil];
}
@end
