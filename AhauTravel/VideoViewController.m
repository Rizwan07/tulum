//
//  VideoViewController.m
//  AhauTravel
//
//  Created by Rizwan on 5/23/13.
//  Copyright (c) 2013 com. All rights reserved.
//

#import "VideoViewController.h"
#import <QuartzCore/QuartzCore.h>
//#import <MediaPlayer/MediaPlayer.h>

#import "NimbusAttributedLabel.h"
#import "SelectedImageViewController.h"
#import "HomeViewController.h"

@interface VideoViewController () {
    
    int _duration;
    float _currentPlayBackTime;
    BOOL isVolumeOn;
    BOOL isPlaying;
    BOOL isPause;
    IBOutlet UIImageView *imageView;
    IBOutlet UIImageView *imageViewFull;
    NSTimer *updateTimer;
    
    int totalMinutes;
    IBOutlet UIButton *btnPlayPause;
    
    IBOutlet UISlider *slider;

}

@property (nonatomic, weak) UIViewController *myParentViewController;

@property (nonatomic, strong) NSArray *arrData;


//@property (strong, nonatomic) MPMoviePlayerController *playerController;
@property (weak, nonatomic) AVPlayer *avPlayer;

@property (weak, nonatomic) IBOutlet UIView *viewVideo;
@property (weak, nonatomic) IBOutlet UILabel *lblTime;
@property (weak, nonatomic) IBOutlet UISegmentedControl *segmentControl;
@property (weak, nonatomic) IBOutlet UITextView *txtView;
@property (weak, nonatomic) IBOutlet UIView *viewWhatIsThat;
@property (strong, nonatomic) IBOutlet UIScrollView *scrollView;
@property (strong, nonatomic) IBOutlet NIAttributedLabel *lblText;

- (IBAction)segmentBtnPressed:(id)sender;

- (IBAction)backBtnPressed:(id)sender;
- (IBAction)playBtnPressed:(id)sender;
- (IBAction)volumeBtnPressed:(id)sender;
- (IBAction)commentBtnPressed:(id)sender;
- (IBAction)fullScreenBtnPressed:(id)sender;

- (IBAction)cameraBtnPressed:(id)sender;

- (IBAction)sliderSlided:(id)sender;

@end

@interface VideoViewController () <NIAttributedLabelDelegate, AVAudioPlayerDelegate>
@end

@implementation VideoViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    isVolumeOn = YES;
    
    [slider setMaximumTrackImage:[UIImage imageNamed:@"outer-volume-bar.png"] forState:UIControlStateNormal];
    [slider setMinimumTrackImage:[UIImage imageNamed:@"inner-volume-bar.png"] forState:UIControlStateNormal];
    [slider setThumbImage:[UIImage imageNamed:@"thumb-volume-control.png"] forState:UIControlStateNormal];

    NSString *path = [[NSBundle mainBundle] pathForResource:@"AudioFileList" ofType:@"plist"];
    self.arrData = [NSArray arrayWithContentsOfFile:path];
    NSDictionary *dic = _arrData[_selectedIndex];
    //NSString *imageName = [NSString stringWithFormat:@"str%02d.png",_selectedIndex-19];
    NSString *imageName = [dic objectForKey:@"mapImage"];
    UIImage *image = [UIImage imageNamed:imageName];
    if (image) {
        [imageView setImage:image];
        [imageViewFull setImage:image];
    }
    
    [_lblHeading setText:[dic objectForKey:@"heading"]];
    self.myParentViewController = self.navigationController.viewControllers[0];
    
    [self setScrollViewProgramatically];
    //[self playBtnPressed:nil];
    
    //[self setMediaView];
	// Do any additional setup after loading the view.
}

- (void)viewDidAppear:(BOOL)animated {
    [super viewDidAppear:animated];
    
    [self.view bringSubviewToFront:imageViewFull];
    
}

- (void)viewWillDisappear:(BOOL)animated {
    [super viewWillDisappear:animated];
    
    //[self audioStopped];
}

- (void)setScrollViewProgramatically {
    
    self.lblText = [[NIAttributedLabel alloc] initWithFrame:CGRectZero];
    [_lblText setDelegate:self];
    _lblText.numberOfLines = 0;
    _lblText.lineBreakMode = NSLineBreakByWordWrapping;
    _lblText.font = [UIFont systemFontOfSize:15];
    [_lblText setBackgroundColor:[UIColor clearColor]];
    [_lblText setTextColor:[UIColor whiteColor]];
    //self.label.autoDetectLinks = YES;
    //_lblText.dataDetectorTypes = NSTextCheckingAllSystemTypes;
    
    _lblText.textAlignment = NSTextAlignmentJustified;
    
    // When we enable defering the link detection is offloaded to a separate thread. This allows
    // the label to display its text immediately and redraw itself once the links have been
    // detected. For the block of text below this ends up displaying the text ~300ms faster on an
    // iPhone 4S than it otherwise would have.
    _lblText.deferLinkDetection = YES;
    
    
    self.scrollView = [[UIScrollView alloc] initWithFrame:CGRectMake(10, 270, 300, self.view.frame.size.height-270)];
    self.scrollView.autoresizingMask = UIViewAutoresizingFlexibleDimensions;
    //self.scrollView.contentInset = UIEdgeInsetsMake(20, 0, 20, 0);
    [self.scrollView addSubview:_lblText];
    [self.view addSubview:self.scrollView];
    
    
    [self setLabelFrame];
    
}

/*- (void)setMediaView {
    
    isVolumeOn = YES;
    isPlaying = YES;
    
    NSString *strUrl = [[NSBundle mainBundle] pathForResource:@"demo" ofType:@"mp4"];
    NSURL *url = [NSURL fileURLWithPath:strUrl];
    _playerController = [[MPMoviePlayerController alloc] initWithContentURL:url];
    
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(movieFinished:) name:MPMoviePlayerPlaybackDidFinishNotification object:_playerController];
    
    [_playerController.view setFrame:CGRectMake(0, 0, 320, 198)];
    [_playerController setControlStyle:MPMovieControlStyleNone];
    [_playerController prepareToPlay];
    [_playerController play];
    _currentPlayBackTime = _playerController.currentPlaybackTime;
    _duration = [_playerController duration];
    [_playerController currentPlaybackTime];
    [_viewVideo addSubview:_playerController.view];
}

- (void)setMediaPlayerLayerView {
    
    NSString *strUrl = [[NSBundle mainBundle] pathForResource:@"demo" ofType:@"mp4"];
    NSURL *url = [NSURL fileURLWithPath:strUrl];
    self.avPlayer = [AVPlayer playerWithURL:url];
    
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(movieFinished:) name:MPMoviePlayerPlaybackDidFinishNotification object:_avPlayer];
    
    
    AVPlayerLayer *layer = [AVPlayerLayer playerLayerWithPlayer:_avPlayer];
    
    [layer setFrame:CGRectMake(0, 0, 320, 198)];
    [_avPlayer setActionAtItemEnd:AVPlayerActionAtItemEndNone];
    [_viewVideo.layer addSublayer:layer];
    
    [_avPlayer play];
    
}

- (void)movieFinished:(MPMoviePlayerController *)playerContorller {
    
    [[NSNotificationCenter defaultCenter] removeObserver:self name:MPMoviePlayerPlaybackDidFinishNotification object:playerContorller];
    //NSLog(@"End Video");
    isPlaying = NO;
}*/

- (void)_layoutLabel {
    CGSize size = [_lblText sizeThatFits:CGSizeMake(290, CGFLOAT_MAX)];
    _lblText.frame = CGRectMake(5, 0, 290, size.height);
    _scrollView.contentSize = CGSizeMake(300, size.height);
}

- (void)setLabelFrame {
    
    [_lblText setText:[_arrData[_selectedIndex] objectForKey:@"text"]];
    
    NSArray *array = [_arrData[_selectedIndex] objectForKey:@"imageLink"];
    
    for (NSDictionary *aDic in array) {
        
        NSString *imageName = [aDic objectForKey:@"imageName"];
        NSString *data = [aDic objectForKey:@"data"];
        
        [_lblText addLink:[NSURL URLWithString:imageName] range:[_lblText.text rangeOfString:data]];
        //[_lblText addLink:[NSURL URLWithString:@"nimbus://custom/url"] range:[_lblText.text rangeOfString:[aDic objectForKey:@"data"]]];
    }
    
    [self _layoutLabel];
    
//    CGRect labelFrame = _lblText.frame;
//    labelFrame.size = [_lblText.text sizeWithFont:_lblText.font constrainedToSize:CGSizeMake(300, CGFLOAT_MAX) lineBreakMode:_lblText.lineBreakMode];
//    _lblText.frame = labelFrame;
//    [_lblText sizeToFit];
//    [_scrollView setContentSize:CGSizeMake(300, labelFrame.size.height)];
}

- (UIStatusBarStyle)preferredStatusBarStyle
{
    return UIStatusBarStyleLightContent;
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (IBAction)segmentBtnPressed:(id)sender {
    
    NSLog(@"Segment %@", [_segmentControl titleForSegmentAtIndex:[_segmentControl selectedSegmentIndex]]);
    
    switch ([_segmentControl selectedSegmentIndex]) {
        case 0: {
            [UIView transitionWithView:_viewWhatIsThat duration:1.0 options:UIViewAnimationOptionTransitionCrossDissolve animations:^{
                [_viewWhatIsThat setHidden:YES];
            } completion:^(BOOL finished) {
            }];
        }
            break;
        case 1: {
            [UIView transitionWithView:_viewWhatIsThat duration:1.0 options:UIViewAnimationOptionTransitionCrossDissolve animations:^{
                [_viewWhatIsThat setHidden:NO];
            } completion:^(BOOL finished) {
                
            }];
        }
            break;
        default:
            break;
    }
    
}

- (IBAction)backBtnPressed:(id)sender {
    
    [self audioStopped];
    [self.navigationController popViewControllerAnimated:YES];
}

- (IBAction)playBtnPressed:(id)sender {

    if (![self canPlay]) {
        
        SIAlertView *alertView = [[SIAlertView alloc] initWithTitle:nil andMessage:@"Audio is already Running"];
        [alertView addButtonWithTitle:@"OK"
                                 type:SIAlertViewButtonTypeDefault
                              handler:^(SIAlertView *alertView) {
                              }];
        
        [alertView show];
        
        return;
    }
    
    if (_audioPlayer && [_audioPlayer isPlaying]) {
        [(UIButton *)sender setSelected:NO];
        [_audioPlayer pause];
        isPause = YES;
        [self audioPaused];
    }
    else if (_audioPlayer && isPause) {
        [(UIButton *)sender setSelected:YES];
        isPause = NO;
        [_audioPlayer play];
    }
    else {
        [(UIButton *)sender setSelected:YES];
        isPause = NO;
        [slider setMinimumValue:0.0];
        [slider setValue:0.0];

        NSString *strResourchPath = [[NSBundle mainBundle] resourcePath];
        
        [self setLabelFrame];
        
        NSURL *url = [NSURL fileURLWithPath:[NSString stringWithFormat:@"%@/%@", strResourchPath, [_arrData[_selectedIndex] objectForKey:@"fileName"]]];
        
        NSError *error;
        self.audioPlayer = [[AVAudioPlayer alloc] initWithContentsOfURL:url error:&error];
        //_audioPlayer.numberOfLoops = -1;
        [_audioPlayer setDelegate:self];
        if (isVolumeOn) {
            [_audioPlayer setVolume:1.0];
        }
        else {
            [_audioPlayer setVolume:0.0];
        }
        if (_audioPlayer == nil)
            NSLog(@"%@", [error description]);
        else
            [_audioPlayer play];
        
        totalMinutes = [_audioPlayer duration]/60;

        updateTimer = [NSTimer scheduledTimerWithTimeInterval:.1 target:self selector:@selector(updateCurrentTime) userInfo:_audioPlayer repeats:YES];
        [slider setMaximumValue:[_audioPlayer duration]];
    }
    
//    UIButton *btn = (UIButton *)sender;
//
//    if (!isPlaying) {
//        [btn setSelected:NO];
//        [_playerController play];
//    }
//    else {
//        [btn setSelected:YES];
//        [_playerController pause];
//    }
//    isPlaying = !isPlaying;
}

- (BOOL)canPlay {
    
    NSLog(@"can play");
    
    if ([_myParentViewController isKindOfClass:[HomeViewController class]]) {
        if ([[Singleton sharedSingleton] aAudioRunning] || [[Singleton sharedSingleton] tAudioRunning]) {
            //
            return NO;
        }
        else {
            [[Singleton sharedSingleton] setHAudioRunning:YES];
        }
    }
    else {
        if ([[Singleton sharedSingleton] aAudioRunning] || [[Singleton sharedSingleton] hAudioRunning]) {
            //
            return NO;
        }
        else {
            [[Singleton sharedSingleton] setTAudioRunning:YES];
        }
    }
    return YES;
}

- (void)audioPaused {
    
    if ([_myParentViewController isKindOfClass:[HomeViewController class]]) {
        [[Singleton sharedSingleton] setHAudioRunning:NO];
    }
    else {
        [[Singleton sharedSingleton] setTAudioRunning:NO];
    }
}

//- (void)startAudioPlay {
//    
//    if ([_myParentViewController isKindOfClass:[HomeViewController class]]) {
//        if (![[Singleton sharedSingleton] audioRunningAudioPlay] && ![[Singleton sharedSingleton] audioRunningTakeMeThere]) {
//            [[Singleton sharedSingleton] setAudioRunningHome:YES];
//            [_audioPlayer play];
//        }
//    }
//    else {
//        if (![[Singleton sharedSingleton] audioRunningAudioPlay] && ![[Singleton sharedSingleton] audioRunningHome]) {
//            [[Singleton sharedSingleton] setAudioRunningTakeMeThere:YES];
//            [_audioPlayer play];
//        }
//    }
//}

- (void)audioStopped {
    
    [btnPlayPause setSelected:NO];
    [updateTimer invalidate];
    [_audioPlayer stop];
    
    if ([_myParentViewController isKindOfClass:[HomeViewController class]]) {
        [[Singleton sharedSingleton] setHAudioRunning:NO];
    }
    else {
        [[Singleton sharedSingleton] setTAudioRunning:NO];
    }
}

- (void)updateCurrentTime {
    
    [self updateCurrentTimeForPlayer:_audioPlayer];

    int currentMinutes = [_audioPlayer currentTime]/60;
    
    [_lblTime setText:[NSString stringWithFormat:@"%02d:%02.f / %02d:%02.f", currentMinutes, ([_audioPlayer currentTime]-(currentMinutes*60)), totalMinutes, ([_audioPlayer duration]- (totalMinutes*60))]];
}

- (IBAction)volumeBtnPressed:(id)sender {
    
    if (isVolumeOn) {
        [(UIButton *)sender setSelected:YES];
        [_audioPlayer setVolume:0.0];
        //[[MPMusicPlayerController applicationMusicPlayer] setVolume:0.0];
    }
    else {
        [(UIButton *)sender setSelected:NO];
        [_audioPlayer setVolume:1.0];
        //[[MPMusicPlayerController applicationMusicPlayer] setVolume:1.0];
    }
    isVolumeOn = !isVolumeOn;
}

- (IBAction)commentBtnPressed:(id)sender {
}

- (IBAction)fullScreenBtnPressed:(id)sender {
    
//    [_playerController setControlStyle:MPMovieControlStyleDefault];
//    [_playerController setFullscreen:YES animated:YES];
}

- (IBAction)sliderSlided:(id)sender {
    
    [_audioPlayer setCurrentTime:slider.value];
}

-(void)updateCurrentTimeForPlayer:(AVAudioPlayer *)p
{
	slider.value = p.currentTime;
}


#pragma mark - NIAttributedLabelDelegate

- (void)attributedLabel:(NIAttributedLabel *)attributedLabel didSelectTextCheckingResult:(NSTextCheckingResult *)result atPoint:(CGPoint)point {
    
    NSString *strImageData = [result.URL.relativeString stringByReplacingPercentEscapesUsingEncoding:NSUTF8StringEncoding];
    
    NSArray *arrImageData = [strImageData componentsSeparatedByString:@"____"];

    if ([strImageData length]) {
        
        SelectedImageViewController *selectedImageViewController = [self.storyboard instantiateViewControllerWithIdentifier:@"SelectedImageViewController"];
        [selectedImageViewController setStrImages:arrImageData[0]];
        [selectedImageViewController setStrHeading:arrImageData[1]];
        [self.navigationController pushViewController:selectedImageViewController animated:YES];
        
        //[selectedImageViewController.imageView setImage:[UIImage imageNamed:strImageName]];
        //[selectedImageViewController.lblHeading setText:[strImageName stringByReplacingOccurrencesOfString:@".png" withString:@""]];
    }
    else {
        
        SIAlertView *alertView = [[SIAlertView alloc] initWithTitle:nil andMessage:@"Image not found"];
        [alertView addButtonWithTitle:@"OK"
                                 type:SIAlertViewButtonTypeDefault
                              handler:^(SIAlertView *alertView) {
                              }];
        
        [alertView show];
    }
}

#pragma mark --
#pragma mark -- AVAudioPlayer Delegates

- (void)audioPlayerDidFinishPlaying:(AVAudioPlayer *)player successfully:(BOOL)flag {
    [self audioStopped];
}

- (void)audioPlayerDecodeErrorDidOccur:(AVAudioPlayer *)player error:(NSError *)error {
    [self audioStopped];
}

- (IBAction)cameraBtnPressed:(id)sender {
    
    SIAlertView *alertView = [[SIAlertView alloc] initWithTitle:nil andMessage:@"Choose type"];
    [alertView addButtonWithTitle:@"Take Picture"
                             type:SIAlertViewButtonTypeDefault
                          handler:^(SIAlertView *alertView) {
                              
                              PickerViewController *pickerViewController = [self.storyboard instantiateViewControllerWithIdentifier:@"PickerViewController"];
                              [pickerViewController setIsMediaTypeMovie:NO];
                              [self presentViewController:pickerViewController animated:YES completion:nil];
                          }];
    [alertView addButtonWithTitle:@"Make Video"
                             type:SIAlertViewButtonTypeDefault
                          handler:^(SIAlertView *alertView) {
                              
                              PickerViewController *pickerViewController = [self.storyboard instantiateViewControllerWithIdentifier:@"PickerViewController"];
                              [pickerViewController setIsMediaTypeMovie:YES];
                              [self presentViewController:pickerViewController animated:YES completion:nil];
                          }];
    [alertView addButtonWithTitle:@"Cancel"
                             type:SIAlertViewButtonTypeCancel
                          handler:^(SIAlertView *alertView) {
                          }];
    
    [alertView show];
}

@end
