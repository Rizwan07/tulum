//
//  EdsTourViewController.m
//  AhauTravel
//
//  Created by Rizwan on 5/20/13.
//  Copyright (c) 2013 com. All rights reserved.
//

#import "EdsTourViewController.h"

@interface EdsTourViewController () {
    
    UIScrollView *_scrollView;
    UIImageView *_imageView;
}

- (IBAction)cameraBtnPressed:(id)sender;

@end

@interface EdsTourViewController ()<UIScrollViewDelegate>
@end

@implementation EdsTourViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
	[self setScrollView];
}

- (UIStatusBarStyle)preferredStatusBarStyle
{
    return UIStatusBarStyleLightContent;
}

-(void) setScrollView
{
    CGRect rectScroll = CGRectZero;
    rectScroll.size.width = self.view.frame.size.width;
    
    if ([[[UIDevice currentDevice] systemVersion] doubleValue] < 7.0) {
        if ([[UIDevice currentDevice] userInterfaceIdiom] == UIUserInterfaceIdiomPad) {
            rectScroll.origin.y = 90;
        }
        else {
            rectScroll.origin.y = 45;
        }
    }
    else {
        if ([[UIDevice currentDevice] userInterfaceIdiom] == UIUserInterfaceIdiomPad) {
            rectScroll.origin.y = 110;
        }
        else {
            rectScroll.origin.y = 65;
        }
    }
    
    rectScroll.size.height = self.view.frame.size.height-self.tabBarController.tabBar.frame.size.height-rectScroll.origin.y;
    
    _scrollView = [[UIScrollView alloc] initWithFrame:rectScroll];

	_scrollView.backgroundColor = [UIColor blackColor];
	_scrollView.delegate = self;
	_imageView = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"map-eds-tour.jpg"]];
    [_imageView setUserInteractionEnabled:YES];
    _scrollView.contentSize = _imageView.image.size;
    [_scrollView setAlwaysBounceHorizontal:YES];
	[_scrollView addSubview:_imageView];
	_scrollView.minimumZoomScale = _scrollView.frame.size.width / _imageView.frame.size.width;
	_scrollView.maximumZoomScale = 1.5;
	[_scrollView setZoomScale:_scrollView.minimumZoomScale];
	[self.view addSubview:_scrollView];
    
    UITapGestureRecognizer *dblRecognizer = [[UITapGestureRecognizer alloc] initWithTarget:self
                                                                                    action:@selector(handleDoubleTapFrom:)];
    [dblRecognizer setNumberOfTapsRequired:2];
    
    [_scrollView addGestureRecognizer:dblRecognizer];
    
}

- (CGRect)zoomRectForScale:(float)scale withCenter:(CGPoint)center {
    
    CGRect zoomRect;
    
    zoomRect.size.height = [_imageView frame].size.height / scale;
    zoomRect.size.width  = [_imageView frame].size.width  / scale;
    
    center = [_imageView convertPoint:center fromView:_scrollView];
    
    zoomRect.origin.x    = center.x - ((zoomRect.size.width / 2.0));
    zoomRect.origin.y    = center.y - ((zoomRect.size.height / 2.0));
    
    return zoomRect;
}

- (void)handleDoubleTapFrom:(UITapGestureRecognizer *)recognizer {
    
    //float newScale = [scrollView zoomScale] * 3.0;
    
    if (_scrollView.zoomScale > _scrollView.minimumZoomScale) {
        [_scrollView setZoomScale:_scrollView.minimumZoomScale animated:YES];
    }
    else {
        CGRect zoomRect = [self zoomRectForScale:_scrollView.maximumZoomScale withCenter:[recognizer locationInView:recognizer.view]];
        [_scrollView zoomToRect:zoomRect animated:YES];
    }
}

- (CGRect)centeredFrameForScrollView:(UIScrollView *)scroll andUIView:(UIView *)rView {
	CGSize boundsSize = scroll.bounds.size;
    CGRect frameToCenter = rView.frame;
    
    // center horizontally
    if (frameToCenter.size.width < boundsSize.width) {
        frameToCenter.origin.x = (boundsSize.width - frameToCenter.size.width) / 2;
    }
    else {
        frameToCenter.origin.x = 0;
    }
    
    // center vertically
    if (frameToCenter.size.height < boundsSize.height) {
        frameToCenter.origin.y = (boundsSize.height - frameToCenter.size.height) / 2;
    }
    else {
        frameToCenter.origin.y = 0;
    }
	
	return frameToCenter;
}


#pragma mark --
#pragma mark -- UIScrollView Delegate

- (void)scrollViewDidZoom:(UIScrollView *)scrollV {
	_imageView.frame = [self centeredFrameForScrollView:scrollV andUIView:_imageView];
}

- (UIView *)viewForZoomingInScrollView:(UIScrollView *)scrollView {
	return _imageView;
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (IBAction)cameraBtnPressed:(id)sender {
    
    SIAlertView *alertView = [[SIAlertView alloc] initWithTitle:nil andMessage:@"Choose type"];
    [alertView addButtonWithTitle:@"Take Picture"
                             type:SIAlertViewButtonTypeDefault
                          handler:^(SIAlertView *alertView) {
                              
                              PickerViewController *pickerViewController = [self.storyboard instantiateViewControllerWithIdentifier:@"PickerViewController"];
                              [pickerViewController setIsMediaTypeMovie:NO];
                              [self presentViewController:pickerViewController animated:YES completion:nil];
                          }];
    [alertView addButtonWithTitle:@"Make Video"
                             type:SIAlertViewButtonTypeDefault
                          handler:^(SIAlertView *alertView) {
                              
                              PickerViewController *pickerViewController = [self.storyboard instantiateViewControllerWithIdentifier:@"PickerViewController"];
                              [pickerViewController setIsMediaTypeMovie:YES];
                              [self presentViewController:pickerViewController animated:YES completion:nil];
                          }];
    [alertView addButtonWithTitle:@"Cancel"
                             type:SIAlertViewButtonTypeCancel
                          handler:^(SIAlertView *alertView) {
                          }];
    
    [alertView show];
}

@end
