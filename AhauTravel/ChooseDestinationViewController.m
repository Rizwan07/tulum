//
//  ChooseDestinationViewController.m
//  AhauTravel
//
//  Created by Rizwan on 7/26/13.
//  Copyright (c) 2013 com. All rights reserved.
//

#import "ChooseDestinationViewController.h"
#import "TakeMeThereViewController.h"

@interface ChooseDestinationViewController () {

    NSArray *_arrData;
    IBOutlet UITableView *_tableView;
    
    int tags[80];
    float arrCoordinates[100][2];
}

- (IBAction)backBtnPressed:(id)sender;
- (IBAction)cameraBtnPressed:(id)sender;

@end


@implementation ChooseDestinationViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    [_tableView setBackgroundView:nil];
    
    int tag[80] = {20, // 1
        21, // 2
        22, // 3
        23, // 4
        24, // 5
        25, // 6
        26, // 7
        27, // 8
        28, // 9
        29, // 10
        30, // 11
        31, // 12
        32, // 13
        33, // 14
        34, // 15
        35, // 16
        36, // 17
        37, // 18
        38, // 19
        39, // 20
        40, // 21
        41, // 22
        42, // 23
        43, // 24
        44, // 25
        45, // 26
        46, // 27
        47, // 28
        48, // 29
        49, // 30
        50, // 31
        51, // 32
        52, // 33
        53, // 34
        54, // 35
        55, // 36
//        56, // 37
//        56, // 38
        56, // 39
        56, // 40
        56, // 41
        57, // 42
        58, // 43
        59, // 44
        60, // 45
        61, // 46
        62, // 47
//        62, // 48
        63, // 49
        64, // 50
        65, // 51
        66, // 52
        67, // 53
        68, // 54
        69, // 55
        69, // 56
        70, // 60
        71, // 61
        72, // 62
        73, // 63
        74, // 64
        75, // 65
        76, // 66
        77, // 67
        78, // 68
        7, // gift shop
        79, // tichet booth
        0, // bathroom women
        0, // bathroom men
        12, // North gate 2
        11, // North gate 1
        80, // West gate
        15, // South gate 2
        14, // South gate 1
        8, // Lesser Wall
        1, // Beach access
        2 // Beach port
        
    };
    
    for (int i = 0; i < 80; i ++) {
        tags[i] = tag[i];
    }
    
    float temp[100][2] = {{20.214653, -87.428772}, // str1 NE
        //{20.214367, -87.428983}, // str1 SE
        {20.214747, -87.428858}, // str2
        {20.214411, -87.428997}, // str3
        {20.214820, -87.428861}, // str4
        {20.214833, -87.428939}, // str5 DivingDog
        {20.214872, -87.429064}, // str6
        {20.214728, -87.429033}, // str7
        {20.214681, -87.429158}, // str8
        {20.214369, -87.429081}, // str9 initial series
        {20.214478, -87.429228}, // str10
        {20.214592, -87.429286}, // str11
        {20.214531, -87.429375}, // str12
        {20.214439, -87.429550}, // str13
        {20.214597, -87.429642}, // str14
        {20.214792, -87.429686}, // str15
        {20.214801, -87.429414}, // str16 frescos center
        {20.214717, -87.429503}, // str17
        {20.214789, -87.429472}, // str18
        {20.214697, -87.429850}, // str19
        {20.214842, -87.429811}, // str20
        {20.214944, -87.429444}, // str21 hcolomns
        {20.215225, -87.429447}, // str22
        {20.215131, -87.429386}, // str23
        {20.215194, -87.429156}, // str24
        {20.215281, -87.429290}, // str25
        {20.215006, -87.429775}, // str26
        {20.215125, -87.429717}, // str27
        {20.215178, -87.429667}, // str28
        {20.215358, -87.429514}, // str29
        {20.215503, -87.429428}, // str30
        {20.216222, -87.429111}, // str31
        {20.216319, -87.429075}, // str32
        {20.216061, -87.429006}, // str33
        {20.216197, -87.428944}, // str34
        {20.216172, -87.428283}, // str35 Cenout house
        {20.216150, -87.428181}, // str36
//        {20.215843, -87.428155}, // str37
//        {20.215843, -87.428155}, // str38
        {20.215636, -87.428250}, // str39
        {20.215661, -87.428231}, // str40
        {20.215664, -87.428200}, // str41
        {20.215544, -87.428194}, // str42
        {20.215536, -87.428169}, // str43
        {20.215531, -87.428147}, // str44
        {20.215458, -87.428181}, // str45
        {20.214333, -87.429406}, // str46
        {20.214261, -87.429481}, // str47
//        {20.214039, -87.429638}, // str48 empty space
        {20.213656, -87.430211}, // str49
        {20.213067, -87.429678}, // str50
        {20.213356, -87.429469}, // str51
        {20.213361, -87.429300}, // str52
        {20.213242, -87.429333}, // str53
        {20.213161, -87.429300}, // str54
        {20.216681, -87.429475}, // str55
        {20.213272, -87.430814}, // str56
        {20.215633, -87.428506}, // str60
        {20.216286, -87.428758}, // str61
        {20.215883, -87.429089}, // str62
        {20.215778, -87.429144}, // str63
        {20.215594, -87.429158}, // str64
        {20.215625, -87.429233}, // str65
        {20.215447, -87.429233}, // str66
        {20.215989, -87.429211}, // str67
        {20.215728, -87.429342}, // str68
        {20.215433, -87.430372}, // gift shop
        {20.215218, -87.430475}, // tichet booth
        {20.215156, -87.430598}, // bathroom women
        {20.215057, -87.430628}, // bathroom men
        {20.216489, -87.428961}, // North gate 2
        {20.216216, -87.428178}, // North gate 1
        {20.214789, -87.430306}, // West gate
        {20.213089, -87.430506}, // South gate 2
        {20.212836, -87.429583}, // South gate 1
        {20.213114, -87.430892}, // Lesser Wall
        {20.214150, -87.429022}, // Beach access
        {20.215078, -87.428517}, // Beach port
    };
    
    for (int i=0; i<100; i++) {
        for (int j=0; j<2; j++) {
            arrCoordinates[i][j] = temp[i][j];
        }
    }
    
    _arrData = [NSArray arrayWithObjects:@"Structure 1, The Castillo",
                     @"Structure 2",
                     @"Structure 3",
                     @"Structure 4",
                     @"Structure 5, The Temple of Diving God",
                     @"Structure 6",
                     @"Structure 7",
                     @"Structure 8",
                     @"Structure 9, The Temple of Initial Series",
                     @"Structure 10",
                     @"Structure 11",
                     @"Structure 12",
                     @"Structure 13",
                     @"Structure 14",
                     @"Structure 15",
                     @"Structure 16, The Temple of Frescos",
                     @"Structure 17",
                     @"Structure 18",
                     @"Structure 19",
                     @"Structure 20, The House of Chultun",
                     @"Structure 21, The House of Columns",
                     @"Structure 22",
                     @"Structure 23",
                     @"Structure 24",
                     @"Structure 25, The House of Halach Winik",
                     @"Structure 26",
                     @"Structure 27",
                     @"Structure 28",
                     @"Structure 29",
                     @"Structure 30",
                     @"Structure 31",
                     @"Structure 32",
                     @"Structure 33",
                     @"Structure 34",
                     @"Structure 35, The Cenote House",
                     @"Structure 36",
//                     @"Structure 37",
//                     @"Structure 38",
                     @"Structure 39, The Miniature Shrine",
                     @"Structure 40, The Miniature Shrine",
                     @"Structure 41, The Miniature Shrine",
                     @"Structure 42",
                     @"Structure 43",
                     @"Structure 44",
                     @"Structure 45, The Temple of Wind",
                     @"Structure 46",
                     @"Structure 47",
//                     @"Structure 48",
                     @"Structure 49",
                     @"Structure 50",
                     @"Structure 51",
                     @"Structure 52",
                     @"Structure 53",
                     @"Structure 54",
                     @"Structure 55, The Watch Towers",
                     @"Structure 56, The Watch Towers",
                     @"Structure 60",
                     @"Structure 61",
                     @"Structure 62",
                     @"Structure 63",
                     @"Structure 64",
                     @"Structure 65",
                     @"Structure 66",
                     @"Structure 67",
                     @"Structure 68",
                     @"Gift shop",
                     @"Ticket booth",
                     @"Bathroom women",
                     @"Bathroom men",
                     @"North gate 2",
                     @"North gate 1",
                     @"West gate",
                     @"South gate 2",
                     @"South gate 1",
                     @"Lesser Wall",
                     @"Beach access",
                     @"Beach port",
                     nil];
	
}

- (UIStatusBarStyle)preferredStatusBarStyle
{
    return UIStatusBarStyleLightContent;
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    
    return [_arrData count];
}


- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"CellIdentifier"];
    
    UILabel *lbl = (UILabel *)[cell viewWithTag:1000];
    [lbl setText:_arrData[indexPath.row]];
    
    return cell;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    
    UITableViewCell *cell = [tableView cellForRowAtIndexPath:indexPath];
    [cell setSelectionStyle:UITableViewCellSelectionStyleNone];
    
//    if (indexPath.row == 36 || indexPath.row == 37 || indexPath.row == 47) {
//        return;
//    }
    
    NSArray *arr = [_arrData[indexPath.row] componentsSeparatedByString:@", "];
    
    NSString *strMsg = [NSString stringWithFormat:@"Destination %@ ?", arr[0]];
    
    SIAlertView *alertView = [[SIAlertView alloc] initWithTitle:nil andMessage:strMsg];
    [alertView addButtonWithTitle:@"OK"
                             type:SIAlertViewButtonTypeDefault
                          handler:^(SIAlertView *alertView) {
                              
                              //[[NSUserDefaults standardUserDefaults] setObject:_arrData[indexPath.row] forKey:@"structure"];
                              [[NSUserDefaults standardUserDefaults] setObject:arr[0] forKey:@"structure"];
                              [[NSUserDefaults standardUserDefaults] synchronize];
                              
                              TakeMeThereViewController *tkVC = self.navigationController.viewControllers[0];
                              [tkVC setFinalLocation:CLLocationCoordinate2DMake(arrCoordinates[indexPath.row][0], arrCoordinates[indexPath.row][1])];
                              [tkVC setSelectedIndex:tags[indexPath.row]];
                              [self backBtnPressed:nil];
                            }];
    [alertView addButtonWithTitle:@"Cancel"
                             type:SIAlertViewButtonTypeDefault
                          handler:^(SIAlertView *alertView) {
                              
                          }];
    alertView.messageFont = [UIFont fontWithName:@"Papyrus" size:17];

    [alertView show];

}

- (IBAction)backBtnPressed:(id)sender {
    
    [self.navigationController popViewControllerAnimated:YES];
}

- (IBAction)cameraBtnPressed:(id)sender {
    
    SIAlertView *alertView = [[SIAlertView alloc] initWithTitle:nil andMessage:@"Choose type"];
    [alertView addButtonWithTitle:@"Take Picture"
                             type:SIAlertViewButtonTypeDefault
                          handler:^(SIAlertView *alertView) {
                              
                              PickerViewController *pickerViewController = [self.storyboard instantiateViewControllerWithIdentifier:@"PickerViewController"];
                              [pickerViewController setIsMediaTypeMovie:NO];
                              [self presentViewController:pickerViewController animated:YES completion:nil];
                          }];
    [alertView addButtonWithTitle:@"Make Video"
                             type:SIAlertViewButtonTypeDefault
                          handler:^(SIAlertView *alertView) {
                              
                              PickerViewController *pickerViewController = [self.storyboard instantiateViewControllerWithIdentifier:@"PickerViewController"];
                              [pickerViewController setIsMediaTypeMovie:YES];
                              [self presentViewController:pickerViewController animated:YES completion:nil];
                          }];
    [alertView addButtonWithTitle:@"Cancel"
                             type:SIAlertViewButtonTypeCancel
                          handler:^(SIAlertView *alertView) {
                          }];
    
    [alertView show];

}

@end
