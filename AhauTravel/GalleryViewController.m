//
//  GalleryViewController.m
//  AhauTravel
//
//  Created by Rizwan on 5/22/13.
//  Copyright (c) 2013 com. All rights reserved.
//

#import <QuartzCore/QuartzCore.h>
#import <CoreMedia/CoreMedia.h>

#import "GalleryViewController.h"
#import "GalleryItemViewController.h"
#import "VideoViewController.h"
#import "GalleryVideoViewController.h"

@interface GalleryViewController () {
    
    NSArray *arrData;
    dispatch_queue_t backgroundQueue;
    BOOL isEditEnable;
}

@property (strong, nonatomic) IBOutlet UICollectionView *collectionView;
@property (strong, nonatomic) IBOutlet UILabel *lblHeading;
@property (strong, nonatomic) IBOutlet UIButton *btnEdit;

- (IBAction)backBtnPressed:(id)sender;
- (IBAction)editBtnPressed:(id)sender;

@end

@implementation GalleryViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    isEditEnable = NO;
    
    if (_isImageGallery) {
        [_lblHeading setText:@"Photo Album"];
    }
    else {
        [_lblHeading setText:@"Video Gallery"];
    }
    backgroundQueue = dispatch_queue_create("background.dispatch.queue", NULL); 
	// Do any additional setup after loading the view.
}

- (void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
    
    [[Singleton sharedSingleton] setDelegate:self];
    
    [self done];
}

- (void)viewWillDisappear:(BOOL)animated {
    [super viewWillDisappear:animated];
    
    [[Singleton sharedSingleton] setDelegate:nil];
}

- (NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section {
    
    return [arrData count];
}

- (UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath {
    
    UICollectionViewCell *cell = [collectionView dequeueReusableCellWithReuseIdentifier:@"GalleryCollectionCell" forIndexPath:indexPath];
    
    UIImageView *imgView = (UIImageView *)[cell viewWithTag:1000];
    
    
    UIButton *btn = (UIButton *)[cell viewWithTag:1001];
    
    if (isEditEnable) {
        [btn setHidden:NO];
    }
    else {
        [btn setHidden:YES];
    }
    
    if (_isImageGallery) {
        
        imgView.image = nil;
        dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^{
            
            Ahau *ahau = arrData[indexPath.row];
            UIImage * img = [UIImage imageWithContentsOfFile:[ahau name]];
            dispatch_sync(dispatch_get_main_queue(), ^{
                [imgView setImage:img];
            });
        });
        
        //dispatch_async(backgroundQueue, ^(void) {
        //});
//        Ahau *ahau = arrData[indexPath.row];
//        [imgView setImage:[UIImage imageWithContentsOfFile:[ahau name]]];
    }
    else {
        Ahau *ahau = arrData[indexPath.row];
        [self generateImageForImageView:imgView withUrl:[NSURL fileURLWithPath:[ahau name]]];
    }
    return cell;
}

- (void)collectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath:(NSIndexPath *)indexPath {
    
    [[Singleton sharedSingleton] setArrData:arrData];
    
    if (_isImageGallery) {
        
        [[Singleton sharedSingleton] setTotalPages:[arrData count]];
        
        GalleryItemViewController *galleryItemViewController = [self.storyboard instantiateViewControllerWithIdentifier:@"GalleryItemViewController"];
        [galleryItemViewController setSelectedIndex:indexPath.row];
        [self.navigationController pushViewController:galleryItemViewController animated:YES];
    }
    else {
        GalleryVideoViewController *galleryVideoViewController = [self.storyboard instantiateViewControllerWithIdentifier:@"GalleryVideoViewController"];
        [galleryVideoViewController setSelectedIndex:indexPath.row];
        [self.navigationController pushViewController:galleryVideoViewController animated:YES];
    }
}

- (UIStatusBarStyle)preferredStatusBarStyle
{
    return UIStatusBarStyleLightContent;
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (IBAction)backBtnPressed:(id)sender {
    
    [self.navigationController popViewControllerAnimated:YES];
}

- (void)done {
    
    AhauDB *ahauDb = [[AhauDB alloc] init];
    NSString *strMsg = nil;
    if (_isImageGallery) {
        strMsg = @"No Photo in the Album";
        arrData = [ahauDb getAhauImages];
    }
    else {
        strMsg = @"No Video in the Gallery";
        arrData = [ahauDb getAhauVideos];
    }
    if ([arrData count] == 0) {
        SIAlertView *alertView = [[SIAlertView alloc] initWithTitle:nil andMessage:strMsg];
        [alertView addButtonWithTitle:@"OK"
                                 type:SIAlertViewButtonTypeDefault
                              handler:^(SIAlertView *alertView) {
                              }];
        [alertView show];
    }
    [_collectionView reloadData];

}

- (UIImage*)loadImage:(NSURL *)url {
    
    AVURLAsset *asset = [[AVURLAsset alloc] initWithURL:url options:nil];
    AVAssetImageGenerator *generate = [[AVAssetImageGenerator alloc] initWithAsset:asset];
    generate.appliesPreferredTrackTransform = YES;
    NSError *err = NULL;
    CMTime time = CMTimeMake(1, 60);
    CGImageRef imgRef = [generate copyCGImageAtTime:time actualTime:NULL error:&err];
    NSLog(@"err==%@, imageRef==%@", err, imgRef);
    
    return [[UIImage alloc] initWithCGImage:imgRef];
    
}


-(void)generateImageForImageView:(UIImageView *)imageView withUrl:(NSURL *)url
{
    AVURLAsset *asset=[[AVURLAsset alloc] initWithURL:url options:nil];
    AVAssetImageGenerator *generator = [[AVAssetImageGenerator alloc] initWithAsset:asset];
    generator.appliesPreferredTrackTransform=TRUE;

    CMTime thumbTime = CMTimeMakeWithSeconds(0,30);
    
    AVAssetImageGeneratorCompletionHandler handler = ^(CMTime requestedTime, CGImageRef im, CMTime actualTime, AVAssetImageGeneratorResult result, NSError *error){
        if (result != AVAssetImageGeneratorSucceeded) {
            NSLog(@"couldn't generate thumbnail, error:%@", error);
        }
        UIImage *thumbImg=[UIImage imageWithCGImage:im];
        [imageView setImage:thumbImg];
    };
    
    CGSize maxSize = CGSizeMake(320, 180);
    generator.maximumSize = maxSize;
    [generator generateCGImagesAsynchronouslyForTimes:[NSArray arrayWithObject:[NSValue valueWithCMTime:thumbTime]] completionHandler:handler];
    
}

- (IBAction)editBtnPressed:(id)sender {
    
    isEditEnable = !isEditEnable;
    
    [_btnEdit setSelected:isEditEnable];
    
    [_collectionView reloadData];
    
//    SIAlertView *alertView = [[SIAlertView alloc] initWithTitle:nil andMessage:@"Choose type"];
//    [alertView addButtonWithTitle:@"Take Picture"
//                             type:SIAlertViewButtonTypeDefault
//                          handler:^(SIAlertView *alertView) {
//                              
//                              PickerViewController *pickerViewController = [self.storyboard instantiateViewControllerWithIdentifier:@"PickerViewController"];
//                              [pickerViewController setIsMediaTypeMovie:NO];
//                              [self presentViewController:pickerViewController animated:YES completion:nil];
//                          }];
//    [alertView addButtonWithTitle:@"Make Video"
//                             type:SIAlertViewButtonTypeDefault
//                          handler:^(SIAlertView *alertView) {
//                              
//                              PickerViewController *pickerViewController = [self.storyboard instantiateViewControllerWithIdentifier:@"PickerViewController"];
//                              [pickerViewController setIsMediaTypeMovie:YES];
//                              [self presentViewController:pickerViewController animated:YES completion:nil];
//                          }];
//    [alertView addButtonWithTitle:@"Cancel"
//                             type:SIAlertViewButtonTypeCancel
//                          handler:^(SIAlertView *alertView) {
//                          }];
//    
//    [alertView show];
}

- (IBAction)deleteBtnPressed:(id)sender forEvent:(UIEvent *)event {
    
    UITouch *touch = [[event allTouches] anyObject];
    CGPoint point = [touch locationInView:_collectionView];
    NSIndexPath *indexPath = [_collectionView indexPathForItemAtPoint:point];
    
    AhauDB *ahauDb = [[AhauDB alloc] init];
    [ahauDb deleteAhauImageWithName:[arrData[indexPath.row] name]];
    
    NSMutableArray *mutableArr = [arrData mutableCopy];
    [mutableArr removeObject:arrData[indexPath.row]];
    arrData = mutableArr;
    
    [_collectionView reloadData];
    
}
@end
