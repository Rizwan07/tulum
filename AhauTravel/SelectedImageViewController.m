//
//  TempleDiosViewController.m
//  AhauTravel
//
//  Created by Rizwan on 6/17/13.
//  Copyright (c) 2013 com. All rights reserved.
//

#import "SelectedImageViewController.h"
#import "TextLinkImageViewController.h"

@interface SelectedImageViewController () {
    
    NSArray *arrImages;
}

@property (strong, nonatomic) IBOutlet UIScrollView *scrollView;
- (IBAction)backBtnPressed:(id)sender;

@end

@interface SelectedImageViewController (UIPageViewController) <UIPageViewControllerDataSource>
@end

@implementation SelectedImageViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

//- (void)viewDidLoad
//{
//    [super viewDidLoad];
//    
//    arrImages = [_strImages componentsSeparatedByString:@","];
//    
//    [self setScrollContents];
//    
//	// Do any additional setup after loading the view.
//}
- (void)viewDidLoad
{
    
    NSLog(@"%s", __PRETTY_FUNCTION__);
    
    [super viewDidLoad];
    
    [_lblHeading setText:_strHeading];
    
    [[Singleton sharedSingleton] setArrSelectedImages:[_strImages componentsSeparatedByString:@","]];
    
    TextLinkImageViewController *textLinkScrollViewController = [TextLinkImageViewController photoViewControllerForPageIndex:0];
    if (textLinkScrollViewController != nil)
    {
        //[_photoScrollViewController.view setFrame:CGRectMake(0, 65, 320, self.view.frame.size.height-self.tabBarController.tabBar.frame.size.height-65-68)];
        
        // assign the first page to the pageViewController (our rootViewController)
        UIPageViewController *pageViewController = [[UIPageViewController alloc] initWithTransitionStyle:UIPageViewControllerTransitionStyleScroll navigationOrientation:UIPageViewControllerNavigationOrientationHorizontal options:nil];
        
        pageViewController.dataSource = self;
        
        [pageViewController setViewControllers:@[textLinkScrollViewController]
                                     direction:UIPageViewControllerNavigationDirectionForward
                                      animated:YES
                                    completion:NULL];
        
        if ([[UIDevice currentDevice] userInterfaceIdiom] == UIUserInterfaceIdiomPad) {
            [pageViewController.view setFrame:CGRectMake(0, 110, self.view.frame.size.width, self.view.frame.size.height-120)];
        }
        else {
            [pageViewController.view setFrame:CGRectMake(0, 55, self.view.frame.size.width, self.view.frame.size.height-60)];
        }
        [self addChildViewController:pageViewController];
        [self.view addSubview:pageViewController.view];
        [pageViewController didMoveToParentViewController:self];
        self.view.gestureRecognizers = pageViewController.gestureRecognizers;
    }
    
    UIView *view = [self.view viewWithTag:120];
    [self.view bringSubviewToFront:view];
    
	// Do any additional setup after loading the view.
}

- (UIViewController *)pageViewController:(UIPageViewController *)pvc viewControllerBeforeViewController:(TextLinkImageViewController *)vc
{
    
    NSUInteger index = vc.pageIndex;
    return [TextLinkImageViewController photoViewControllerForPageIndex:(index-1)];
}

- (UIViewController *)pageViewController:(UIPageViewController *)pvc viewControllerAfterViewController:(TextLinkImageViewController *)vc
{
    
    NSUInteger index = vc.pageIndex;
    return [TextLinkImageViewController photoViewControllerForPageIndex:(index+1)];
}

- (void)setScrollContents {
    
    int i=0;
    for (NSString *strName in arrImages) {
        ;
        UIImageView *imageView;
        //UIImageView *imageView = [[UIImageView alloc] initWithFrame:CGRectMake(i+5, 0, 310, self.view.frame.size.height-self.tabBarController.tabBar.frame.size.height-50)];
        if ([[[UIDevice currentDevice] systemVersion] doubleValue] < 7.0) {
            imageView = [[UIImageView alloc] initWithFrame:CGRectMake(i+10, 5, self.view.frame.size.width-20, _scrollView.frame.size.height-10)];
        }
        else {
            imageView = [[UIImageView alloc] initWithFrame:CGRectMake(i+10, 25, self.view.frame.size.width-20, _scrollView.frame.size.height-30)];
        }
        
        [imageView setImage:[UIImage imageNamed:strName]];
        [imageView setContentMode:UIViewContentModeScaleAspectFit];
        [_scrollView addSubview:imageView];
        i+=self.view.frame.size.width;
    }
    if ([arrImages count] > 1) {
        [_scrollView setContentSize:CGSizeMake(self.view.frame.size.width*2, _scrollView.frame.size.height)];
    }
}

- (UIStatusBarStyle)preferredStatusBarStyle
{
    return UIStatusBarStyleLightContent;
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (IBAction)backBtnPressed:(id)sender {
    [self.navigationController popViewControllerAnimated:YES];
}

@end
