//
//  UIImage+fixOrientation.h
//  Unum
//
//  Created by Rizwan on 5/29/13.
//
//

#import <UIKit/UIKit.h>

@interface UIImage (fixOrientation)

- (UIImage *)fixOrientation;

@end
