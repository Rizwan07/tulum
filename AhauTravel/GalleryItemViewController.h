//
//  GalleryItemViewController.h
//  AhauTravel
//
//  Created by Rizwan on 5/23/13.
//  Copyright (c) 2013 com. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <Social/Social.h>
#import <MessageUI/MessageUI.h>

@interface GalleryItemViewController : UIViewController<MFMailComposeViewControllerDelegate>

@property (nonatomic, assign) int selectedIndex;

- (IBAction)twitterBtnPressed:(id)sender;
- (IBAction)fbBtnPressed:(id)sender;
- (IBAction)emailBtnPressed:(id)sender;
- (IBAction)saveBtnPressed:(id)sender;
@end
