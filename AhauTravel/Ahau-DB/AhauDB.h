//
//  MemeODB.h
//  Memeo
//
//  Created by Rizwan on 4/1/13.
//  Copyright (c) 2013 Salsoft Technologies. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <sqlite3.h>
@interface AhauDB : NSObject{
    
    NSString *dbname,*dbPath;
    
}

- (void)addAhauWithname:(NSString *)name title:(NSString *)title;
//- (void)updateMemeOWithName:(NSString *)name index:(NSUInteger)index;
-(void)deleteAhauImageWithName:(NSString *)picsName;
- (NSArray *)getAhauImages;
- (NSArray *)getAhauVideos;
- (BOOL)CopyDatabaseIfNeeded;


@end
