//
//  MemeODB.m
//  Memeo
//
//  Created by Rizwan on 4/1/13.
//  Copyright (c) 2013 Salsoft Technologies. All rights reserved.
//

#import "AhauDB.h"
#import "Ahau.h"

@implementation AhauDB

-(id)init{
    
    
    dbname = @"AhauDb.sqlite";
    
    NSArray *docspaths=NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
    
    NSString *docsdir=[docspaths objectAtIndex:0];
    dbPath = [docsdir stringByAppendingPathComponent:dbname];
    //NSLog(@"Directries %@", dbPath);
    
    NSFileManager *filemgr=[NSFileManager defaultManager];
    
    if(![filemgr fileExistsAtPath:dbPath])
    {
        NSString *databasePathFromApp = [[[NSBundle mainBundle] resourcePath ] stringByAppendingPathComponent:dbname];
        [filemgr copyItemAtPath:databasePathFromApp toPath:dbPath error:nil];
    }
    
    
    return self;
}
-(BOOL)CopyDatabaseIfNeeded{
    
    NSFileManager *filemgr=[NSFileManager defaultManager];
    BOOL success=[filemgr fileExistsAtPath:dbPath];
    if(success)
    {
        NSLog(@"Success!!!");
        return YES;
    }
    else
    {
        NSString *databasePathFromApp = [[[NSBundle mainBundle] resourcePath ] stringByAppendingPathComponent:dbname];
        [filemgr copyItemAtPath:databasePathFromApp toPath:dbPath error:nil];
        NSLog(@"Fail!!!");
        return  NO;
    }
    
}

- (void)addAhauWithname:(NSString *)name title:(NSString *)title {

    sqlite3 *database;

    if(sqlite3_open([dbPath UTF8String], &database) == SQLITE_OK) {
                
        const char* sqlStatement = "INSERT INTO Ahau (fileName, title) VALUES (?,?)";
        sqlite3_stmt* compiledStatement;
        
        if( sqlite3_prepare_v2(database, sqlStatement, -1, &compiledStatement, NULL) == SQLITE_OK )
        {
            sqlite3_bind_text(compiledStatement,    1, [name UTF8String], -1, SQLITE_TRANSIENT);
            sqlite3_bind_text(compiledStatement,    2, [title UTF8String], -1, SQLITE_TRANSIENT);

            if (sqlite3_step(compiledStatement) == SQLITE_DONE) {
                NSLog(@"successfuly insert");
            }
            else {
                NSLog(@"not inserted");
            }
            
        }
        else {
            NSAssert1(0, @"Error while saving. '%s'", sqlite3_errmsg(database));
            NSLog( @"SaveBody: Failed from sqlite3_prepare_v2. Error is:  %s", sqlite3_errmsg(database) );
        }
        // Finalize and close database.
        sqlite3_finalize(compiledStatement);
        
    }
    sqlite3_close(database);
}

//- (void)updateMemeOWithName:(NSString *)name index:(NSUInteger)index {
//    
//    sqlite3 *database;
//    
//    if(sqlite3_open([dbPath UTF8String], &database) == SQLITE_OK)
//    {        
//        const char *sqlStatement1 = "UPDATE MemeO SET index = ? Where name = ?";
//        sqlite3_stmt *compiledStatement;
//        
//        if(sqlite3_prepare_v2(database, sqlStatement1, -1, &compiledStatement, NULL) == SQLITE_OK ) {
//                        
//            sqlite3_bind_text(compiledStatement, 1, [name UTF8String], -1, SQLITE_TRANSIENT);
//            sqlite3_bind_int(compiledStatement,  3, index);
//            
//            if (sqlite3_step(compiledStatement) == SQLITE_DONE) {
//                NSLog(@"successfuly updated1");
//            }
//            else {
//                NSLog(@"not updated");
//                NSAssert1(0, @"Error while updating. '%s'", sqlite3_errmsg(database));
//            }
//        }
//        else {
//            NSAssert1(0, @"Error while updating. '%s'", sqlite3_errmsg(database));
//            NSLog( @"SaveBody: Failed from sqlite3_prepare_v2. Error is:  %s", sqlite3_errmsg(database) );
//        }
//        sqlite3_reset(compiledStatement);
//        
//    }
//    sqlite3_close(database);
//}

-(void)deleteAhauImageWithName:(NSString *)picsName {
    sqlite3 *database;
    
    if(sqlite3_open([dbPath UTF8String], &database) == SQLITE_OK)
    {
        NSString *query;
        
        query = [NSString stringWithFormat:@"DELETE FROM Ahau WHERE fileName = \"%@\"",picsName];
        const char *sqlStatement = [query cStringUsingEncoding:NSUTF8StringEncoding];
        sqlite3_stmt *compiledStatement;
        if(sqlite3_prepare_v2(database, sqlStatement, -1, &compiledStatement, NULL) == SQLITE_OK)
        {
            if (sqlite3_step(compiledStatement) == SQLITE_DONE) {
                
                NSFileManager *filemgr=[NSFileManager defaultManager];
                NSError *error;
                [filemgr removeItemAtPath:picsName error:&error];
                
                NSLog(@"successfuly delete");
            }
            else {
                NSLog(@"Not deleted");
            }
        }
        sqlite3_finalize(compiledStatement);
    }
    sqlite3_close(database);
}

- (NSArray *)getAhauImages {
    
    NSMutableArray *itemsArr = [[NSMutableArray alloc]init];
    
    sqlite3 *database;
    
    if(sqlite3_open([dbPath UTF8String], &database) == SQLITE_OK)
    {
        NSString *query;
        
        query = @"SELECT * FROM Ahau WHERE title=\"image\"";
        const char *sqlStatement = [query cStringUsingEncoding:NSUTF8StringEncoding];
        sqlite3_stmt *compiledStatement;
        if(sqlite3_prepare_v2(database, sqlStatement, -1, &compiledStatement, NULL) == SQLITE_OK)
        {
            while(sqlite3_step(compiledStatement) == SQLITE_ROW)
            {
                Ahau *kp =[[Ahau alloc]init];
                
                @try {
                    kp.name     = [NSString stringWithUTF8String:(char *)sqlite3_column_text(compiledStatement, 0)];
                    kp.title    = [NSString stringWithUTF8String:(char *)sqlite3_column_text(compiledStatement, 1)];
                }
                @catch (NSException *exception) {
                    NSLog(@"error occured %@", [exception description]);
                }
                [itemsArr addObject:kp];
            }
        }
        sqlite3_finalize(compiledStatement);
    }
    sqlite3_close(database);
    
    return itemsArr;
    
}

- (NSArray *)getAhauVideos {
    
    NSMutableArray *itemsArr = [[NSMutableArray alloc]init];
    
    sqlite3 *database;
    
    if(sqlite3_open([dbPath UTF8String], &database) == SQLITE_OK)
    {
        NSString *query;
        
        query = @"SELECT * FROM Ahau WHERE title = \"video\"";
        const char *sqlStatement = [query cStringUsingEncoding:NSUTF8StringEncoding];
        sqlite3_stmt *compiledStatement;
        if(sqlite3_prepare_v2(database, sqlStatement, -1, &compiledStatement, NULL) == SQLITE_OK)
        {
            while(sqlite3_step(compiledStatement) == SQLITE_ROW)
            {
                Ahau *kp =[[Ahau alloc]init];
                
                @try {
                    kp.name     = [NSString stringWithUTF8String:(char *)sqlite3_column_text(compiledStatement, 0)];
                    kp.title    = [NSString stringWithUTF8String:(char *)sqlite3_column_text(compiledStatement, 1)];
                }
                @catch (NSException *exception) {
                    NSLog(@"error occured %@", [exception description]);
                }
                [itemsArr addObject:kp];
            }
        }
        sqlite3_finalize(compiledStatement);
    }
    sqlite3_close(database);
    
    return itemsArr;
}
@end
