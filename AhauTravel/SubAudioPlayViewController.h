//
//  SubAudioPlayViewController.h
//  AhauTravel
//
//  Created by Rizwan on 7/25/13.
//  Copyright (c) 2013 com. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface SubAudioPlayViewController : UIViewController

@property(nonatomic, assign) int selectedIndex;

@end
