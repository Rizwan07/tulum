//
//  TempleDiosViewController.h
//  AhauTravel
//
//  Created by Rizwan on 6/17/13.
//  Copyright (c) 2013 com. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface SelectedImageViewController : UIViewController

@property (strong, nonatomic) IBOutlet UILabel *lblHeading;
@property (strong, nonatomic) NSString *strImages;
@property (strong, nonatomic) NSString *strHeading;
@end
