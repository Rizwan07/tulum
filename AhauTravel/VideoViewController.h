//
//  VideoViewController.h
//  AhauTravel
//
//  Created by Rizwan on 5/23/13.
//  Copyright (c) 2013 com. All rights reserved.
//

#import "ViewController.h"
#import <AVFoundation/AVFoundation.h>

@interface VideoViewController : UIViewController

@property (nonatomic, strong) AVAudioPlayer *audioPlayer;

@property (nonatomic, strong) IBOutlet UILabel *lblHeading;
@property (nonatomic, assign) int selectedIndex;

@end
