//
//  GalleryViewController.h
//  AhauTravel
//
//  Created by Rizwan on 5/22/13.
//  Copyright (c) 2013 com. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface GalleryViewController : UIViewController<CallBackImageSaveDelegate>

@property (nonatomic, assign) BOOL isImageGallery;
- (IBAction)deleteBtnPressed:(id)sender forEvent:(UIEvent *)event;

@end
