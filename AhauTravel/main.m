
//
//  main.m
//  AhauTravel
//
//  Created by Rizwan on 5/20/13.
//  Copyright (c) 2013 com. All rights reserved.
//

#import <UIKit/UIKit.h>

#import "AppDelegate.h"

int main(int argc, char *argv[])
{
    @autoreleasepool {
        return UIApplicationMain(argc, argv, nil, NSStringFromClass([AppDelegate class]));
    }
}
