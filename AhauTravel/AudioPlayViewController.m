//
//  AncientHistoryViewController.m
//  AhauTravel
//
//  Created by Rizwan on 6/18/13.
//  Copyright (c) 2013 com. All rights reserved.
//

#import "AudioPlayViewController.h"
#import "BuildingFeatureViewController.h"
#import "TulumSiteHistoryViewController.h"
#import "SelectedImageViewController.h"

#import "HomeViewController.h"
#import "WhatIsThatViewController.h"
#import "TakeMeThereViewController.h"
#import "MoreViewController.h"

#import "SubAudioPlayViewController.h"

#import "NimbusAttributedLabel.h"

@interface AudioPlayViewController () {
    
    BOOL isVolumeOn;
    IBOutlet UISlider *slider;
    NSTimer *updateTimer;
    BOOL isPause;
    IBOutlet UIButton *btnPlayPause;
}

- (IBAction)fullscreenBtnPressed:(id)sender;
- (IBAction)previousBtnPressed:(id)sender;
- (IBAction)playBtnPressed:(id)sender;
- (IBAction)nextBtnPressed:(id)sender;
- (IBAction)volumeBtnPressed:(id)sender;
- (IBAction)sliderSlided:(id)sender;

- (IBAction)cameraBtnPressed:(id)sender;

@property (strong, nonatomic) IBOutlet UITextView *txtView;
@property (strong, nonatomic) UIImageView *imageViewTop;
@property (strong, nonatomic) IBOutlet UIView *viewButtons;
@property (strong, nonatomic) IBOutlet UIView *viewBig;
@property (strong, nonatomic) UIScrollView *scrollView;
@property (strong, nonatomic) NIAttributedLabel *lblText;
@property (strong, nonatomic) IBOutlet UILabel *lblHeading;

@property (nonatomic, weak) UIViewController *myParentViewController;

@end

@interface AudioPlayViewController () <AVAudioPlayerDelegate, NIAttributedLabelDelegate>
@end

@implementation AudioPlayViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    isVolumeOn = YES;
    isPause = NO;
    
//    [slider setMaximumTrackImage:[UIImage imageNamed:@"outer-volume-bar.png"] forState:UIControlStateNormal];
//    [slider setMinimumTrackImage:[UIImage imageNamed:@"inner-volume-bar.png"] forState:UIControlStateNormal];
//    [slider setThumbImage:[UIImage imageNamed:@"thumb-volume-control.png"] forState:UIControlStateNormal];

    self.myParentViewController = self.navigationController.viewControllers[0];
    
    [self setScrollViewProgramatically];
    
	// Do any additional setup after loading the view.
}

- (void)setScrollViewProgramatically {
    
    self.lblText = [[NIAttributedLabel alloc] initWithFrame:CGRectZero];
    [_lblText setDelegate:self];
    _lblText.numberOfLines = 0;
    _lblText.lineBreakMode = NSLineBreakByWordWrapping;
    _lblText.font = [UIFont systemFontOfSize:15];
    [_lblText setBackgroundColor:[UIColor clearColor]];
    [_lblText setTextColor:[UIColor whiteColor]];
    //self.label.autoDetectLinks = YES;
    //_lblText.dataDetectorTypes = NSTextCheckingAllSystemTypes;
    
    _lblText.textAlignment = NSTextAlignmentJustified;
    
    // When we enable defering the link detection is offloaded to a separate thread. This allows
    // the label to display its text immediately and redraw itself once the links have been
    // detected. For the block of text below this ends up displaying the text ~300ms faster on an
    // iPhone 4S than it otherwise would have.
    _lblText.deferLinkDetection = YES;
    
//    if ([[[UIDevice currentDevice] systemVersion] doubleValue] < 7.0) {
//        _scrollView = [[UIScrollView alloc] initWithFrame:CGRectMake(0, 50, 320, self.view.frame.size.height-55)];
//    }
//    else {
//        _scrollView = [[UIScrollView alloc] initWithFrame:CGRectMake(0, 70, 320, self.view.frame.size.height-75)];
//    }
    
    CGRect rectScroll = CGRectZero;
    rectScroll.size.width = self.view.frame.size.width;
    
    if ([[[UIDevice currentDevice] systemVersion] doubleValue] < 7.0) {
        if ([[UIDevice currentDevice] userInterfaceIdiom] == UIUserInterfaceIdiomPad) {
            _lblText.font = [UIFont systemFontOfSize:20];
            rectScroll.origin.y = 100;
        }
        else {
            rectScroll.origin.y = 50;
        }
        //_scrollView = [[UIScrollView alloc] initWithFrame:CGRectMake(0, 45, 320, self.view.frame.size.height-self.tabBarController.tabBar.frame.size.height-45)];
    }
    else {
        if ([[UIDevice currentDevice] userInterfaceIdiom] == UIUserInterfaceIdiomPad) {
            _lblText.font = [UIFont systemFontOfSize:20];
            rectScroll.origin.y = 120;
        }
        else {
            rectScroll.origin.y = 70;
        }
        
        //_scrollView = [[UIScrollView alloc] initWithFrame:CGRectMake(0, 65, 320, self.view.frame.size.height-self.tabBarController.tabBar.frame.size.height-65)];
    }
    rectScroll.size.height = self.view.frame.size.height-rectScroll.origin.y-5;
    
    _scrollView = [[UIScrollView alloc] initWithFrame:rectScroll];
    
    self.scrollView.autoresizingMask = UIViewAutoresizingFlexibleDimensions;
    self.scrollView.contentInset = UIEdgeInsetsMake(20, 0, 20, 0);
    [self.scrollView addSubview:_lblText];
    [self.view addSubview:self.scrollView];
    
    [self setAllViewsAndButtons];
    
    [self setLabelFrame];
    
}

- (void)setAllViewsAndButtons {
    
    int x1 = 0;
    if ([[UIDevice currentDevice] userInterfaceIdiom] == UIUserInterfaceIdiomPad) {
        x1 = 260;
    }
    else {
        x1 = 210;
    }

    int x = 0;
    
    int count = [self.navigationController.viewControllers count];
    if (count > 2) {
        UIViewController *vc = self.navigationController.viewControllers[count-2];
    
        if ([vc isKindOfClass:[TulumSiteHistoryViewController class]]) {
            if ([[UIDevice currentDevice] userInterfaceIdiom] == UIUserInterfaceIdiomPad) {
                x = 250;
            }
            else {
                x = 200;
            }
        }
        else {
            self.imageViewTop = [[UIImageView alloc] initWithFrame:CGRectMake(0, 0, self.view.frame.size.width, x1-10)];
            [_imageViewTop setContentMode:UIViewContentModeScaleAspectFit];
            [_scrollView addSubview:_imageViewTop];
            [self setTopPhoto];
        }
    }
    else {
        self.imageViewTop = [[UIImageView alloc] initWithFrame:CGRectMake(0, 0, self.view.frame.size.width, x1-10)];
        [_imageViewTop setContentMode:UIViewContentModeScaleAspectFit];
        [_scrollView addSubview:_imageViewTop];
        [self setTopPhoto];
    }
    
    UIView *view = [[UIView alloc] initWithFrame:CGRectMake(0, x1-x-10, self.view.frame.size.width, 75)];
    [view setBackgroundColor:[UIColor clearColor]];
    [_scrollView addSubview:view];
    
    UIImageView *imageViewBar = [[UIImageView alloc] initWithFrame:CGRectMake(0, 0, self.view.frame.size.width, 75)];
    [imageViewBar setContentMode:UIViewContentModeScaleAspectFit];
    [imageViewBar setImage:[UIImage imageNamed:@"audio-player-bg.png"]];
    [view addSubview:imageViewBar];

    slider = [[UISlider alloc] initWithFrame:CGRectMake(10, 8, 300, 23)];
    [slider setMaximumTrackImage:[UIImage imageNamed:@"outer-volume-bar.png"] forState:UIControlStateNormal];
    [slider setMinimumTrackImage:[UIImage imageNamed:@"inner-volume-bar.png"] forState:UIControlStateNormal];
    [slider setThumbImage:[UIImage imageNamed:@"thumb-volume-control.png"] forState:UIControlStateNormal];
    [slider addTarget:self action:@selector(sliderSlided:) forControlEvents:UIControlEventValueChanged];
    [view addSubview:slider];
    
    UIButton *btnprevious = [UIButton buttonWithType:UIButtonTypeCustom];
    [btnprevious setFrame:CGRectMake(100, 29, 30, 30)];
    [btnprevious setImage:[UIImage imageNamed:@"btn-audio-next.png"] forState:UIControlStateNormal];
    [btnprevious addTarget:self action:@selector(previousBtnPressed:) forControlEvents:UIControlEventTouchUpInside];
    [view addSubview:btnprevious];
    
    btnPlayPause = [UIButton buttonWithType:UIButtonTypeCustom];
    [btnPlayPause setFrame:CGRectMake(145, 29, 30, 30)];
    [btnPlayPause setImage:[UIImage imageNamed:@"btn-play.png"] forState:UIControlStateNormal];
    [btnPlayPause setImage:[UIImage imageNamed:@"btn-pause.png"] forState:UIControlStateSelected];
    [btnPlayPause addTarget:self action:@selector(playBtnPressed:) forControlEvents:UIControlEventTouchUpInside];
    [view addSubview:btnPlayPause];

    UIButton *btnNext = [UIButton buttonWithType:UIButtonTypeCustom];
    [btnNext setFrame:CGRectMake(190, 29, 30, 30)];
    [btnNext setImage:[UIImage imageNamed:@"btn-audio-previous.png"] forState:UIControlStateNormal];
    [btnNext addTarget:self action:@selector(nextBtnPressed:) forControlEvents:UIControlEventTouchUpInside];
    [view addSubview:btnNext];

    UIButton *btnVolume = [UIButton buttonWithType:UIButtonTypeCustom];
    [btnVolume setFrame:CGRectMake(280, 29, 30, 30)];
    [btnVolume setImage:[UIImage imageNamed:@"btn-audio-volume-on.png"] forState:UIControlStateNormal];
    [btnVolume setImage:[UIImage imageNamed:@"btn-audio-volume-off.png"] forState:UIControlStateSelected];
    [btnVolume addTarget:self action:@selector(volumeBtnPressed:) forControlEvents:UIControlEventTouchUpInside];
    [view addSubview:btnVolume];
    
    
    if ([[UIDevice currentDevice] userInterfaceIdiom] == UIUserInterfaceIdiomPad) {
        [view setFrame:CGRectMake(0, x1-x-10, self.view.frame.size.width, 150)];
        [imageViewBar setFrame:CGRectMake(0, 0, self.view.frame.size.width, 150)];
        
        [slider setFrame:       CGRectMake(15, 22, 738, 34)];
        [btnprevious setFrame:  CGRectMake(270, 72, 58, 58)];
        [btnPlayPause setFrame: CGRectMake(355, 72, 58, 58)];
        [btnNext setFrame:      CGRectMake(440, 72, 58, 58)];
        [btnVolume setFrame:    CGRectMake(650, 72, 58, 58)];

    }

}

- (void)setTopPhoto {
    
    NSDictionary *dic = _arrAudioFile[_selectedIndex];
    NSString *imageName = [dic objectForKey:@"mapImage"];
    UIImage *image = [UIImage imageNamed:imageName];
    [_imageViewTop setImage: image ? image : [UIImage imageNamed:@"no-photo.png"]];
}

- (UIStatusBarStyle)preferredStatusBarStyle
{
    return UIStatusBarStyleLightContent;
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (IBAction)backBtnPressed:(id)sender {
    
    [self audioStopped];
    [self.navigationController popViewControllerAnimated:YES];
}

- (IBAction)fullscreenBtnPressed:(id)sender {
    
}

- (IBAction)previousBtnPressed:(id)sender {
    
    if (_selectedIndex > 0) {
        
        [_audioPlayer stop];
        _selectedIndex -= 1;
        [self playBtnPressed:btnPlayPause];
    }
}

- (IBAction)playBtnPressed:(id)sender {
    
    if (![self canPlay]) {
        
        SIAlertView *alertView = [[SIAlertView alloc] initWithTitle:nil andMessage:@"Audio is already Running"];
        [alertView addButtonWithTitle:@"OK"
                                 type:SIAlertViewButtonTypeDefault
                              handler:^(SIAlertView *alertView) {
                              }];
        
        [alertView show];
        
        return;
    }
    [self setTopPhoto];
    UIButton *btn = (UIButton *)sender;
    
    if (_audioPlayer && [_audioPlayer isPlaying]) {
        [btn setSelected:NO];
        [_audioPlayer pause];
        isPause = YES;
        [self audioPaused];
    }
    else if (_audioPlayer && isPause) {
        [btn setSelected:YES];
        isPause = NO;
        [_audioPlayer play];
    }
    else {
        [btn setSelected:YES];
        isPause = NO;
        [slider setMinimumValue:0.0];
        [slider setValue:0.0];
        
        NSString *strResourchPath = [[NSBundle mainBundle] resourcePath];
        
        [self setLabelFrame];
        
        NSURL *url = [NSURL fileURLWithPath:[NSString stringWithFormat:@"%@/%@", strResourchPath, [_arrAudioFile[_selectedIndex] objectForKey:@"fileName"]]];
        
        NSError *error;
        if ([_myParentViewController isKindOfClass:[MoreViewController class]]) {
            [Singleton sharedSingleton].audioPlayerMore = [[AVAudioPlayer alloc] initWithContentsOfURL:url error:&error];
            self.audioPlayer = [Singleton sharedSingleton].audioPlayerMore;
        }
        else {
            self.audioPlayer = [[AVAudioPlayer alloc] initWithContentsOfURL:url error:&error];
        }
        //_audioPlayer.numberOfLoops = -1;
        [_audioPlayer setDelegate:self];
        if (isVolumeOn) {
            [_audioPlayer setVolume:1.0];
        }
        else {
            [_audioPlayer setVolume:0.0];
        }
        if (_audioPlayer == nil)
            NSLog(@"%@", [error description]);
        else
            [_audioPlayer play];
        updateTimer = [NSTimer scheduledTimerWithTimeInterval:.01 target:self selector:@selector(updateCurrentTime) userInfo:_audioPlayer repeats:YES];
        
        [slider setMaximumValue:[_audioPlayer duration]];
        
    }
}


- (BOOL)canPlay {
    
    if ([[Singleton sharedSingleton] audioPlayer] != nil && [[[Singleton sharedSingleton] audioPlayer] isPlaying]) {
        NSLog(@"audio player %@", [[Singleton sharedSingleton] audioPlayer]);
        return NO;
    }
    
    if ([_myParentViewController isKindOfClass:[HomeViewController class]]) {
        if ([[Singleton sharedSingleton] wAudioRunning] || [[Singleton sharedSingleton] tAudioRunning] || [[Singleton sharedSingleton] aAudioRunning]) {
            return NO;
        }
        else {
            [[Singleton sharedSingleton] setHAudioRunning:YES];
        }
    }
    else if ([_myParentViewController isKindOfClass:[WhatIsThatViewController class]])  {
        if ([[Singleton sharedSingleton] hAudioRunning] || [[Singleton sharedSingleton] tAudioRunning] || [[Singleton sharedSingleton] aAudioRunning]) {
            return NO;
        }
        else {
            [[Singleton sharedSingleton] setWAudioRunning:YES];
        }
    }
    else if ([_myParentViewController isKindOfClass:[TakeMeThereViewController class]]) {
        if ([[Singleton sharedSingleton] hAudioRunning] || [[Singleton sharedSingleton] wAudioRunning] || [[Singleton sharedSingleton] aAudioRunning]) {
            return NO;
        }
        else {
            [[Singleton sharedSingleton] setTAudioRunning:YES];
        }
    }
    else {
        if ([[Singleton sharedSingleton] hAudioRunning] || [[Singleton sharedSingleton] wAudioRunning] || [[Singleton sharedSingleton] tAudioRunning]) {
            return NO;
        }
        else {
            [[Singleton sharedSingleton] setAAudioRunning:YES];
        }
    }

//    if ([[Singleton sharedSingleton] hAudioRunning] || [[Singleton sharedSingleton] tAudioRunning]) {
//        //
//        return NO;
//    }
//    else {
//        [[Singleton sharedSingleton] setAAudioRunning:YES];
//    }
    return YES;
}

- (void)audioPaused {
    
    if ([_myParentViewController isKindOfClass:[HomeViewController class]]) {
        [[Singleton sharedSingleton] setHAudioRunning:NO];
    }
    else if ([_myParentViewController isKindOfClass:[WhatIsThatViewController class]]) {
        [[Singleton sharedSingleton] setWAudioRunning:NO];
    }
    else if ([_myParentViewController isKindOfClass:[TakeMeThereViewController class]]) {
        [[Singleton sharedSingleton] setTAudioRunning:NO];
    }
    else {
        [[Singleton sharedSingleton] setAAudioRunning:NO];
    }
}

- (void)audioStopped {
    
    [updateTimer invalidate];
    [_audioPlayer stop];

    [self audioPaused];
    
    //[[Singleton sharedSingleton] setAAudioRunning:NO];
}

- (IBAction)nextBtnPressed:(id)sender {
    
    if ([_arrAudioFile count] > _selectedIndex+1) {
        isPause = NO;
        [_audioPlayer stop];
        _selectedIndex += 1;
        [self playBtnPressed:btnPlayPause];
    }
}

- (IBAction)volumeBtnPressed:(id)sender {
    
    UIButton *btn = (UIButton *)sender;
    
    if (isVolumeOn) {
        [btn setSelected:YES];
        [_audioPlayer setVolume:0.0];
    }
    else {
        [btn setSelected:NO];
        [_audioPlayer setVolume:1.0];
    }
    isVolumeOn = !isVolumeOn;

}

- (IBAction)sliderSlided:(id)sender {
    
    [_audioPlayer setCurrentTime:slider.value];
    
    //_audioPlayer set
}

- (void)updateCurrentTime
{
	[self updateCurrentTimeForPlayer:_audioPlayer];
}

-(void)updateCurrentTimeForPlayer:(AVAudioPlayer *)p
{
	//currentTime.text = [NSString stringWithFormat:@"%d:%02d", (int)p.currentTime / 60, (int)p.currentTime % 60, nil];
	slider.value = p.currentTime;
}

- (void)_layoutLabel {
    
    int x = 0;
    
    int count = [self.navigationController.viewControllers count];
    if (count > 2) {
        
        UIViewController *vc = self.navigationController.viewControllers[count-2];
        
        if ([vc isKindOfClass:[TulumSiteHistoryViewController class]]) {
            if ([[UIDevice currentDevice] userInterfaceIdiom] == UIUserInterfaceIdiomPad) {
                x = 250;
            }
            else {
                x = 200;
            }
        }
    }
    
    if ([[UIDevice currentDevice] userInterfaceIdiom] == UIUserInterfaceIdiomPad) {
        CGSize size = [_lblText sizeThatFits:CGSizeMake(718, CGFLOAT_MAX)];
        _lblText.frame = CGRectMake(25, 420-x, 718, size.height);
        _scrollView.contentSize = CGSizeMake(748, size.height+420-x);
    }
    else {
        CGSize size = [_lblText sizeThatFits:CGSizeMake(290, CGFLOAT_MAX)];
        _lblText.frame = CGRectMake(15, 280-x, 290, size.height);
        _scrollView.contentSize = CGSizeMake(300, size.height+280-x);
    }

}

- (void)setLabelFrame {
    
//    UIFont *font = [UIFont boldSystemFontOfSize:15];
    //NSString *str = [[_arrAudioFile[_selectedIndex] objectForKey:@"imageLink"] objectAtIndex:0];
    
    NSString *str = [_arrAudioFile[_selectedIndex] objectForKey:@"heading"];
    if ([str rangeOfString:@"---"].location != NSNotFound) {
        NSArray *arr = [str componentsSeparatedByString:@"---"];
        [_lblHeading setText:arr[1]];
    }
    else {
        [_lblHeading setText:str];
    }
    
    [_lblText setText:[_arrAudioFile[_selectedIndex] objectForKey:@"text"]];

    NSArray *array = [_arrAudioFile[_selectedIndex] objectForKey:@"imageLink"];
    
    for (NSDictionary *aDic in array) {
        
        NSString *imageName = [NSString stringWithFormat:@"____%@",[aDic objectForKey:@"title"]];
        NSString *combineImageName = [[aDic objectForKey:@"imageName"] stringByAppendingString:imageName];
        NSString *data = [aDic objectForKey:@"data"];
        
        if (data != nil) {
            
            [_lblText addLink:[NSURL fileURLWithPath:combineImageName] range:[_lblText.text rangeOfString:data]];
        }
    }
    
    [self _layoutLabel];
    
//    CGRect labelFrame = _lblText.frame;
//    labelFrame.size = [_lblText.text sizeWithFont:_lblText.font constrainedToSize:CGSizeMake(300, CGFLOAT_MAX) lineBreakMode:_lblText.lineBreakMode];
//    labelFrame.origin = CGPointMake(0, 0);
//    _lblText.frame = labelFrame;
//    [_lblText sizeToFit];
//    [_scrollView setContentOffset:CGPointMake(0, 0)];
//    [_scrollView setContentSize:CGSizeMake(300, labelFrame.size.height)];
}


#pragma mark - NIAttributedLabelDelegate

- (void)attributedLabel:(NIAttributedLabel *)attributedLabel didSelectTextCheckingResult:(NSTextCheckingResult *)result atPoint:(CGPoint)point {
    
    NSString *strImageData = [result.URL.relativeString stringByReplacingPercentEscapesUsingEncoding:NSUTF8StringEncoding];
    
    NSLog(@"relative String %@", strImageData);
    
    NSArray *arrImageData = [strImageData componentsSeparatedByString:@"____"];
    
    if ([strImageData length]) {
        
        if ([[arrImageData[1] lowercaseString] rangeOfString:@"south gate 1"].location != NSNotFound ||
            [[arrImageData[1] lowercaseString] rangeOfString:@"diving god"].location != NSNotFound ||
            [[arrImageData[1] lowercaseString] rangeOfString:@"great wall"].location != NSNotFound ||
            [[arrImageData[1] lowercaseString] rangeOfString:@"north gate 2"].location != NSNotFound ||
            [[arrImageData[1] lowercaseString] rangeOfString:@"backside of castillo"].location != NSNotFound ||
            [[arrImageData[1] lowercaseString] rangeOfString:@"castillo’s construction"].location != NSNotFound ||
            [[arrImageData[1] lowercaseString] rangeOfString:@"frescos  plan"].location != NSNotFound) {
            
            int index = 0;
            
            if ([[arrImageData[1] lowercaseString] rangeOfString:@"south gate 1"].location != NSNotFound) {
                index = 0;
            }
            if ([[arrImageData[1] lowercaseString] rangeOfString:@"diving god"].location != NSNotFound) {
                index = 1;
            }
            if ([[arrImageData[1] lowercaseString] rangeOfString:@"great wall"].location != NSNotFound) {
                index = 2;
            }
            if ([[arrImageData[1] lowercaseString] rangeOfString:@"backside of castillo"].location != NSNotFound) {
                index = 3;
            }
            if ([[arrImageData[1] lowercaseString] rangeOfString:@"castillo’s construction"].location != NSNotFound) {
                index = 4;
            }
            if ([[arrImageData[1] lowercaseString] rangeOfString:@"north gate 2"].location != NSNotFound) {
                index = 7;
            }
            if ([[arrImageData[1] lowercaseString] rangeOfString:@"frescos  plan"].location != NSNotFound) {
                index = 8;
            }
            
            SubAudioPlayViewController *subAudioPlayViewController = [self.storyboard instantiateViewControllerWithIdentifier:@"SubAudioPlayViewController"];
            [subAudioPlayViewController setSelectedIndex:index];
            [self.navigationController pushViewController:subAudioPlayViewController animated:YES];
        }
        else {
            SelectedImageViewController *selectedImageViewController = [self.storyboard instantiateViewControllerWithIdentifier:@"SelectedImageViewController"];
            [selectedImageViewController setStrImages:arrImageData[0]];
            [selectedImageViewController setStrHeading:arrImageData[1]];
            [self.navigationController pushViewController:selectedImageViewController animated:YES];
        }
    }
    else {
        
        SIAlertView *alertView = [[SIAlertView alloc] initWithTitle:nil andMessage:@"Image not found"];
        [alertView addButtonWithTitle:@"OK"
                                 type:SIAlertViewButtonTypeDefault
                              handler:^(SIAlertView *alertView) {
                              }];
        
        [alertView show];
    }
}

#pragma mark --
#pragma mark -- AVAudioPlayer Delegates

- (void)audioPlayerDidFinishPlaying:(AVAudioPlayer *)player successfully:(BOOL)flag {
    
    [btnPlayPause setSelected:NO];
    [self audioStopped];
}

- (void)audioPlayerDecodeErrorDidOccur:(AVAudioPlayer *)player error:(NSError *)error {
    
    [btnPlayPause setSelected:NO];
    [self audioStopped];
}

- (IBAction)cameraBtnPressed:(id)sender {
    
    SIAlertView *alertView = [[SIAlertView alloc] initWithTitle:nil andMessage:@"Choose type"];
    [alertView addButtonWithTitle:@"Take Picture"
                             type:SIAlertViewButtonTypeDefault
                          handler:^(SIAlertView *alertView) {
                              
                              PickerViewController *pickerViewController = [self.storyboard instantiateViewControllerWithIdentifier:@"PickerViewController"];
                              [pickerViewController setIsMediaTypeMovie:NO];
                              [self presentViewController:pickerViewController animated:YES completion:nil];
                          }];
    [alertView addButtonWithTitle:@"Make Video"
                             type:SIAlertViewButtonTypeDefault
                          handler:^(SIAlertView *alertView) {
                              
                              PickerViewController *pickerViewController = [self.storyboard instantiateViewControllerWithIdentifier:@"PickerViewController"];
                              [pickerViewController setIsMediaTypeMovie:YES];
                              [self presentViewController:pickerViewController animated:YES completion:nil];
                          }];
    [alertView addButtonWithTitle:@"Cancel"
                             type:SIAlertViewButtonTypeCancel
                          handler:^(SIAlertView *alertView) {
                          }];
    
    [alertView show];
}

@end
