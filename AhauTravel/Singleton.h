//
//  Singleton.h
//  AhauTravel
//
//  Created by Rizwan on 6/10/13.
//  Copyright (c) 2013 com. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <Foundation/Foundation.h>
#import <CoreLocation/CoreLocation.h>
#import <AVFoundation/AVFoundation.h>

@protocol CallBackImageSaveDelegate <NSObject>

- (void)done;

@end

@interface Singleton : NSObject <UIImagePickerControllerDelegate, UINavigationControllerDelegate>

@property (nonatomic, assign)int totalPages;
@property (nonatomic, strong)NSArray *arrData;
@property (nonatomic, strong)NSArray *arrSelectedImages;
@property (nonatomic, strong)AVAudioPlayer *audioPlayer;
@property (nonatomic, strong)AVAudioPlayer *audioPlayerMore;

@property (nonatomic, assign)CLLocationCoordinate2D locationCoordinate;

@property (nonatomic, assign)id<CallBackImageSaveDelegate> delegate;

@property (nonatomic, assign)BOOL isMovieMediaType;

@property BOOL hAudioRunning;
@property BOOL tAudioRunning;
@property BOOL aAudioRunning;
@property BOOL wAudioRunning;

+(Singleton *)sharedSingleton;

@end
