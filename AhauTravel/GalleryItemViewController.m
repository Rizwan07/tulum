//
//  GalleryItemViewController.m
//  AhauTravel
//
//  Created by Rizwan on 5/23/13.
//  Copyright (c) 2013 com. All rights reserved.
//

#import "GalleryItemViewController.h"
#import "PhotoScrollViewController.h"

@interface GalleryItemViewController ()

@property (nonatomic, strong) PhotoScrollViewController *photoScrollViewController;

@property (strong, nonatomic) IBOutlet UIView *viewInputTItle;
@property (strong, nonatomic) IBOutlet UIView *viewShare;
@property (strong, nonatomic) IBOutlet UITextField *txtFieldRename;
@property (strong, nonatomic) IBOutlet UILabel *lblHeader;

- (IBAction)backBtnPressed:(id)sender;

- (IBAction)refereshBtnPressed:(id)sender;
- (IBAction)renameBtnPressed:(id)sender;

- (IBAction)cameraBtnPressed:(id)sender;

@end

@interface GalleryItemViewController (UIPageViewController)<UIPageViewControllerDataSource, UIPageViewControllerDelegate>
@end

@implementation GalleryItemViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    
    NSLog(@"%s", __PRETTY_FUNCTION__);
    
    [super viewDidLoad];

    _photoScrollViewController = [PhotoScrollViewController photoViewControllerForPageIndex:_selectedIndex];
    if (_photoScrollViewController != nil)
    {
        //[_photoScrollViewController.view setFrame:CGRectMake(0, 65, 320, self.view.frame.size.height-self.tabBarController.tabBar.frame.size.height-65-68)];

        // assign the first page to the pageViewController (our rootViewController)
        UIPageViewController *pageViewController = [[UIPageViewController alloc] initWithTransitionStyle:UIPageViewControllerTransitionStyleScroll navigationOrientation:UIPageViewControllerNavigationOrientationHorizontal options:nil];
        
        pageViewController.dataSource = self;
        pageViewController.delegate = self;
        
        [pageViewController setViewControllers:@[_photoScrollViewController]
                                     direction:UIPageViewControllerNavigationDirectionForward
                                      animated:YES
                                    completion:NULL];
        
        //[pageViewController.view setFrame:CGRectMake(0, 55, self.view.frame.size.width, self.view.frame.size.height-self.tabBarController.tabBar.frame.size.height-60)];
        
        if ([[UIDevice currentDevice] userInterfaceIdiom] == UIUserInterfaceIdiomPad) {
            [pageViewController.view setFrame:CGRectMake(0, 110, self.view.frame.size.width, self.view.frame.size.height-self.tabBarController.tabBar.frame.size.height-120-40)];
        }
        else {
            [pageViewController.view setFrame:CGRectMake(0, 55, self.view.frame.size.width, self.view.frame.size.height-self.tabBarController.tabBar.frame.size.height-60)];
        }

        [self addChildViewController:pageViewController];
        [self.view addSubview:pageViewController.view];
        [pageViewController didMoveToParentViewController:self];
        self.view.gestureRecognizers = pageViewController.gestureRecognizers;
        [self setImageLabel];
    }
    
    UIView *view = [self.view viewWithTag:120];
    [self.view bringSubviewToFront:view];
    
	// Do any additional setup after loading the view.
}

- (UIViewController *)pageViewController:(UIPageViewController *)pvc viewControllerBeforeViewController:(PhotoScrollViewController *)vc
{
    
//    NSLog(@"before index %d", vc.pageIndex);
    
    NSUInteger index = vc.pageIndex;
    _selectedIndex = index;
    [self setImageLabel];
    _photoScrollViewController = [PhotoScrollViewController photoViewControllerForPageIndex:(index-1)];
    return _photoScrollViewController;
}

- (UIViewController *)pageViewController:(UIPageViewController *)pvc viewControllerAfterViewController:(PhotoScrollViewController *)vc
{
//    NSLog(@"after index %d", vc.pageIndex);
    
    NSUInteger index = vc.pageIndex;
    _selectedIndex = index;
    [self setImageLabel];
    _photoScrollViewController = [PhotoScrollViewController photoViewControllerForPageIndex:(index+1)];
    return _photoScrollViewController;
}

- (void)pageViewController:(UIPageViewController *)pageViewController didFinishAnimating:(BOOL)finished previousViewControllers:(NSArray *)previousViewControllers transitionCompleted:(BOOL)completed {
    
//    NSLog(@"finish %d, completed %d", finished, completed);
}

- (void)setImageLabel {
    
    [_lblHeader setText:[NSString stringWithFormat:@"Image_%02d", _selectedIndex+1]];
}

- (UIStatusBarStyle)preferredStatusBarStyle
{
    return UIStatusBarStyleLightContent;
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (IBAction)backBtnPressed:(id)sender {
    [self.navigationController popViewControllerAnimated:YES];
}


- (IBAction)refereshBtnPressed:(id)sender {
    
    [UIView transitionWithView:_viewInputTItle duration:1.0 options:UIViewAnimationOptionTransitionCrossDissolve animations:^{
        [_viewInputTItle setHidden:YES];
    } completion:^(BOOL finished) {
        [UIView transitionWithView:_viewShare duration:1.0 options:UIViewAnimationOptionTransitionCrossDissolve animations:^{
            [_viewShare setHidden:NO];
        } completion:^(BOOL finished) {
            
        }];
    }];
}

- (IBAction)renameBtnPressed:(id)sender {
    
}

- (IBAction)twitterBtnPressed:(id)sender {
    
    if ([SLComposeViewController isAvailableForServiceType:SLServiceTypeTwitter]) {
        SLComposeViewController *tweetSheet = [SLComposeViewController composeViewControllerForServiceType:SLServiceTypeTwitter];
        [tweetSheet setInitialText:@"Enter Message :"];
        
        NSString *strImagePath = [[[[Singleton sharedSingleton] arrData] objectAtIndex:_selectedIndex] name];
        
        UIImage *img = [UIImage imageWithContentsOfFile:strImagePath];
        [tweetSheet addImage:img];
//        [tweetSheet setCompletionHandler:^(SLComposeViewControllerResult result) {
//            
//            switch (result) {
//                case SLComposeViewControllerResultCancelled:
//                    NSLog(@"Post Canceled");
//                    break;
//                case SLComposeViewControllerResultDone:
//                    //[self titleOKBtnPressed:nil];
//                    break;
//                    
//                default:
//                    break;
//            }
//        }];
        
        [self presentViewController:tweetSheet animated:YES completion:nil];
    }
    else {
        
        SIAlertView *alertView = [[SIAlertView alloc] initWithTitle:@"Sorry" andMessage:@"You can't send a tweet right now, make sure your device has an internet connection and have at least one twitter account setup."];
        [alertView setMessageFont:[UIFont fontWithName:@"Papyrus" size:18]];
        [alertView addButtonWithTitle:@"OK"
                                 type:SIAlertViewButtonTypeDefault
                              handler:^(SIAlertView *alertView) {
                              }];
        
        [alertView show];
    }

}

- (IBAction)fbBtnPressed:(id)sender {
    
    if ([SLComposeViewController isAvailableForServiceType:SLServiceTypeFacebook]) {
        SLComposeViewController *fbSheet = [SLComposeViewController composeViewControllerForServiceType:SLServiceTypeFacebook];
        [fbSheet setInitialText:@"Enter Message :"];
        
        NSString *strImagePath = [[[[Singleton sharedSingleton] arrData] objectAtIndex:_selectedIndex] name];
        
        UIImage *img = [UIImage imageWithContentsOfFile:strImagePath];
        [fbSheet addImage:img];
//        [fbSheet setCompletionHandler:^(SLComposeViewControllerResult result) {
//            
//            switch (result) {
//                case SLComposeViewControllerResultCancelled:
//                    NSLog(@"Post Canceled");
//                    break;
//                case SLComposeViewControllerResultDone:
//                    //[self setNextTab];
//                    //[self titleOKBtnPressed:nil];
//                    break;
//                    
//                default:
//                    break;
//            }
//        }];
        [self presentViewController:fbSheet animated:YES completion:nil];
        
    }
    else {
        SIAlertView *alertView = [[SIAlertView alloc] initWithTitle:@"Sorry" andMessage:@"You can't share an image right now, make sure your device has an internet connection and have at least one facebook account setup."];
        [alertView setMessageFont:[UIFont fontWithName:@"Papyrus" size:18]];
        [alertView addButtonWithTitle:@"OK"
                                 type:SIAlertViewButtonTypeDefault
                              handler:^(SIAlertView *alertView) {
                              }];
        
        [alertView show];
    }
    
}

- (IBAction)emailBtnPressed:(id)sender {
    
    if ([MFMailComposeViewController canSendMail]) {
        // Initialization
        MFMailComposeViewController *vc = [[MFMailComposeViewController alloc] init];
        
        [vc setSubject:@"Famous Photo"];

        NSString *strImagePath = [[[[Singleton sharedSingleton] arrData] objectAtIndex:_selectedIndex] name];
        
        UIImage *img = [UIImage imageWithContentsOfFile:strImagePath];
        NSData *imageData = UIImageJPEGRepresentation(img, 1);
        [vc addAttachmentData:imageData mimeType:@"image/png" fileName:@"memeo.jpg"];
        
        [vc setMailComposeDelegate:self];
        
        [self presentViewController:vc animated:YES completion:nil];
    }
}

- (IBAction)saveBtnPressed:(id)sender {
    
    NSString *strImagePath = [[[[Singleton sharedSingleton] arrData] objectAtIndex:_selectedIndex] name];

    UIImage *img = [UIImage imageWithContentsOfFile:strImagePath];
    UIImageWriteToSavedPhotosAlbum(img, self, @selector(image:didFinishSavingWithError:contextInfo:), nil);

}

#pragma mark --
#pragma mark UIImageWriteToSavedPhotosAlbum Delegate

- (void)image:(UIImage *)image didFinishSavingWithError:(NSError *)error contextInfo: (void *) contextInfo {
    
    if (error != nil) {
        NSLog(@"Error %@", error.localizedDescription);
    }
    else {
        
        SIAlertView *alertView = [[SIAlertView alloc] initWithTitle:nil andMessage:@"Saved to photo album"];
        [alertView addButtonWithTitle:@"OK"
                                 type:SIAlertViewButtonTypeDefault
                              handler:^(SIAlertView *alertView) {
                              }];
        
        [alertView show];
    }
}



#pragma mark --
#pragma mark MFMailComposerViewController Delegate

- (void)mailComposeController:(MFMailComposeViewController*)controller didFinishWithResult:(MFMailComposeResult)result error:(NSError*)error
{
    // Notifies users about errors associated with the interface
    switch (result)
    {
        case MFMailComposeResultCancelled:
            break;
        case MFMailComposeResultSaved:
            break;
        case MFMailComposeResultSent:
        {
            [controller dismissViewControllerAnimated:YES completion:nil];
            break;
        }
        case MFMailComposeResultFailed:
            break;
        default:
        {
            
            SIAlertView *alertView = [[SIAlertView alloc] initWithTitle:@"Email" andMessage:@"Sending Failed - Unknown Error :-("];
            [alertView addButtonWithTitle:@"OK"
                                     type:SIAlertViewButtonTypeDefault
                                  handler:^(SIAlertView *alertView) {
                                  }];
            
            [alertView show];
        }
            break;
    }
    [self dismissViewControllerAnimated:YES completion:nil];
}


#pragma mark --
#pragma mark UITextField Delegate

- (BOOL)textFieldShouldReturn:(UITextField *)textField {
    [textField resignFirstResponder];
    return YES;
}

- (IBAction)cameraBtnPressed:(id)sender {
    
    SIAlertView *alertView = [[SIAlertView alloc] initWithTitle:nil andMessage:@"Choose type"];
    [alertView addButtonWithTitle:@"Take Picture"
                             type:SIAlertViewButtonTypeDefault
                          handler:^(SIAlertView *alertView) {
                              
                              PickerViewController *pickerViewController = [self.storyboard instantiateViewControllerWithIdentifier:@"PickerViewController"];
                              [pickerViewController setIsMediaTypeMovie:NO];
                              [self presentViewController:pickerViewController animated:YES completion:nil];
                          }];
    [alertView addButtonWithTitle:@"Make Video"
                             type:SIAlertViewButtonTypeDefault
                          handler:^(SIAlertView *alertView) {
                              
                              PickerViewController *pickerViewController = [self.storyboard instantiateViewControllerWithIdentifier:@"PickerViewController"];
                              [pickerViewController setIsMediaTypeMovie:YES];
                              [self presentViewController:pickerViewController animated:YES completion:nil];
                          }];
    [alertView addButtonWithTitle:@"Cancel"
                             type:SIAlertViewButtonTypeCancel
                          handler:^(SIAlertView *alertView) {
                          }];
    
    [alertView show];
}

@end
