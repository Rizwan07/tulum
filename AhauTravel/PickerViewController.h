//
//  PickerViewController.h
//  AhauTravel
//
//  Created by Rizwan on 7/2/13.
//  Copyright (c) 2013 com. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface PickerViewController : UIImagePickerController

@property (assign, nonatomic) BOOL isMediaTypeMovie;

- (IBAction)backBtnPressed:(id)sender;
@end
