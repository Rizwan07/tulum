//
//  MoreViewController.m
//  AhauTravel
//
//  Created by Rizwan on 5/20/13.
//  Copyright (c) 2013 com. All rights reserved.
//

#import "MoreViewController.h"
#import "TulumSiteHistoryViewController.h"
#import "BuildingFeatureViewController.h"
#import "GalleryViewController.h"

#import "BibliographyViewController.h"

#import "CreditNHelpViewController.h"

#import <iAd/iAd.h>

@interface MoreViewController () {
    
    NSArray *_arrData;
    IBOutlet UITableView *_tableView;
}

- (IBAction)cameraBtnPressed:(id)sender;
- (IBAction)mayaCalenderAppClicked:(id)sender;

@end

@implementation MoreViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    _arrData = [NSArray arrayWithObjects:@"Tulum Site History", @"Buildings / Features", @"Photo Album", @"Video Gallery", @"Credits", @"Bibliography", nil];
    //_arrData = [NSArray arrayWithObjects:@"Tulum Site History", @"Buildings / Features", @"My Photo Album", @"Settings", nil];
    [_tableView setBackgroundView:nil];
    
	// Do any additional setup after loading the view.
}

- (void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
    [[[Singleton sharedSingleton] audioPlayer] stop];
    [Singleton sharedSingleton].audioPlayer = nil;
    [[[Singleton sharedSingleton] audioPlayerMore] stop];
    [Singleton sharedSingleton].audioPlayerMore = nil;
}


- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    
    return [_arrData count];
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"MoreCellIdentifier"];
    
    UILabel *lbl = (UILabel *)[cell viewWithTag:1000];
    [lbl setText:_arrData[indexPath.row]];
    
    return cell;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    
    UITableViewCell *cell = [tableView cellForRowAtIndexPath:indexPath];
    [cell setSelectionStyle:UITableViewCellSelectionStyleNone];
    
    switch (indexPath.row) {
        case 0: {
            TulumSiteHistoryViewController *tulumSiteHistoryViewController = [self.storyboard instantiateViewControllerWithIdentifier:@"TulumSiteHistoryViewController"];
            [self.navigationController pushViewController:tulumSiteHistoryViewController animated:YES];
        }
            break;
        case 1: {
            BuildingFeatureViewController *buildingFeatureViewController = [self.storyboard instantiateViewControllerWithIdentifier:@"BuildingFeatureViewController"];
            [self.navigationController pushViewController:buildingFeatureViewController animated:YES];
        }
            break;
        case 2:
        case 3: {
            GalleryViewController *galleryViewController = [self.storyboard instantiateViewControllerWithIdentifier:@"GalleryViewController"];
            [galleryViewController setIsImageGallery:(indexPath.row == 2)];
            [self.navigationController pushViewController:galleryViewController animated:YES];
            
        }
            break;
        case 4: {
            CreditNHelpViewController *creditNHelpViewController = [self.storyboard instantiateViewControllerWithIdentifier:@"CreditNHelpViewController"];
            [self.navigationController pushViewController:creditNHelpViewController animated:YES];
        }
            break;
        case 5: {
            BibliographyViewController *bibliographyViewController = [self.storyboard instantiateViewControllerWithIdentifier:@"BibliographyViewController"];
            [self.navigationController pushViewController:bibliographyViewController animated:YES];
        }
            break;
        default:
            break;
    }
}

- (UIStatusBarStyle)preferredStatusBarStyle
{
    return UIStatusBarStyleLightContent;
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (IBAction)cameraBtnPressed:(id)sender {
    
    SIAlertView *alertView = [[SIAlertView alloc] initWithTitle:nil andMessage:@"Choose type"];
    [alertView addButtonWithTitle:@"Take Picture"
                             type:SIAlertViewButtonTypeDefault
                          handler:^(SIAlertView *alertView) {
                              
                              PickerViewController *pickerViewController = [self.storyboard instantiateViewControllerWithIdentifier:@"PickerViewController"];
                              [pickerViewController setIsMediaTypeMovie:NO];
                              [self presentViewController:pickerViewController animated:YES completion:nil];
                          }];
    [alertView addButtonWithTitle:@"Make Video"
                             type:SIAlertViewButtonTypeDefault
                          handler:^(SIAlertView *alertView) {
                              
                              PickerViewController *pickerViewController = [self.storyboard instantiateViewControllerWithIdentifier:@"PickerViewController"];
                              [pickerViewController setIsMediaTypeMovie:YES];
                              [self presentViewController:pickerViewController animated:YES completion:nil];
                          }];
    [alertView addButtonWithTitle:@"Cancel"
                             type:SIAlertViewButtonTypeCancel
                          handler:^(SIAlertView *alertView) {
                          }];
    
    [alertView show];
}

- (IBAction)mayaCalenderAppClicked:(id)sender {
    
    [[UIApplication sharedApplication] openURL:[NSURL URLWithString:@"https://itunes.apple.com/pk/app/maya-calendar/id305642777?mt=8"]];
}

#pragma mark --
#pragma mark -- ADBannerView Delegates

- (void)bannerView:(ADBannerView *)banner didFailToReceiveAdWithError:(NSError *)error {
    
}

@end
