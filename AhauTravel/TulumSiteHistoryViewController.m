//
//  TulumSiteHistoryViewController.m
//  AhauTravel
//
//  Created by Rizwan on 5/22/13.
//  Copyright (c) 2013 com. All rights reserved.
//

#import "TulumSiteHistoryViewController.h"
#import "AudioPlayViewController.h"

@interface TulumSiteHistoryViewController () {
    
    NSArray *_arrData;
    IBOutlet UITableView *_tableView;
    
    AudioPlayViewController *audioPlayViewController;
}
- (IBAction)backBtnPressed:(id)sender;

- (IBAction)cameraBtnPressed:(id)sender;

@end

@implementation TulumSiteHistoryViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    //_arrData = [NSArray arrayWithObjects:@"General Ancient History", @"Story of Discovery", @"Research History", nil];
    
    NSString *path = [[NSBundle mainBundle] pathForResource:@"AudioFileTulum" ofType:@"plist"];
    _arrData = [NSArray arrayWithContentsOfFile:path];
    
    //_arrData = [NSArray arrayWithObjects:@"Contact Period History", @"History of Research and Archaeology", @"History Chronology", nil];
    [_tableView setBackgroundView:nil];
	// Do any additional setup after loading the view.
}

- (void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
    
    if (audioPlayViewController) {
        audioPlayViewController.audioPlayer = nil;
        [[Singleton sharedSingleton] setAAudioRunning:NO];
    }

}

- (IBAction)backBtnPressed:(id)sender {
    [self.navigationController popViewControllerAnimated:YES];
}
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    
    return [_arrData count];
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"TulumCellIdentifier"];
    
    UILabel *lbl = (UILabel *)[cell viewWithTag:1000];
    //[lbl setText:[_arrData[indexPath.row] objectForKey:@"heading"]];
    
    NSString *str = [_arrData[indexPath.row] objectForKey:@"heading"];
    if ([str rangeOfString:@"---"].location != NSNotFound) {
        NSArray *arr = [str componentsSeparatedByString:@"---"];
        [lbl setText:arr[0]];
    }
    else {
        [lbl setText:str];
    }
    
    return cell;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    
    UITableViewCell *cell = [tableView cellForRowAtIndexPath:indexPath];
    [cell setSelectionStyle:UITableViewCellSelectionStyleNone];
    
    audioPlayViewController = [self.storyboard instantiateViewControllerWithIdentifier:@"AudioPlayViewControllerFull"];
    [audioPlayViewController setArrAudioFile:_arrData];
    [audioPlayViewController setSelectedIndex:indexPath.row];
    [self.navigationController pushViewController:audioPlayViewController animated:YES];
    
}

- (UIStatusBarStyle)preferredStatusBarStyle
{
    return UIStatusBarStyleLightContent;
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (IBAction)cameraBtnPressed:(id)sender {
    
    SIAlertView *alertView = [[SIAlertView alloc] initWithTitle:nil andMessage:@"Choose type"];
    [alertView addButtonWithTitle:@"Take Picture"
                             type:SIAlertViewButtonTypeDefault
                          handler:^(SIAlertView *alertView) {
                              
                              PickerViewController *pickerViewController = [self.storyboard instantiateViewControllerWithIdentifier:@"PickerViewController"];
                              [pickerViewController setIsMediaTypeMovie:NO];
                              [self presentViewController:pickerViewController animated:YES completion:nil];
                          }];
    [alertView addButtonWithTitle:@"Make Video"
                             type:SIAlertViewButtonTypeDefault
                          handler:^(SIAlertView *alertView) {
                              
                              PickerViewController *pickerViewController = [self.storyboard instantiateViewControllerWithIdentifier:@"PickerViewController"];
                              [pickerViewController setIsMediaTypeMovie:YES];
                              [self presentViewController:pickerViewController animated:YES completion:nil];
                          }];
    [alertView addButtonWithTitle:@"Cancel"
                             type:SIAlertViewButtonTypeCancel
                          handler:^(SIAlertView *alertView) {
                          }];
    
    [alertView show];
}

@end
