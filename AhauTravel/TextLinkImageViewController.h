//
//  TextLinkImageViewController.h
//  AhauTravel
//
//  Created by Rizwan on 7/23/13.
//  Copyright (c) 2013 com. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface TextLinkImageViewController : UIViewController

+ (TextLinkImageViewController *)photoViewControllerForPageIndex:(NSUInteger)pageIndex;

- (NSInteger)pageIndex;

@end
