//
//  BuildingFeatureViewController.m
//  AhauTravel
//
//  Created by Rizwan on 5/23/13.
//  Copyright (c) 2013 com. All rights reserved.
//

#import "BuildingFeatureViewController.h"
#import "AudioPlayViewController.h"

@interface BuildingFeatureViewController () {
    
    NSArray *_arrData;
    AudioPlayViewController *audioPlayViewController;
}

- (IBAction)backBtnPressed:(id)sender;

- (IBAction)cameraBtnPressed:(id)sender;

@end

@implementation BuildingFeatureViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    //_arrData = [NSArray arrayWithObjects:@"Pyramid", @"Greate Palace", @"Temple Dios", @"Frescos Temple", nil];
    
    /*_arrData = [NSArray arrayWithObjects:
                @"Bathrooms",
                @"The stairs down to the beach",
                @"The original beach port",
                @"The Backside of the Castillo",
                @"Analysis of the Castillo’s construction",
                @"The Diving God",
                @"Frescos",
                @"The INAH gift shop",
                @"The Great Wall",
                @"The Inner Enclosure",
                @"The Structures Along Tulum’s Main Street",
                @"North Gate 1",
                @"North Gate 2",
                @"North Wall",
                @"South Gate 1",
                @"South Gate 2",
                @"South Wall",
                @"South wall Stairs",
                @"Stela 1",
                @"Stela 2",
                @"The Castillo (structure 1)",
                @"Structure 2",
                @"Structure 3",
                @"Structure 4",
                @"The Temple of the Diving God (Structure 5)",
                @"Structure 6",
                @"Structure 7",
                @"Structure 8",
                @"The Temple of the Initial Series (Structure 9)",
                @"Structure 10",
                @"Structure 11",
                @"Structure 12",
                @"Structure 13",
                @"Structure 14",
                @"Structure 15",
                @"The Temple of the Frescos (Structure 16)",
                @"Structure 17",
                @"Structure 18",
                @"Structure 19",
                @"The House of the Chultun (Structure 20)",
                @"The House of the Columns (Structure 21)",
                @"Structure 22",
                @"Structure 23",
                @"Structure 24",
                @"The House of the Halach Winik (Structure 25)",
                @"Structure 26",
                @"Structure 27",
                @"Structure 28",
                @"Structure 29",
                @"Structure 30",
                @"Structure 31",
                @"Structure 32",
                @"Structure 33",
                @"Structure 34",
                @"The Cenote House",
                @"Structure 36",
                @"The Miniature Shrines (Structures 39 – 41)",
                @"Structure 42",
                @"Structure 43",
                @"Structure 44",
                @"The Temple of the Wind (Structure 45)",
                @"Structure 46",
                @"Structure 47",
                @"Structure 49",
                @"Structure 50",
                @"Structure 51",
                @"Structure 52",
                @"Structure 53",
                @"Structure 54",
                @"Structure 60",
                @"Structure 61",
                @"Structure 62",
                @"Structure 63",
                @"Structure 64",
                @"Structure 65",
                @"Structure 66",
                @"Structure 67",
                @"Structure 68",
                @"The INAH ticket booth",
                @"The Watch Tower",
                @"The West Gate",
                @"West Wall", nil];*/
    
    NSString *path = [[NSBundle mainBundle] pathForResource:@"AudioFileList" ofType:@"plist"];
    _arrData = [NSArray arrayWithContentsOfFile:path];
    
    UITableView *tableView = (UITableView *)[self.view viewWithTag:123];
    [tableView setBackgroundView:nil];
    
	// Do any additional setup after loading the view.
}

- (void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
    
    if (audioPlayViewController) {
        audioPlayViewController.audioPlayer = nil;
        [[Singleton sharedSingleton] setAAudioRunning:NO];
    }
    
}

- (UIStatusBarStyle)preferredStatusBarStyle
{
    return UIStatusBarStyleLightContent;
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (IBAction)backBtnPressed:(id)sender {
    
    [self.navigationController popViewControllerAnimated:YES];
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    
    return [_arrData count];
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"BuidingCellIdentifier"];
    
    UILabel *lbl = (UILabel *)[cell viewWithTag:1000];
    
    NSString *str = [_arrData[indexPath.row] objectForKey:@"heading"];
    if ([str rangeOfString:@"---"].location != NSNotFound) {
        NSArray *arr = [str componentsSeparatedByString:@"---"];
        [lbl setText:arr[0]];
    }
    else {
        [lbl setText:str];
    }

    //[lbl setText:[_arrData[indexPath.row] objectForKey:@"heading"]];
    
    return cell;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    
    UITableViewCell *cell = [tableView cellForRowAtIndexPath:indexPath];
    [cell setSelectionStyle:UITableViewCellSelectionStyleNone];
    
    audioPlayViewController = [self.storyboard instantiateViewControllerWithIdentifier:@"AudioPlayViewControllerFull"];
    [audioPlayViewController setArrAudioFile:_arrData];
    [audioPlayViewController setSelectedIndex:indexPath.row];
    [self.navigationController pushViewController:audioPlayViewController animated:YES];
}

- (IBAction)cameraBtnPressed:(id)sender {
    
    SIAlertView *alertView = [[SIAlertView alloc] initWithTitle:nil andMessage:@"Choose type"];
    [alertView addButtonWithTitle:@"Take Picture"
                             type:SIAlertViewButtonTypeDefault
                          handler:^(SIAlertView *alertView) {
                              
                              PickerViewController *pickerViewController = [self.storyboard instantiateViewControllerWithIdentifier:@"PickerViewController"];
                              [pickerViewController setIsMediaTypeMovie:NO];
                              [self presentViewController:pickerViewController animated:YES completion:nil];
                          }];
    [alertView addButtonWithTitle:@"Make Video"
                             type:SIAlertViewButtonTypeDefault
                          handler:^(SIAlertView *alertView) {
                              
                              PickerViewController *pickerViewController = [self.storyboard instantiateViewControllerWithIdentifier:@"PickerViewController"];
                              [pickerViewController setIsMediaTypeMovie:YES];
                              [self presentViewController:pickerViewController animated:YES completion:nil];
                          }];
    [alertView addButtonWithTitle:@"Cancel"
                             type:SIAlertViewButtonTypeCancel
                          handler:^(SIAlertView *alertView) {
                          }];
    
    [alertView show];
}

@end
