//
//  PhotoScrollViewController.m
//  TestScroll
//
//  Created by Rizwan on 5/23/13.
//  Copyright (c) 2013 com. All rights reserved.
//

#import "PhotoScrollViewController.h"

@interface PhotoScrollViewController () {
    NSUInteger _pageIndex;
    UIImageView *_imageView;
    UIScrollView *_scrollView;
}

@end

@interface PhotoScrollViewController (UIScrollView) <UIScrollViewDelegate>
@end

@implementation PhotoScrollViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    [self setMyScrollView];
	// Do any additional setup after loading the view.
}

- (void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
    
}

- (void)viewDidAppear:(BOOL)animated {
    [super viewDidAppear:animated];
}

-(void) setMyScrollView
{
    
    if ([[[UIDevice currentDevice] systemVersion] doubleValue] < 7.0) {
        _scrollView = [[UIScrollView alloc] initWithFrame:CGRectMake(10, 0, self.view.frame.size.width-20, self.view.frame.size.height-55-60-49)]; //size.height-20)];
    }
    else {
        _scrollView = [[UIScrollView alloc] initWithFrame:CGRectMake(10, 20, self.view.frame.size.width-20, self.view.frame.size.height-55-60-49-20)]; //size.height-20)];
    }

    if ([[UIDevice currentDevice] userInterfaceIdiom] == UIUserInterfaceIdiomPad) {
        CGRect rect = _scrollView.frame;
        rect.size.height -= 80;
        [_scrollView setFrame:rect];
    }

	_scrollView.backgroundColor = [UIColor blackColor];
	_scrollView.delegate = self;
	
    NSString *strImageName = [NSString stringWithFormat:@"frame-border.png"];
  
    _imageView = [[UIImageView alloc] initWithImage:[UIImage imageNamed:strImageName]];
    //[_imageView setFrame:CGRectMake(0, 0, 300, self.view.frame.size.height-self.tabBarController.tabBar.frame.size.height-65-68)];
    _scrollView.contentSize = _imageView.image.size;
	[_scrollView addSubview:_imageView];
    
    NSString *strImagePath = [[[[Singleton sharedSingleton] arrData] objectAtIndex:_pageIndex] name];
    
    UIImageView *imgView = [[UIImageView alloc] initWithImage:[UIImage imageWithContentsOfFile:strImagePath]];
    [imgView setFrame:CGRectMake(10, 10, _imageView.image.size.width-20, _imageView.image.size.height-30)];
    [imgView setContentMode:UIViewContentModeScaleAspectFit];
    [_imageView addSubview:imgView];
    
	_scrollView.minimumZoomScale = _scrollView.frame.size.width / _imageView.frame.size.width;
	_scrollView.maximumZoomScale = 2.0;
	[_scrollView setZoomScale:_scrollView.minimumZoomScale];
	[self.view addSubview:_scrollView];
    
    
    UITapGestureRecognizer *dblRecognizer = [[UITapGestureRecognizer alloc] initWithTarget:self
                                                                                    action:@selector(handleDoubleTapFrom:)];
    [dblRecognizer setNumberOfTapsRequired:2];
    [_scrollView addGestureRecognizer:dblRecognizer];

}

- (void)handleDoubleTapFrom:(UITapGestureRecognizer *)recognizer {
    
    if (_scrollView.zoomScale > _scrollView.minimumZoomScale)
    {
        [_scrollView setZoomScale:_scrollView.minimumZoomScale animated:YES];
    }
    else
    {
        CGRect zoomRect = [self zoomRectForScale:_scrollView.maximumZoomScale withCenter:[recognizer locationInView:recognizer.view]];
        [_scrollView zoomToRect:zoomRect animated:YES];
    }
}

- (CGRect)zoomRectForScale:(float)scale withCenter:(CGPoint)center {
    
    CGRect zoomRect;
    
    zoomRect.size.height = [_imageView frame].size.height / scale;
    zoomRect.size.width  = [_imageView frame].size.width  / scale;
    
    center = [_imageView convertPoint:center fromView:_scrollView];
    
    zoomRect.origin.x    = center.x - ((zoomRect.size.width / 2.0));
    zoomRect.origin.y    = center.y - ((zoomRect.size.height / 2.0));
    
    return zoomRect;
}

+ (PhotoScrollViewController *)photoViewControllerForPageIndex:(NSUInteger)pageIndex
{
    if (pageIndex < [[Singleton sharedSingleton] totalPages])
    {
        return [[self alloc] initWithPageIndex:pageIndex];
    }
    return nil;
}

- (id)initWithPageIndex:(NSInteger)pageIndex;
{
    self = [super initWithNibName:nil bundle:nil];
    if (self)
    {
        _pageIndex = pageIndex;
    }
    return self;
}

- (NSInteger)pageIndex
{
    return _pageIndex;
}

- (void)loadView
{
    [super loadView];
}

// (this can also be defined in Info.plist via UISupportedInterfaceOrientations)
- (NSUInteger)supportedInterfaceOrientations
{
    return UIInterfaceOrientationMaskAllButUpsideDown;
}

- (CGRect)centeredFrameForScrollView:(UIScrollView *)scroll andUIView:(UIView *)rView {
	CGSize boundsSize = scroll.bounds.size;
    CGRect frameToCenter = rView.frame;
    
    // center horizontally
    if (frameToCenter.size.width < boundsSize.width) {
        frameToCenter.origin.x = (boundsSize.width - frameToCenter.size.width) / 2;
    }
    else {
        frameToCenter.origin.x = 0;
    }
    
    // center vertically
    if (frameToCenter.size.height < boundsSize.height) {
        frameToCenter.origin.y = (boundsSize.height - frameToCenter.size.height) / 2;
    }
    else {
        frameToCenter.origin.y = 0;
    }
	
	return frameToCenter;
}


#pragma mark --
#pragma mark -- UIScrollView Delegate

- (void)scrollViewDidZoom:(UIScrollView *)scrollV {
	_imageView.frame = [self centeredFrameForScrollView:scrollV andUIView:_imageView];
}

- (UIView *)viewForZoomingInScrollView:(UIScrollView *)scrollView {
	return _imageView;
}

- (UIStatusBarStyle)preferredStatusBarStyle
{
    return UIStatusBarStyleLightContent;
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

@end
