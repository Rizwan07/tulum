//
//  HomeViewController.m
//  AhauTravel
//
//  Created by Rizwan on 5/20/13.
//  Copyright (c) 2013 com. All rights reserved.
//

#import "HomeViewController.h"
#import "TakeMeThereViewController.h"
#import "AudioPlayViewController.h"
#import <CoreLocation/CoreLocation.h>

@interface HomeViewController () {
    
    CLLocationManager *_locationManager;
    UIScrollView *_scrollView;
    UIImageView *_imageView;
    UIImageView *_imageViewDot;
    NSArray *_arrFrames;
    NSArray *_arrStructure;
    
    CLLocationCoordinate2D coordTopLeft;
    CLLocationCoordinate2D coordTopRight;
    CLLocationCoordinate2D coordBottomRight;
    CLLocationCoordinate2D coordBottomLeft;
    
    int mapWidth;
    int mapHeight;
    
    
    float arrCoordinates[100][2];
    BOOL isFirstTime;
    BOOL isCommingFirstTime;
    int tags[80];
    IBOutlet UIView *viewAlert;
    
    AudioPlayViewController *audioPlayViewController;
    
    int count;
    IBOutlet UIButton *btnForward;
    IBOutlet UIButton *btnBackward;
    
    SIAlertView *alertInfo;
    NSTimer *timerAlert;
}

- (IBAction)alertOKBtnPressed:(id)sender;
- (IBAction)cameraBtnPressed:(id)sender;

- (IBAction)backwardBtnPressed:(id)sender;
- (IBAction)forwardBtnPressed:(id)sender;
@end

@interface HomeViewController (CoreLocation)<CLLocationManagerDelegate>
@end
@interface HomeViewController (UIScrollView)<UIScrollViewDelegate>
@end

@implementation HomeViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    [[NSUserDefaults standardUserDefaults] setObject:@"Bathroom men" forKey:@"structure"];
    [[NSUserDefaults standardUserDefaults] setBool:YES forKey:@"showMessage"];
    [[NSUserDefaults standardUserDefaults] synchronize];
    
    UINavigationController *navController = self.tabBarController.viewControllers[2];
    TakeMeThereViewController *viewController = (TakeMeThereViewController *)(navController.viewControllers[0]);
    [viewController setFinalLocation:CLLocationCoordinate2DMake(20.215057, -87.430628)];
    
    isCommingFirstTime = YES;
    
    NSLog(@"Frame %@", NSStringFromCGRect(self.view.frame));
    
    [[Singleton sharedSingleton] setHAudioRunning:NO];
    [[Singleton sharedSingleton] setTAudioRunning:NO];
    [[Singleton sharedSingleton] setAAudioRunning:NO];
    
    // left 130
    // right 800
    // top 100
    // bottom 1520
    
    // CGRectMake(130, 100, 570, 1420);
    
    // hori 3390
    // vert 4347
    
    int tag[80] = {20, // 1
        21, // 2
        22, // 3
        23, // 4
        24, // 5
        25, // 6
        26, // 7
        27, // 8
        28, // 9
        29, // 10
        30, // 11
        31, // 12
        32, // 13
        33, // 14
        34, // 15
        35, // 16
        36, // 17
        37, // 18
        38, // 19
        39, // 20
        40, // 21
        41, // 22
        42, // 23
        43, // 24
        44, // 25
        45, // 26
        46, // 27
        47, // 28
        48, // 29
        49, // 30
        50, // 31
        51, // 32
        52, // 33
        53, // 34
        54, // 35
        55, // 36
        56, // 37
        56, // 38
        56, // 39
        56, // 40
        56, // 41
        57, // 42
        58, // 43
        59, // 44
        60, // 45
        61, // 46
        62, // 47
        62, // 48
        63, // 49
        64, // 50
        65, // 51
        66, // 52
        67, // 53
        68, // 54
        69, // 55
        69, // 56
        70, // 60
        71, // 61
        72, // 62
        73, // 63
        74, // 64
        75, // 65
        76, // 66
        77, // 67
        78, // 68
        7, // gift shop
        79, // tichet booth
        0, // bathroom women
        0, // bathroom men
        12, // North gate 2
        11, // North gate 1
        80, // West gate
        15, // South gate 2
        14, // South gate 1
        8, // Lesser Wall
        1, // Beach access
        2 // Beach port
        
        };
    
    for (int i = 0; i < 80; i ++) {
        tags[i] = tag[i];
    }
    
    float temp[100][2] = {{20.214653, -87.428772}, // str1 NE
        //{20.214367, -87.428983}, // str1 SE
        {20.214747, -87.428858}, // str2
        {20.214411, -87.428997}, // str3
        {20.214820, -87.428861}, // str4
        {20.214833, -87.428939}, // str5 DivingDog
        {20.214872, -87.429064}, // str6
        {20.214728, -87.429033}, // str7
        {20.214681, -87.429158}, // str8
        {20.214369, -87.429081}, // str9 initial series
        {20.214478, -87.429228}, // str10
        {20.214592, -87.429286}, // str11
        {20.214531, -87.429375}, // str12
        {20.214439, -87.429550}, // str13
        {20.214597, -87.429642}, // str14
        {20.214792, -87.429686}, // str15
        {20.214801, -87.429414}, // str16 frescos center
        {20.214717, -87.429503}, // str17
        {20.214789, -87.429472}, // str18
        {20.214697, -87.429850}, // str19
        {20.214842, -87.429811}, // str20
        {20.214944, -87.429444}, // str21 hcolomns
        {20.215225, -87.429447}, // str22
        {20.215131, -87.429386}, // str23
        {20.215194, -87.429156}, // str24
        {20.215281, -87.429290}, // str25
        {20.215006, -87.429775}, // str26
        {20.215125, -87.429717}, // str27
        {20.215178, -87.429667}, // str28
        {20.215358, -87.429514}, // str29
        {20.215503, -87.429428}, // str30
        {20.216222, -87.429111}, // str31
        {20.216319, -87.429075}, // str32
        {20.216061, -87.429006}, // str33
        {20.216197, -87.428944}, // str34
        {20.216172, -87.428283}, // str35 Cenout house
        {20.216150, -87.428181}, // str36
        {20.215843, -87.428155}, // str37
        {20.215843, -87.428155}, // str38
        {20.215636, -87.428250}, // str39
        {20.215661, -87.428231}, // str40
        {20.215664, -87.428200}, // str41
        {20.215544, -87.428194}, // str42
        {20.215536, -87.428169}, // str43
        {20.215531, -87.428147}, // str44
        {20.215458, -87.428181}, // str45 
        {20.214333, -87.429406}, // str46
        {20.214261, -87.429481}, // str47
        {20.214039, -87.429638}, // str48 empty space
        {20.213656, -87.430211}, // str49
        {20.213067, -87.429678}, // str50
        {20.213356, -87.429469}, // str51
        {20.213361, -87.429300}, // str52
        {20.213242, -87.429333}, // str53
        {20.213161, -87.429300}, // str54
        {20.216681, -87.429475}, // str55
        {20.213272, -87.430814}, // str56
        {20.215633, -87.428506}, // str60
        {20.216286, -87.428758}, // str61
        {20.215883, -87.429089}, // str62
        {20.215778, -87.429144}, // str63
        {20.215594, -87.429158}, // str64
        {20.215625, -87.429233}, // str65
        {20.215447, -87.429233}, // str66
        {20.215989, -87.429211}, // str67
        {20.215728, -87.429342}, // str68
        {20.215433, -87.430372}, // gift shop
        {20.215218, -87.430475}, // tichet booth
        {20.215156, -87.430598}, // bathroom women
        {20.215057, -87.430628}, // bathroom men
        {20.216489, -87.428961}, // North gate 2
        {20.216216, -87.428178}, // North gate 1
        {20.214789, -87.430306}, // West gate
        {20.213089, -87.430506}, // South gate 2
        {20.212836, -87.429583}, // South gate 1
        {20.213114, -87.430892}, // Lesser Wall
        {20.214150, -87.429022}, // Beach access
        {20.215078, -87.428517}, // Beach port
    };
    
    for (int i=0; i<100; i++) {
        for (int j=0; j<2; j++) {
            arrCoordinates[i][j] = temp[i][j];
        }
    }
    
    // upper    left    20°12'59.10" N  87°25'46.54" W
    // upper    right   20°12'56.07" N  87°25'40.54" W
    // bottom   right   20°12'46.21" N  87°25'46.85" W
    // bottom   left    20°12'48.26" N  87°25'49.49" W
    
    
    // top left     24.879662,67.059896
    // top right    24.879662,67.075732
    // bottom right 24.864984,67.075732
    // bottom left  24.864984,67.059896
    
    // Tulum final coordinates before
    // top left     20.217146,-87.431267
    // top right    20.217123,-87.427745
    // bottom right 20.212810,-87.428068
    // bottom left  20.212804,-87.430946
    
    // Tulum final coordinates after
    // top left      20.217145,-87.431157
    // top right     20.217123,-87.427745
    // bottom right  20.212786,-87.427783
    // bottom left   20.212791,-87.430845
    
//    //coordTopLeft    = CLLocationCoordinate2DMake(20.217135,-87.431158);
//    coordTopLeft    = CLLocationCoordinate2DMake(20.217135,-87.430158);
//    coordTopRight   = CLLocationCoordinate2DMake(20.217135,-87.427768);
//    coordBottomRight= CLLocationCoordinate2DMake(20.212788,-87.427768);
//    //coordBottomLeft = CLLocationCoordinate2DMake(20.212788,-87.431158);
//    coordBottomLeft = CLLocationCoordinate2DMake(20.212788,-87.430158);
    coordTopLeft    = CLLocationCoordinate2DMake(20.216702,-87.430900);
    coordTopRight   = CLLocationCoordinate2DMake(20.216702,-87.428000);
    coordBottomRight= CLLocationCoordinate2DMake(20.212800,-87.428000);
    coordBottomLeft = CLLocationCoordinate2DMake(20.212800,-87.430900);
    
    
    // Karachi Coordinates Square
//    coordTopLeft    = CLLocationCoordinate2DMake(24.879662, 67.059896);
//    coordTopRight   = CLLocationCoordinate2DMake(24.879662, 67.075732);
//    coordBottomRight= CLLocationCoordinate2DMake(24.864984, 67.075732);
//    coordBottomLeft = CLLocationCoordinate2DMake(24.864984, 67.059896);

    CLLocation *location1 = [[CLLocation alloc] initWithLatitude:coordTopLeft.latitude longitude:coordTopLeft.longitude];
    CLLocation *location2 = [[CLLocation alloc] initWithLatitude:coordTopRight.latitude longitude:coordTopRight.longitude];
    CLLocation *location3 = [[CLLocation alloc] initWithLatitude:coordBottomRight.latitude longitude:coordBottomRight.longitude];
    //CLLocation *location4 = [[CLLocation alloc] initWithLatitude:coordBottomLeft.latitude longitude:coordBottomLeft.longitude];
    
    mapWidth = round([location1 distanceFromLocation:location2]);
    mapHeight = round([location2 distanceFromLocation:location3]);

    NSLog(@"map width %d", mapWidth);
    NSLog(@"map height %d", mapHeight);
    
    _arrFrames = [NSArray arrayWithObjects:NSStringFromCGRect(CGRectMake(646, 725, 50, 50)), // 1
                  NSStringFromCGRect(CGRectMake(656, 670, 30, 30)), // 2
                  NSStringFromCGRect(CGRectMake(663, 807, 30, 25)), // 3
                  NSStringFromCGRect(CGRectMake(648, 640, 30, 20)), // 4
                  NSStringFromCGRect(CGRectMake(616, 650, 30, 30)), // 5
                  NSStringFromCGRect(CGRectMake(570, 655, 40, 30)), // 6
                  NSStringFromCGRect(CGRectMake(615, 715, 25, 25)), // 7
                  NSStringFromCGRect(CGRectMake(578, 740, 30, 30)), // 8
                  NSStringFromCGRect(CGRectMake(635, 832, 30, 30)), // 9
                  NSStringFromCGRect(CGRectMake(580, 815, 40, 30)), // 10
                  NSStringFromCGRect(CGRectMake(550, 785, 30, 30)), // 11
                  NSStringFromCGRect(CGRectMake(528, 815, 40, 40)), // 12
                  NSStringFromCGRect(CGRectMake(480, 865, 38, 40)), // 13
                  NSStringFromCGRect(CGRectMake(442, 825, 40, 40)), // 14
                  NSStringFromCGRect(CGRectMake(412, 770, 25, 25)), // 15
                  NSStringFromCGRect(CGRectMake(436, 773, 40, 40)), // 16
                  NSStringFromCGRect(CGRectMake(482, 780, 25, 25)), // 17
                  NSStringFromCGRect(CGRectMake(480, 742, 25, 25)), // 18
                  NSStringFromCGRect(CGRectMake(360, 815, 45, 50)), // 19
                  NSStringFromCGRect(CGRectMake(360, 750, 50, 60)), // 20
                  NSStringFromCGRect(CGRectMake(438, 676, 80, 60)), // 21
                  NSStringFromCGRect(CGRectMake(438, 585, 20, 50)), // 22
                  NSStringFromCGRect(CGRectMake(470, 622, 25, 25)), // 23
                  NSStringFromCGRect(CGRectMake(524, 550, 25, 25)), // 24
                  NSStringFromCGRect(CGRectMake(463, 550, 38, 40)), // 25
                  NSStringFromCGRect(CGRectMake(355, 698, 35, 52)), // 26
                  NSStringFromCGRect(CGRectMake(360, 650, 40, 40)), // 27
                  NSStringFromCGRect(CGRectMake(374, 610, 30, 36)), // 28
                  NSStringFromCGRect(CGRectMake(385, 540, 38, 40)), // 29
                  NSStringFromCGRect(CGRectMake(388, 415, 35, 120)), // 30
                  NSStringFromCGRect(CGRectMake(395, 210, 36, 40)), // 31
                  NSStringFromCGRect(CGRectMake(395, 166, 36, 40)), // 32
                  NSStringFromCGRect(CGRectMake(448, 251, 55, 36)), // 33
                  NSStringFromCGRect(CGRectMake(448, 188, 50, 50)), // 34
                  NSStringFromCGRect(CGRectMake(615, 132, 40, 40)), // 35
                  NSStringFromCGRect(CGRectMake(670, 136, 30, 30)), // 36
                  NSStringFromCGRect(CGRectMake(690, 180, 40, 25)), // 37
                  NSStringFromCGRect(CGRectMake(690, 206, 40, 30)), // 38
                  NSStringFromCGRect(CGRectMake(715, 290, 25, 20)), // 39
                  NSStringFromCGRect(CGRectMake(726, 268, 20, 20)), // 40
                  NSStringFromCGRect(CGRectMake(750, 265, 20, 25)), // 41
                  NSStringFromCGRect(CGRectMake(750, 302, 25, 25)), // 42
                  NSStringFromCGRect(CGRectMake(773, 305, 20, 25)), // 43
                  NSStringFromCGRect(CGRectMake(785, 320, 25, 25)), // 44
                  NSStringFromCGRect(CGRectMake(780, 345, 30, 30)), // 45
                  NSStringFromCGRect(CGRectMake(558, 890, 38, 36)), // 46
                  NSStringFromCGRect(CGRectMake(525, 924, 45, 40)), // 47
                  NSStringFromCGRect(CGRectMake(530, 1010, 45, 40)), // 48
                  NSStringFromCGRect(CGRectMake(374, 1112, 45, 55)), // 49
                  NSStringFromCGRect(CGRectMake(575, 1312, 35, 70)), // 50
                  NSStringFromCGRect(CGRectMake(610, 1150, 45, 70)), // 51
                  NSStringFromCGRect(CGRectMake(662, 1205, 45, 40)), // 52
                  NSStringFromCGRect(CGRectMake(663, 1250, 40, 38)), // 53
                  NSStringFromCGRect(CGRectMake(686, 1288, 31, 30)), // 54
                  NSStringFromCGRect(CGRectMake(250, 120, 50, 50)), // 55
                  NSStringFromCGRect(CGRectMake(217, 1484, 40, 40)), // 56
                  NSStringFromCGRect(CGRectMake(638, 290, 30, 50)), // 60
                  NSStringFromCGRect(CGRectMake(505, 160, 35, 35)), // 61
                  NSStringFromCGRect(CGRectMake(442, 321, 38, 38)), // 62
                  NSStringFromCGRect(CGRectMake(440, 360, 38, 38)), // 63
                  NSStringFromCGRect(CGRectMake(460, 410, 35, 45)), // 64
                  NSStringFromCGRect(CGRectMake(433, 410, 25, 45)), // 65
                  NSStringFromCGRect(CGRectMake(450, 464, 35, 46)), // 66
                  NSStringFromCGRect(CGRectMake(388, 298, 40, 40)), // 67
                  NSStringFromCGRect(CGRectMake(388, 366, 38, 38)), // 68
                  NSStringFromCGRect(CGRectMake(170, 630, 40, 50)), // gift shop
                  NSStringFromCGRect(CGRectMake(165, 685, 50, 80)), // ticket booth
                  NSStringFromCGRect(CGRectMake(120, 750, 50, 30)), // bathroom women
                  NSStringFromCGRect(CGRectMake(120, 800, 50, 30)), // bathroom men
                  NSStringFromCGRect(CGRectMake(420, 94, 38, 38)), // North gate 2
                  NSStringFromCGRect(CGRectMake(652, 100, 38, 34)), // North gate 1
                  NSStringFromCGRect(CGRectMake(220, 846, 36, 36)), // West gate
                  NSStringFromCGRect(CGRectMake(380, 1450, 40, 50)), // South gate 2
                  NSStringFromCGRect(CGRectMake(630, 1410, 40, 45)), // South gate 1
                  NSStringFromCGRect(CGRectMake(210, 1620, 70, 40)), // Lesser Wall
                  NSStringFromCGRect(CGRectMake(670, 880, 50, 50)), // Beach access
                  NSStringFromCGRect(CGRectMake(670, 450, 100, 100)), // Beach port
                  nil];
    
    _arrStructure = [NSArray arrayWithObjects:@"Structure 1",
                     @"Structure 2",
                     @"Structure 3",
                     @"Structure 4",
                     @"Structure 5",
                     @"Structure 6",
                     @"Structure 7",
                     @"Structure 8",
                     @"Structure 9",
                     @"Structure 10",
                     @"Structure 11",
                     @"Structure 12",
                     @"Structure 13",
                     @"Structure 14",
                     @"Structure 15",
                     @"Structure 16",
                     @"Structure 17",
                     @"Structure 18",
                     @"Structure 19",
                     @"Structure 20",
                     @"Structure 21",
                     @"Structure 22",
                     @"Structure 23",
                     @"Structure 24",
                     @"Structure 25",
                     @"Structure 26",
                     @"Structure 27",
                     @"Structure 28",
                     @"Structure 29",
                     @"Structure 30",
                     @"Structure 31",
                     @"Structure 32",
                     @"Structure 33",
                     @"Structure 34",
                     @"Structure 35",
                     @"Structure 36",
                     @"Structure 37",
                     @"Structure 38",
                     @"Structure 39",
                     @"Structure 40",
                     @"Structure 41",
                     @"Structure 42",
                     @"Structure 43",
                     @"Structure 44",
                     @"Structure 45",
                     @"Structure 46",
                     @"Structure 47",
                     @"Structure 48",
                     @"Structure 49",
                     @"Structure 50",
                     @"Structure 51",
                     @"Structure 52",
                     @"Structure 53",
                     @"Structure 54",
                     @"Structure 55",
                     @"Structure 56",
                     @"Structure 60",
                     @"Structure 61",
                     @"Structure 62",
                     @"Structure 63",
                     @"Structure 64",
                     @"Structure 65",
                     @"Structure 66",
                     @"Structure 67",
                     @"Structure 68",
                     @"Gift shop",
                     @"Ticket booth",
                     @"Bathroom women",
                     @"Bathroom men",
                     @"North gate 2",
                     @"North gate 1",
                     @"West gate",
                     @"South gate 2",
                     @"South gate 1",
                     @"Lesser Wall",
                     @"Beach access",
                     @"Beach port",
                     nil];
    
    [self setScrollView];
    
    _locationManager = [[CLLocationManager alloc] init];
    [_locationManager setDelegate:self];
    [_locationManager setDesiredAccuracy:kCLLocationAccuracyBest];
    //[_locationManager startUpdatingHeading];
    if ([_locationManager respondsToSelector:@selector(requestWhenInUseAuthorization)]) {
        [_locationManager requestWhenInUseAuthorization];
    }
    [_locationManager startUpdatingLocation];
    
    count = 0;
    
    //[self setTempStructureAtIndex:@"0"];  to another line, it is a programmer error to set either the adjustsFontSizeToFitWidth or adjustsLetterSpacingToFitWidth property to YES.
    
    //[self updateCustomLocation:@"0-0"];
    
	// Do any additional setup after loading the view.
}

- (void)alertDismiss {
    
    [timerAlert invalidate];
    timerAlert = nil;
    [alertInfo dismissAnimated:YES];
}

- (void)setTempStructureAtIndex:(NSString *)index {
    
    CLLocationCoordinate2D coordinate = CLLocationCoordinate2DMake(arrCoordinates[[index intValue]][0], arrCoordinates[[index intValue]][1]);
    [self locationManagerUpdateWithCoordinates:coordinate];
    
    //[self performSelector:@selector(locationManagerUpdateWithCoordinates:) withObject:coordinate afterDelay:1.0];
}

- (void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
    
    if (audioPlayViewController) {
        audioPlayViewController.audioPlayer = nil;
        [[Singleton sharedSingleton] setHAudioRunning:NO];
    }
    isFirstTime = YES;
}

- (void)viewDidAppear:(BOOL)animated {
    [super viewDidAppear:animated];
    
    [self.view bringSubviewToFront:viewAlert];
    if (!isCommingFirstTime && ![CLLocationManager locationServicesEnabled]) {
        SIAlertView *alertView = [[SIAlertView alloc] initWithTitle:nil andMessage:@"Please enable your GPS service through \"settings > Privacy > Location\""];
        [alertView addButtonWithTitle:@"OK"
                                 type:SIAlertViewButtonTypeDefault
                              handler:^(SIAlertView *alertView) {
                              }];
        [alertView show];
    }
    else {
        timerAlert = [NSTimer scheduledTimerWithTimeInterval:2.0 target:self selector:@selector(alertDismiss) userInfo:nil repeats:NO];
        alertInfo = [[SIAlertView alloc] initWithTitle:nil andMessage:@"zoom in and click any building for information"];
        [alertInfo show];
    }
    isCommingFirstTime = NO;
}

- (UIStatusBarStyle)preferredStatusBarStyle
{
    return UIStatusBarStyleLightContent;
}

- (void)updateCustomLocation:(NSString *)str {
    
    NSArray *arr = [str componentsSeparatedByString:@"-"];
    
    int lat = [arr[0] intValue];
    int lon = [arr[1] intValue];
    
    //lat = (lat > 4347) ? lat = 0 : lat + 15;
    lon = (lon > 3390) ? lon = 1 : lon + 25;
    
    int x = horzonalFactor*(float)lon;
    int y = verticalFactor*(float)lat;
    
    [UIView transitionWithView:_imageViewDot duration:1.0 options:UIViewAnimationOptionBeginFromCurrentState animations:^{
        [_imageViewDot setCenter:CGPointMake(x+130, y+100)];

    } completion:nil];
    
    [self performSelector:@selector(updateCustomLocation:) withObject:[NSString stringWithFormat:@"%d-%d", lat, lon] afterDelay:1.0];
}

-(void) setScrollView
{
    
    CGRect rectScroll = CGRectZero;
    rectScroll.size.width = self.view.frame.size.width;
    
    if ([[[UIDevice currentDevice] systemVersion] doubleValue] < 7.0) {
        if ([[UIDevice currentDevice] userInterfaceIdiom] == UIUserInterfaceIdiomPad) {
            rectScroll.origin.y = 90;
        }
        else {
            rectScroll.origin.y = 45;
        }
        //_scrollView = [[UIScrollView alloc] initWithFrame:CGRectMake(0, 45, 320, self.view.frame.size.height-self.tabBarController.tabBar.frame.size.height-45)];
    }
    else {
        if ([[UIDevice currentDevice] userInterfaceIdiom] == UIUserInterfaceIdiomPad) {
            rectScroll.origin.y = 110;
        }
        else {
            rectScroll.origin.y = 65;
        }

        //_scrollView = [[UIScrollView alloc] initWithFrame:CGRectMake(0, 65, 320, self.view.frame.size.height-self.tabBarController.tabBar.frame.size.height-65)];
    }
    rectScroll.size.height = self.view.frame.size.height-self.tabBarController.tabBar.frame.size.height-rectScroll.origin.y;
    
    
    _scrollView = [[UIScrollView alloc] initWithFrame:rectScroll];
    
	_scrollView.backgroundColor = [UIColor blackColor];
	_scrollView.delegate = self;
	_imageView = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"map-plain.png"]];
    [_imageView setUserInteractionEnabled:YES];
    _scrollView.contentSize = _imageView.image.size;
    [_scrollView setAlwaysBounceHorizontal:YES];
	[_scrollView addSubview:_imageView];
	_scrollView.minimumZoomScale = _scrollView.frame.size.width / _imageView.frame.size.width;
	_scrollView.maximumZoomScale = 2.0;
	[_scrollView setZoomScale:_scrollView.minimumZoomScale];
	[self.view addSubview:_scrollView];
    
    UITapGestureRecognizer *dblRecognizer = [[UITapGestureRecognizer alloc] initWithTarget:self
                                                                                    action:@selector(handleDoubleTapFrom:)];
    [dblRecognizer setNumberOfTapsRequired:2];
    //[dblRecognizer setNumberOfTouchesRequired:1];
    
    [_scrollView addGestureRecognizer:dblRecognizer];
    
    [self setButtonsOnScrollView];
    
}

- (CGRect)zoomRectForScale:(float)scale withCenter:(CGPoint)center {
    
    CGRect zoomRect;
    
    zoomRect.size.height = [_imageView frame].size.height / scale;
    zoomRect.size.width  = [_imageView frame].size.width  / scale;
    
    center = [_imageView convertPoint:center fromView:_scrollView];
    
    zoomRect.origin.x    = center.x - ((zoomRect.size.width / 2.0));
    zoomRect.origin.y    = center.y - ((zoomRect.size.height / 2.0));
    
    return zoomRect;
}

- (void)handleDoubleTapFrom:(UITapGestureRecognizer *)recognizer {
    
    //float newScale = [scrollView zoomScale] * 3.0;
    
    if (_scrollView.zoomScale > _scrollView.minimumZoomScale) {
        [_scrollView setZoomScale:_scrollView.minimumZoomScale animated:YES];
    }
    else {
        CGRect zoomRect = [self zoomRectForScale:_scrollView.maximumZoomScale withCenter:[recognizer locationInView:recognizer.view]];
        [_scrollView zoomToRect:zoomRect animated:YES];
    }
}

- (void)setButtonsOnScrollView {
    
    for (int i=0; i < [_arrFrames count]; i++) {
        
        UIButton *btn1 = [UIButton buttonWithType:UIButtonTypeCustom];
        CGRect frame = CGRectFromString(_arrFrames[i]);
        [btn1 setFrame:frame];
        [btn1 setTag:i+1000];
        [btn1 addTarget:self action:@selector(btnPressed:) forControlEvents:UIControlEventTouchUpInside];
        [_imageView addSubview:btn1];
    }
    
//    _imageViewDot = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"map-dot.png"]];
//    [_imageView addSubview:_imageViewDot];
//    [self setDotsize];
    
    [self.view bringSubviewToFront:btnBackward];
    [self.view bringSubviewToFront:btnForward];
}

- (void)btnPressed:(id )sender {
    
    int tag = [sender tag] - 1000;
    
    if (tag == 36 || tag == 37 || tag == 47) {
        return;
    }
    
    SIAlertView *alertView = [[SIAlertView alloc] initWithTitle:nil andMessage:_arrStructure[tag]];
    [alertView addButtonWithTitle:@"About"
                             type:SIAlertViewButtonTypeDefault
                          handler:^(SIAlertView *alertView) {
                              
                              int myTag = tags[tag];// >62 ? tags[tag]-1 : tags[tag];
                              
                              NSString *path = [[NSBundle mainBundle] pathForResource:@"AudioFileList" ofType:@"plist"];
                              
                              audioPlayViewController = [self.storyboard instantiateViewControllerWithIdentifier:@"AudioPlayViewControllerFull"];
                              [audioPlayViewController setArrAudioFile:[NSArray arrayWithContentsOfFile:path]];
                              [audioPlayViewController setSelectedIndex:myTag];
                              [self.navigationController pushViewController:audioPlayViewController animated:YES];

                          }];
    [alertView addButtonWithTitle:@"Take Me There"
                             type:SIAlertViewButtonTypeDefault
                          handler:^(SIAlertView *alertView) {
                              
                              [[NSUserDefaults standardUserDefaults] setObject:_arrStructure[tag] forKey:@"structure"];
                              [[NSUserDefaults standardUserDefaults] synchronize];
                              
                              UINavigationController *navController = self.tabBarController.viewControllers[2];
                              TakeMeThereViewController *viewController = (TakeMeThereViewController *)(navController.viewControllers[0]);
                              [viewController setFinalLocation:CLLocationCoordinate2DMake(arrCoordinates[tag][0], arrCoordinates[tag][1])];
                              [viewController setSelectedIndex:tags[tag]];
                              [self.tabBarController setSelectedViewController:navController];
                          }];
    [alertView addButtonWithTitle:@"Cancel"
                             type:SIAlertViewButtonTypeCancel
                          handler:^(SIAlertView *alertView) {
                          }];
    [alertView show];

}

- (void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex {
    
    if (buttonIndex == 1) {
        
        int myTag = tags[[alertView tag]];// >62 ? tags[tag]-1 : tags[tag];
        
        NSString *path = [[NSBundle mainBundle] pathForResource:@"AudioFileList" ofType:@"plist"];
        
        audioPlayViewController = [self.storyboard instantiateViewControllerWithIdentifier:@"AudioPlayViewControllerFull"];
        [audioPlayViewController setArrAudioFile:[NSArray arrayWithContentsOfFile:path]];
        [audioPlayViewController setSelectedIndex:myTag];
        [self.navigationController pushViewController:audioPlayViewController animated:YES];
        
    }
    else if (buttonIndex == 2) {
        
        int tag = [alertView tag];
        
        [[NSUserDefaults standardUserDefaults] setObject:_arrStructure[tag] forKey:@"structure"];
        [[NSUserDefaults standardUserDefaults] synchronize];

        UINavigationController *navController = self.tabBarController.viewControllers[2];
        TakeMeThereViewController *viewController = (TakeMeThereViewController *)(navController.viewControllers[0]);
        [viewController setFinalLocation:CLLocationCoordinate2DMake(arrCoordinates[tag][0], arrCoordinates[tag][1])];
        [viewController setSelectedIndex:tags[tag]];
        [self.tabBarController setSelectedViewController:navController];
    }
}

- (void)coordinates {
    
    //    int x =  (int) ((MAP_WIDTH/360.0) * (180 + lon));
    //    int y =  (int) ((MAP_HEIGHT/180.0) * (90 - lat));
    
    
//    float x = (_imageView.image.size.width) * (180 + 20.212549) / 360;
//    float y = (_imageView.image.size.height) * (90 - (-87.470376)) / 180;
}

- (CGRect)centeredFrameForScrollView:(UIScrollView *)scroll andUIView:(UIView *)rView {
	CGSize boundsSize = scroll.bounds.size;
    CGRect frameToCenter = rView.frame;
    
    // center horizontally
    if (frameToCenter.size.width < boundsSize.width) {
        frameToCenter.origin.x = (boundsSize.width - frameToCenter.size.width) / 2;
    }
    else {
        frameToCenter.origin.x = 0;
    }
    
    // center vertically
    if (frameToCenter.size.height < boundsSize.height) {
        frameToCenter.origin.y = (boundsSize.height - frameToCenter.size.height) / 2;
    }
    else {
        frameToCenter.origin.y = 0;
    }
	
	return frameToCenter;
}

- (void)setDotsize {
    
    [UIView animateWithDuration:0.6
                          delay:0.0
                        options:UIViewAnimationOptionBeginFromCurrentState | UIViewAnimationOptionCurveEaseOut | UIViewAnimationOptionAllowUserInteraction
                     animations:^{
                         //[_imageViewDot setCenter:CGPointMake(160, 10)];
                         CGAffineTransform scaleUp = CGAffineTransformMakeScale(1.5, 1.5);
                         _imageViewDot.transform = scaleUp;
                     }
                     completion:^(BOOL finished) {
                         
                         [UIView     animateWithDuration:0.6
                                                   delay:0.0
                                                 options:UIViewAnimationOptionBeginFromCurrentState | UIViewAnimationOptionCurveEaseOut | UIViewAnimationOptionAllowUserInteraction
                                              animations:^{
                                                  
                                                  //[_imageViewDot setCenter:CGPointMake(160, 10)];
                                                  //CGAffineTransform scaleDown = CGAffineTransformMakeScale(0.5, 0.5);
                                                  _imageViewDot.transform = CGAffineTransformIdentity;
                                                  
                                              }
                                              completion:^(BOOL finished) {
                                                  [self setDotsize];
                                              }];
                     }];
}

#pragma mark --
#pragma mark -- UIScrollView Delegate

- (void)scrollViewDidZoom:(UIScrollView *)scrollV {
	_imageView.frame = [self centeredFrameForScrollView:scrollV andUIView:_imageView];
}

- (UIView *)viewForZoomingInScrollView:(UIScrollView *)scrollView {
	return _imageView;
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
}

#pragma mark --
#pragma mark -- CLLocationManager Delegate


- (void)locationManager:(CLLocationManager *)manager didUpdateLocations:(NSArray *)locations {
    
    CLLocationCoordinate2D coordinate = manager.location.coordinate;
    [[Singleton sharedSingleton] setLocationCoordinate:coordinate];
    //[self locationManagerUpdateWithCoordinates:coordinate];
}

- (void)locationManager:(CLLocationManager *)manager didFailWithError:(NSError *)error {
    
    //NSLog(@"Error %@", error.localizedDescription);
    
    SIAlertView *alertView = [[SIAlertView alloc] initWithTitle:nil andMessage:@"Please enable your GPS service through \"settings > Privacy > Location\""];
    [alertView addButtonWithTitle:@"OK"
                             type:SIAlertViewButtonTypeDefault
                          handler:^(SIAlertView *alertView) {
                          }];
    [alertView show];

}

- (void)locationManagerUpdateWithCoordinates:(CLLocationCoordinate2D )coordinate {
    
    if (coordinate.latitude < coordTopLeft.latitude && coordinate.latitude > coordBottomRight.latitude &&
        coordinate.longitude > coordTopLeft.longitude && coordinate.longitude < coordBottomRight.longitude) {
        
        float latDiff = coordTopLeft.latitude - coordinate.latitude;
        float lonDiff = coordinate.longitude - coordTopLeft.longitude;
        
        
//        int x = horzonalFactor*roundf(fabsf(lonDiff*1000000));
//        int y = verticalFactor*roundf(fabsf(latDiff*1000000));
        int x = horzonalFactor*(lonDiff*1000000);
        int y = verticalFactor*(latDiff*1000000);
        
        int a = 350 - y/5;
        x -= a;
//        int a = (y > 770) ? y-770 : y;
//        
//        if (y > 770) {
//            x += ((400/770)*a);
//        }
//        else {
//            x -= ((400/770)*a);
//        }
        
        NSLog(@"%@, x %d, y %d", _arrStructure[count], x, y);

        [UIView transitionWithView:_imageViewDot duration:1.0 options:UIViewAnimationOptionBeginFromCurrentState animations:^{
//            [_imageViewDot setCenter:CGPointMake(x+130+250, y+100-150)];
            [_imageViewDot setCenter:CGPointMake(x+130+200, y)];
            
        } completion:nil];
        
        [[NSUserDefaults standardUserDefaults] setBool:YES forKey:@"showMessage"];
        
    }
    else if (isFirstTime) {
        isFirstTime = NO;
        
        NSString *strDate = [NSString stringWithFormat:@"%@", [NSDate date]];
        NSArray *arr = [strDate componentsSeparatedByString:@":"];
        arr = [arr[0] componentsSeparatedByString:@" "];
        
        NSLog(@"date %@", arr[1]);
        
        int hour = [arr[1] intValue];
        if (abs(hour-[[NSUserDefaults standardUserDefaults] integerForKey:@"hour"]) > 1) {
            
            [[NSUserDefaults standardUserDefaults] setInteger:hour forKey:@"hour"];
            
            if ([[NSUserDefaults standardUserDefaults] boolForKey:@"showMessage"]) {
                
                [[NSUserDefaults standardUserDefaults] setBool:NO forKey:@"showMessage"];
                SIAlertView *alertView = [[SIAlertView alloc] initWithTitle:nil andMessage:@"You are not in Tulum"];
                [alertView addButtonWithTitle:@"OK"
                                         type:SIAlertViewButtonTypeDefault
                                      handler:^(SIAlertView *alertView) {
                                      }];
                [alertView show];
            }
        }
    }
    
    //[self performSelector:@selector(setTempStructureAtIndex:) withObject:[NSString stringWithFormat:@"%d", ++count] afterDelay:1.0];
}

- (IBAction)alertOKBtnPressed:(id)sender {
    
    [UIView transitionWithView:viewAlert duration:1.0 options:UIViewAnimationOptionTransitionCrossDissolve animations:^{
        [viewAlert setHidden:YES];
    } completion:^(BOOL finished) {
    }];
}

- (IBAction)cameraBtnPressed:(id)sender {
    
    SIAlertView *alertView = [[SIAlertView alloc] initWithTitle:nil andMessage:@"Choose type"];
    [alertView addButtonWithTitle:@"Take Picture"
                             type:SIAlertViewButtonTypeDefault
                          handler:^(SIAlertView *alertView) {
                              
                              PickerViewController *pickerViewController = [self.storyboard instantiateViewControllerWithIdentifier:@"PickerViewController"];
                              [pickerViewController setIsMediaTypeMovie:NO];
                              [self presentViewController:pickerViewController animated:YES completion:nil];
                              
                          }];
    [alertView addButtonWithTitle:@"Make Video"
                             type:SIAlertViewButtonTypeDefault
                          handler:^(SIAlertView *alertView) {
                              
                              PickerViewController *pickerViewController = [self.storyboard instantiateViewControllerWithIdentifier:@"PickerViewController"];
                              [pickerViewController setIsMediaTypeMovie:YES];
                              [self presentViewController:pickerViewController animated:YES completion:nil];
                          }];
    [alertView addButtonWithTitle:@"Cancel"
                             type:SIAlertViewButtonTypeCancel
                          handler:^(SIAlertView *alertView) {
                          }];
    
//    alertView.willShowHandler = ^(SIAlertView *alertView) {
//        NSLog(@"%@, willShowHandler2", alertView);
//    };
//    alertView.didShowHandler = ^(SIAlertView *alertView) {
//        NSLog(@"%@, didShowHandler2", alertView);
//    };
//    alertView.willDismissHandler = ^(SIAlertView *alertView) {
//        NSLog(@"%@, willDismissHandler2", alertView);
//    };
//    alertView.didDismissHandler = ^(SIAlertView *alertView) {
//        NSLog(@"%@, didDismissHandler2", alertView);
//    };
    
    [alertView show];
}

- (IBAction)backwardBtnPressed:(id)sender {
    
    if (count < 2) {
        return;
    }
    count --;
    [btnBackward setTitle:[_arrStructure[count+1] stringByReplacingOccurrencesOfString:@"Structure " withString:@""] forState:UIControlStateNormal];
    [btnForward setTitle:[_arrStructure[count-1] stringByReplacingOccurrencesOfString:@"Structure " withString:@""] forState:UIControlStateNormal];
//    [btnBackward setTitle:[NSString stringWithFormat:@"%d", count+2] forState:UIControlStateNormal];
//    [btnForward setTitle:[NSString stringWithFormat:@"%d", count] forState:UIControlStateNormal];
    [self setTempStructureAtIndex:[NSString stringWithFormat:@"%d", count]];
}

- (IBAction)forwardBtnPressed:(id)sender {
    
    if (count > 75) {
        return;
    }
    count ++;
    [btnForward setTitle:[_arrStructure[count-1] stringByReplacingOccurrencesOfString:@"Structure " withString:@""] forState:UIControlStateNormal];
    [btnBackward setTitle:[_arrStructure[count+1] stringByReplacingOccurrencesOfString:@"Structure " withString:@""] forState:UIControlStateNormal];
//    [btnForward setTitle:[NSString stringWithFormat:@"%d", count] forState:UIControlStateNormal];
//    [btnBackward setTitle:[NSString stringWithFormat:@"%d", count+2] forState:UIControlStateNormal];
    [self setTempStructureAtIndex:[NSString stringWithFormat:@"%d", count]];
}

@end
