//
//  PhotoScrollViewController.h
//  TestScroll
//
//  Created by Rizwan on 5/23/13.
//  Copyright (c) 2013 com. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface PhotoScrollViewController : UIViewController

+ (PhotoScrollViewController *)photoViewControllerForPageIndex:(NSUInteger)pageIndex;

- (NSInteger)pageIndex;

@end
