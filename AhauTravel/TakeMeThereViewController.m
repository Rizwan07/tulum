//
//  TakeMeThereViewController.m
//  AhauTravel
//
//  Created by Rizwan on 5/20/13.
//  Copyright (c) 2013 com. All rights reserved.
//

#import "TakeMeThereViewController.h"
#import <CoreLocation/CoreLocation.h>

#import "AudioPlayViewController.h"
#import "ChooseDestinationViewController.h"

@interface TakeMeThereViewController () {
    
    CLLocationDirection finalHeading;
    CLLocationDirection currentHeading;
    
    NSArray *_arrCoordinates;
    
    //int count;
    
    //UIImageView *imgView;
    //CLLocationDirection currentAngle;
    
    UIImageView *imgViewCompass;

    AudioPlayViewController *audioPlayViewController;
}

@property (nonatomic, strong) UIImageView *imageViewArrow;
@property (nonatomic, strong) UIImageView *imageViewRedArrow;
@property (nonatomic, strong) CLLocationManager *locationManager;
@property (strong, nonatomic) IBOutlet UILabel *lblTakeMeThere;
@property (strong, nonatomic) IBOutlet UILabel *lbldistance;

- (IBAction)cameraBtnPressed:(id)sender;

@end

@interface TakeMeThereViewController (CLLocationManager)<CLLocationManagerDelegate>
@end

@implementation TakeMeThereViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    _imageViewRedArrow = [[UIImageView alloc] initWithFrame:CGRectMake(140, self.view.frame.size.height/2-95, 40, 200)];
    [_imageViewRedArrow setImage:[UIImage imageNamed:@"needle-blue.png"]];

    [self.view addSubview:_imageViewRedArrow];
    
    _imageViewArrow = [[UIImageView alloc] initWithFrame:CGRectMake(140, self.view.frame.size.height/2-95, 40, 200)];
    [_imageViewArrow setImage:[UIImage imageNamed:@"needle-green.png"]];
    [self.view addSubview:_imageViewArrow];
    
    imgViewCompass = [[UIImageView alloc] initWithFrame:CGRectMake(10, 60, 80, 80)];
    [imgViewCompass setImage:[UIImage imageNamed:@"compass.png"]];
    
    [self.view addSubview:imgViewCompass];
    
    currentHeading = 0.0;
    finalHeading = 0.0;
    
    if ([[UIDevice currentDevice] userInterfaceIdiom] == UIUserInterfaceIdiomPad) {
        [_imageViewArrow setFrame:CGRectMake(self.view.frame.size.width/2-50, self.view.frame.size.height/2-200, 100, 500)];
        [_imageViewRedArrow setFrame:CGRectMake(self.view.frame.size.width/2-50, self.view.frame.size.height/2-200, 100, 500)];
        
        [imgViewCompass setFrame:CGRectMake(50, 150, 150, 150)];

        
        [_imageViewRedArrow setImage:[UIImage imageNamed:@"needle-blue-ipad.png"]];
        [_imageViewArrow setImage:[UIImage imageNamed:@"needle-green-ipad.png"]];
    }
    
    //finalLocation = CLLocationCoordinate2DMake(12.896667, -87.429444);
    //finalLocation = CLLocationCoordinate2DMake(25.116689,62.322296);
    
    //currentLocation = CLLocationCoordinate2DMake(25.397382, 68.380065); // Hyderabad
    //currentLocation = CLLocationCoordinate2DMake(34.011119, 71.545662); // Peshawar
    //currentLocation = CLLocationCoordinate2DMake(31.573856,74.347014); // Lahore
    //currentLocation = CLLocationCoordinate2DMake(30.222882,67.018554); // Quetta

    self.locationManager = [[CLLocationManager alloc] init];
    [_locationManager setDesiredAccuracy:kCLLocationAccuracyBest];
    [_locationManager setDelegate:self];
    [_locationManager setDistanceFilter:1];
    [_locationManager setHeadingFilter:4];
    //[_locationManager startUpdatingLocation];
    if ([CLLocationManager headingAvailable]) {
        
        [_locationManager startUpdatingHeading];
    }
    
    [self blinkRedArrow];

	// Do any additional setup after loading the view.
}

- (void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
    
    if (audioPlayViewController) {
        audioPlayViewController.audioPlayer = nil;
        [[Singleton sharedSingleton] setTAudioRunning:NO];
    }
}

- (void)viewDidAppear:(BOOL)animated {
    [super viewDidAppear:animated];
    
    if ([[NSUserDefaults standardUserDefaults] objectForKey:@"structure"]) {
        [_lblTakeMeThere setText:[[NSUserDefaults standardUserDefaults] objectForKey:@"structure"]];
    }
    
    if (![CLLocationManager locationServicesEnabled]) {
        SIAlertView *alertView = [[SIAlertView alloc] initWithTitle:nil andMessage:@"Please enable your GPS service through \"settings > Privacy > Location\""];
        [alertView addButtonWithTitle:@"OK"
                                 type:SIAlertViewButtonTypeDefault
                              handler:^(SIAlertView *alertView) {
                              }];
        [alertView show];
    }
    
    [self forwardGeocoderFoundLocation];
}

- (UIStatusBarStyle)preferredStatusBarStyle
{
    return UIStatusBarStyleLightContent;
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)blinkRedArrow {
    
    [UIView transitionWithView:_imageViewRedArrow duration:1.0 options:UIViewAnimationOptionTransitionCrossDissolve animations:^{
        [_imageViewRedArrow setAlpha:0.2];
    } completion:^(BOOL finished) {
        [UIView transitionWithView:_imageViewRedArrow duration:0.5 options:UIViewAnimationOptionTransitionCrossDissolve animations:^{
            [_imageViewRedArrow setAlpha:1.0];
        } completion:^(BOOL finished) {
            [self blinkRedArrow];
        }];
    }];
}

- (void)updateHeadingDisplays {
    
    // Animate Pointer
    
    CLLocationDirection currentAngle = currentHeading - finalHeading;
    //currentAngle = (CGFloat)toRad(finalHeading)-(CGFloat)toRad(currentHeading);
    
    //if (currentAngle < 25 && currentAngle > -25) {
    if (currentAngle < 10 && currentAngle > -10) {
        //[_imageViewArrow setImage:[UIImage imageNamed:@"needle-green.png"]];
        if ([[UIDevice currentDevice] userInterfaceIdiom] == UIUserInterfaceIdiomPad) {
            [_imageViewArrow setImage:[UIImage imageNamed:@"needle-green-ipad.png"]];
        }
        else {
            [_imageViewArrow setImage:[UIImage imageNamed:@"needle-green.png"]];
        }
        [_imageViewRedArrow setHidden:YES];
    }
    else {
        //[_imageViewArrow setImage:[UIImage imageNamed:@"needle-golden.png"]];
        if ([[UIDevice currentDevice] userInterfaceIdiom] == UIUserInterfaceIdiomPad) {
            [_imageViewArrow setImage:[UIImage imageNamed:@"needle-golden-ipad.png"]];
        }
        else {
            [_imageViewArrow setImage:[UIImage imageNamed:@"needle-golden.png"]];
        }
        [_imageViewRedArrow setHidden:NO];
    }
    
    [UIView     animateWithDuration:0.6
                              delay:0.0
                            options:UIViewAnimationOptionBeginFromCurrentState | UIViewAnimationOptionCurveEaseOut | UIViewAnimationOptionAllowUserInteraction
                         animations:^{
                             CGAffineTransform headingRotation;
//                             headingRotation = CGAffineTransformRotate(CGAffineTransformIdentity, (CGFloat)-toRad(finalHeading));
                             headingRotation = CGAffineTransformRotate(CGAffineTransformIdentity, (CGFloat)-toRad(currentAngle));
                             _imageViewArrow.transform = headingRotation;
                         }
                         completion:^(BOOL finished) {
                             
                         }] ;
    
}

//
//- (void)locationManager:(CLLocationManager *)manager didUpdateToLocation:(CLLocation *)newLocation fromLocation:(CLLocation *)oldLocation
//{
////    NSLog(@"latitude %+.6f, longitude %+.6f\n", newLocation.coordinate.latitude, newLocation.coordinate.longitude);
//    currentLocation = newLocation.coordinate;
//    [self forwardGeocoderFoundLocation];
//}

- (void)locationManager:(CLLocationManager *)manager didUpdateLocations:(NSArray *)locations {
    
    //currentLocation = manager.location.coordinate;
    [self forwardGeocoderFoundLocation];
    [manager stopUpdatingLocation];
    
    if ([CLLocationManager headingAvailable]) {
        
        [_locationManager startUpdatingHeading];
    }

}

- (void)locationManager:(CLLocationManager *)manager didUpdateHeading:(CLHeading *)newHeading {
    if (newHeading.headingAccuracy < 0)
        return;
    
    // Use the true heading if it is valid.
    CLLocationDirection  theHeading = ((newHeading.trueHeading > 0) ?
                                       newHeading.trueHeading : newHeading.magneticHeading);
    
    currentHeading = theHeading;
    
    [UIView     animateWithDuration:0.3
                              delay:0.0
                            options:UIViewAnimationOptionBeginFromCurrentState | UIViewAnimationOptionCurveEaseOut | UIViewAnimationOptionAllowUserInteraction
                         animations:^{
                             
                             //[imgViewCompass setCenter:CGPointMake(40, 40)];
                             CGAffineTransform headingRotation;
                             headingRotation = CGAffineTransformRotate(CGAffineTransformIdentity, (CGFloat)-toRad(currentHeading));
                             imgViewCompass.transform = headingRotation;
                         }
                         completion:^(BOOL finished) {
                             
                         }];
    //[self updateHeadingDisplays];
    [self forwardGeocoderFoundLocation];
}

-(CLLocationDirection) directionFrom: (CLLocationCoordinate2D) startPt to:(CLLocationCoordinate2D) endPt {
    double lat1 = toRad(startPt.latitude);
    double lat2 = toRad(endPt.latitude);
    double lon1 = toRad(startPt.longitude);
    double lon2 = toRad(endPt.longitude);
    double dLon = (lon2-lon1);
    
    double y = sin(dLon) * cos(lat2);
    double x = cos(lat1) * sin(lat2) - sin(lat1) * cos(lat2) * cos(dLon);
    double brng = toDeg(atan2(y, x));
    
    brng = (brng+360);
    brng = (brng>360)? (brng-360) : brng;
    
    return brng;
}

- (int )getVariableDistance {
    
    if ([_lblTakeMeThere.text isEqualToString:@"Structure 1"]) {
        return 5;
    }
    else if ([_lblTakeMeThere.text isEqualToString:@"Structure 9"]) {
        return 4;
    }
    else if ([_lblTakeMeThere.text isEqualToString:@"Structure 13"]) {
        return 9;
    }
    else if ([_lblTakeMeThere.text isEqualToString:@"Structure 15"]) {
        return 4;
    }
    else if ([_lblTakeMeThere.text isEqualToString:@"Structure 16"]) {
        return 27;
    }
    else if ([_lblTakeMeThere.text isEqualToString:@"Structure 25"]) {
        return 15;
    }
    else if ([_lblTakeMeThere.text isEqualToString:@"Structure 34"]) {
        return 9;
    }
    else if ([_lblTakeMeThere.text isEqualToString:@"Structure 35"]) {
        return 9;
    }
    else if ([_lblTakeMeThere.text isEqualToString:@"Structure 36"]) {
        return 5;
    }
    else if ([_lblTakeMeThere.text isEqualToString:@"Structure 40"]) {
        return 18;
    }
    else if ([_lblTakeMeThere.text isEqualToString:@"Structure 49"]) {
        return 20;
    }
    else if ([_lblTakeMeThere.text isEqualToString:@"Structure 60"]) {
        return 7;
    }
    else if ([_lblTakeMeThere.text isEqualToString:@"Structure 63"]) {
        return 5;
    }
    else if ([_lblTakeMeThere.text isEqualToString:@"Structure 65"]) {
        return 3;
    }
    else if ([_lblTakeMeThere.text isEqualToString:@"Structure 66"]) {
        return 10;
    }
    else if ([_lblTakeMeThere.text isEqualToString:@"Ticket booth"]) {
        return 12;
    }
    return 0;
//    Str 1 - 5 meters. GPS seems to have the point at the other side of the path (by the cliff). I say this because when standing in front the arrow points toward the sea. If you are N or S of that spot, the lines on the navigation tools show you as N or S of the path in front of the structure. Not that important in my opinion considering it is the largest, most impressive structure.
//    Str 9 - 4 meters
//    Str 13 - 9 meters
//    Str 15 - 4 meters
//    Str 16 - 27 meters
//    Str 25 - 12 meters (other side of path, jungle side)
//    Str 25 - 19 meters
//    Str 34 - 8 meters
//    Str 35 - 8-10 meters away
//    Str 36 - Within 5 meters
//    Str 40 - 18 meters
//    Str 48 - unable to click on map. I assume this is because the same reason behind 37 & 38 (non existant) but WIT picked it up and when it gave me the options of "Update" and "About", I clicked about and it gave me the information for 47. Probably by design if my assumption is correct but from an end-user standpoint, it may be mistaken for buggy - it may be best to remove the coordinates for WIT so it doesn't cause confusion.
//    Str 49 - I was at the steps and GPS had me 30 meters off to the left
//    (S)
//    Str 60 - 7 meters
//    Str 63 - appears to have changed with time or additional renovations?
//    Str 65 - 3 meters - directional arrows showed slightly N of the structure but no biggie
//    Str 66 - 10 meters
//    
//    The structures location was not easily identifiable (using the picture on "About") because I couldn't see the steps and the GPS didn't help me identify which one it was. Looking at the map I had an idea where it was but it was challenging to identify.
}

- (void)forwardGeocoderFoundLocation
{
    CLLocationCoordinate2D currentLocation = [[Singleton sharedSingleton] locationCoordinate];
    finalHeading = [self directionFrom:currentLocation to:_finalLocation];
    
    CLLocation *location1 = [[CLLocation alloc] initWithLatitude:currentLocation.latitude longitude:currentLocation.longitude];
    CLLocation *location2 = [[CLLocation alloc] initWithLatitude:_finalLocation.latitude longitude:_finalLocation.longitude];
    
    
    //NSLog(@"Distance in kilomemter %f", [location1 distanceFromLocation:location2]/1000);
    
    
    double distance = [location1 distanceFromLocation:location2] - [self getVariableDistance];
    
    //distance = (distance < 0) ? 0 : distance;
    
    [_lbldistance setText:[NSString stringWithFormat:@"%.f METERS AWAY", fabs(distance)]];
    
    [self updateHeadingDisplays];
    
    return;
    
    [UIView     animateWithDuration:0.6
                              delay:0.0
                            options:UIViewAnimationOptionBeginFromCurrentState | UIViewAnimationOptionCurveEaseOut | UIViewAnimationOptionAllowUserInteraction
                         animations:^{
                             CGAffineTransform headingRotation;
//                             headingRotation = CGAffineTransformRotate(CGAffineTransformIdentity, (CGFloat)-toRad(finalHeading));
                             headingRotation = CGAffineTransformRotate(CGAffineTransformIdentity, (CGFloat)toRad(finalHeading));
                             _imageViewArrow.transform = headingRotation;
                         }
                         completion:^(BOOL finished) {
                             
                         }];
    
}

- (IBAction)aboutBtnPressed:(id)sender {
    
    NSString *path = [[NSBundle mainBundle] pathForResource:@"AudioFileList" ofType:@"plist"];

    audioPlayViewController = [self.storyboard instantiateViewControllerWithIdentifier:@"AudioPlayViewControllerFull"];
    [audioPlayViewController setArrAudioFile:[NSArray arrayWithContentsOfFile:path]];
    [audioPlayViewController setSelectedIndex:_selectedIndex];
    [self.navigationController pushViewController:audioPlayViewController animated:YES];
}

- (IBAction)chooseDestinationBtnPressed:(id)sender {
    
    ChooseDestinationViewController *chooseDestinationViewController = [self.storyboard instantiateViewControllerWithIdentifier:@"ChooseDestinationViewController"];
    [self.navigationController pushViewController:chooseDestinationViewController animated:YES];
}

- (IBAction)cameraBtnPressed:(id)sender {
    
    SIAlertView *alertView = [[SIAlertView alloc] initWithTitle:nil andMessage:@"Choose type"];
    [alertView addButtonWithTitle:@"Take Picture"
                             type:SIAlertViewButtonTypeDefault
                          handler:^(SIAlertView *alertView) {
                              
                              PickerViewController *pickerViewController = [self.storyboard instantiateViewControllerWithIdentifier:@"PickerViewController"];
                              [pickerViewController setIsMediaTypeMovie:NO];
                              [self presentViewController:pickerViewController animated:YES completion:nil];
                          }];
    [alertView addButtonWithTitle:@"Make Video"
                             type:SIAlertViewButtonTypeDefault
                          handler:^(SIAlertView *alertView) {
                              
                              PickerViewController *pickerViewController = [self.storyboard instantiateViewControllerWithIdentifier:@"PickerViewController"];
                              [pickerViewController setIsMediaTypeMovie:YES];
                              [self presentViewController:pickerViewController animated:YES completion:nil];
                          }];
    [alertView addButtonWithTitle:@"Cancel"
                             type:SIAlertViewButtonTypeCancel
                          handler:^(SIAlertView *alertView) {
                          }];
    
    [alertView show];
}

@end
