//
//  AncientHistoryViewController.h
//  AhauTravel
//
//  Created by Rizwan on 6/18/13.
//  Copyright (c) 2013 com. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <AVFoundation/AVFoundation.h>

@interface AudioPlayViewController : UIViewController

@property (nonatomic, strong) AVAudioPlayer *audioPlayer;

@property(nonatomic, assign) int selectedIndex;
@property (nonatomic, strong) NSArray *arrAudioFile;

- (IBAction)backBtnPressed:(id)sender;
@end
