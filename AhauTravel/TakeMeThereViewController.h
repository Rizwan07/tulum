//
//  TakeMeThereViewController.h
//  AhauTravel
//
//  Created by Rizwan on 5/20/13.
//  Copyright (c) 2013 com. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface TakeMeThereViewController : UIViewController

@property (nonatomic, assign) CLLocationCoordinate2D finalLocation;
@property (nonatomic, assign) int selectedIndex;

- (IBAction)aboutBtnPressed:(id)sender;
- (IBAction)chooseDestinationBtnPressed:(id)sender;

@end
