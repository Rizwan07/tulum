//
//  SubAudioPlayViewController.m
//  AhauTravel
//
//  Created by Rizwan on 7/25/13.
//  Copyright (c) 2013 com. All rights reserved.
//

#import "SubAudioPlayViewController.h"

#import "AudioPlayViewController.h"
#import "BuildingFeatureViewController.h"
#import "TulumSiteHistoryViewController.h"
#import "SelectedImageViewController.h"

#import "HomeViewController.h"
#import "WhatIsThatViewController.h"
#import "TakeMeThereViewController.h"

#import "NimbusAttributedLabel.h"

@interface SubAudioPlayViewController () {
    
    BOOL isVolumeOn;
    UISlider *slider;
    NSTimer *updateTimer;
    BOOL isPause;
    UIButton *btnPlayPause;
}

- (IBAction)previousBtnPressed:(id)sender;
- (IBAction)playBtnPressed:(id)sender;
- (IBAction)nextBtnPressed:(id)sender;
- (IBAction)volumeBtnPressed:(id)sender;
- (IBAction)sliderSlided:(id)sender;


@property (strong, nonatomic) UIImageView *imageViewTop;
@property (strong, nonatomic) UIScrollView *scrollView;
@property (strong, nonatomic) NIAttributedLabel *lblText;
@property (strong, nonatomic) IBOutlet UILabel *lblHeading;

@property (nonatomic, weak) UIViewController *myParentViewController;

@property (nonatomic, strong) NSArray *arrAudioFile;

- (IBAction)cameraBtnPressed:(id)sender;
- (IBAction)backBtnPressed:(id)sender;

@end

@interface SubAudioPlayViewController () <AVAudioPlayerDelegate, NIAttributedLabelDelegate>
@end

@implementation SubAudioPlayViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    isVolumeOn = YES;
    isPause = NO;
    
    NSString *path = [[NSBundle mainBundle] pathForResource:@"AudioFileSubList" ofType:@"plist"];
    _arrAudioFile = [NSArray arrayWithContentsOfFile:path];
    
    //    [slider setMaximumTrackImage:[UIImage imageNamed:@"outer-volume-bar.png"] forState:UIControlStateNormal];
    //    [slider setMinimumTrackImage:[UIImage imageNamed:@"inner-volume-bar.png"] forState:UIControlStateNormal];
    //    [slider setThumbImage:[UIImage imageNamed:@"thumb-volume-control.png"] forState:UIControlStateNormal];
    
    self.myParentViewController = self.navigationController.viewControllers[0];
    
    [self setScrollViewProgramatically];
    
	// Do any additional setup after loading the view.
}

- (void)setScrollViewProgramatically {
    
    self.lblText = [[NIAttributedLabel alloc] initWithFrame:CGRectZero];
    [_lblText setDelegate:self];
    _lblText.numberOfLines = 0;
    _lblText.lineBreakMode = NSLineBreakByWordWrapping;
    _lblText.font = [UIFont systemFontOfSize:15];
    [_lblText setBackgroundColor:[UIColor clearColor]];
    [_lblText setTextColor:[UIColor whiteColor]];
    //self.label.autoDetectLinks = YES;
    //_lblText.dataDetectorTypes = NSTextCheckingAllSystemTypes;
    
    _lblText.textAlignment = NSTextAlignmentJustified;
    
    // When we enable defering the link detection is offloaded to a separate thread. This allows
    // the label to display its text immediately and redraw itself once the links have been
    // detected. For the block of text below this ends up displaying the text ~300ms faster on an
    // iPhone 4S than it otherwise would have.
    _lblText.deferLinkDetection = YES;
    
//    if ([[[UIDevice currentDevice] systemVersion] doubleValue] < 7.0) {
//        _scrollView = [[UIScrollView alloc] initWithFrame:CGRectMake(0, 50, 320, self.view.frame.size.height-55)];
//    }
//    else {
//        _scrollView = [[UIScrollView alloc] initWithFrame:CGRectMake(0, 70, 320, self.view.frame.size.height-75)];
//    }
    
    CGRect rectScroll = CGRectZero;
    rectScroll.size.width = self.view.frame.size.width;
    
    if ([[[UIDevice currentDevice] systemVersion] doubleValue] < 7.0) {
        if ([[UIDevice currentDevice] userInterfaceIdiom] == UIUserInterfaceIdiomPad) {
            _lblText.font = [UIFont systemFontOfSize:20];
            rectScroll.origin.y = 100;
        }
        else {
            rectScroll.origin.y = 50;
        }
        //_scrollView = [[UIScrollView alloc] initWithFrame:CGRectMake(0, 45, 320, self.view.frame.size.height-self.tabBarController.tabBar.frame.size.height-45)];
    }
    else {
        if ([[UIDevice currentDevice] userInterfaceIdiom] == UIUserInterfaceIdiomPad) {
            _lblText.font = [UIFont systemFontOfSize:20];
            rectScroll.origin.y = 120;
        }
        else {
            rectScroll.origin.y = 70;
        }
        
        //_scrollView = [[UIScrollView alloc] initWithFrame:CGRectMake(0, 65, 320, self.view.frame.size.height-self.tabBarController.tabBar.frame.size.height-65)];
    }
    rectScroll.size.height = self.view.frame.size.height-self.tabBarController.tabBar.frame.size.height-rectScroll.origin.y-5;
    
    _scrollView = [[UIScrollView alloc] initWithFrame:rectScroll];

    
    self.scrollView.autoresizingMask = UIViewAutoresizingFlexibleDimensions;
    self.scrollView.contentInset = UIEdgeInsetsMake(20, 0, 20, 0);
    [self.scrollView addSubview:_lblText];
    [self.view addSubview:self.scrollView];
    
    [self setAllViewsAndButtons];
    
    [self setLabelFrame];
    
}

- (void)setAllViewsAndButtons {

    self.imageViewTop = [[UIImageView alloc] initWithFrame:CGRectMake(0, 0, 320, 200)];
    [_imageViewTop setContentMode:UIViewContentModeScaleAspectFit];
    [_scrollView addSubview:_imageViewTop];
    [self setTopPhoto];
    
    UIView *view = [[UIView alloc] initWithFrame:CGRectMake(0, 200, 320, 75)];
    [view setBackgroundColor:[UIColor clearColor]];
    [_scrollView addSubview:view];
    
    UIImageView *imageViewBar = [[UIImageView alloc] initWithFrame:CGRectMake(0, 0, 320, 75)];
    [imageViewBar setContentMode:UIViewContentModeScaleAspectFit];
    [imageViewBar setImage:[UIImage imageNamed:@"audio-player-bg.png"]];
    [view addSubview:imageViewBar];
    
    slider = [[UISlider alloc] initWithFrame:CGRectMake(10, 8, 300, 23)];
    [slider setMaximumTrackImage:[UIImage imageNamed:@"outer-volume-bar.png"] forState:UIControlStateNormal];
    [slider setMinimumTrackImage:[UIImage imageNamed:@"inner-volume-bar.png"] forState:UIControlStateNormal];
    [slider setThumbImage:[UIImage imageNamed:@"thumb-volume-control.png"] forState:UIControlStateNormal];
    [slider addTarget:self action:@selector(sliderSlided:) forControlEvents:UIControlEventValueChanged];
    [view addSubview:slider];
    
    btnPlayPause = [UIButton buttonWithType:UIButtonTypeCustom];
    [btnPlayPause setFrame:CGRectMake(145, 29, 30, 30)];
    [btnPlayPause setImage:[UIImage imageNamed:@"btn-play.png"] forState:UIControlStateNormal];
    [btnPlayPause setImage:[UIImage imageNamed:@"btn-pause.png"] forState:UIControlStateSelected];
    [btnPlayPause addTarget:self action:@selector(playBtnPressed:) forControlEvents:UIControlEventTouchUpInside];
    [view addSubview:btnPlayPause];
    
    UIButton *btnVolume = [UIButton buttonWithType:UIButtonTypeCustom];
    [btnVolume setFrame:CGRectMake(280, 29, 30, 30)];
    [btnVolume setImage:[UIImage imageNamed:@"btn-audio-volume-on.png"] forState:UIControlStateNormal];
    [btnVolume setImage:[UIImage imageNamed:@"btn-audio-volume-off.png"] forState:UIControlStateSelected];
    [btnVolume addTarget:self action:@selector(volumeBtnPressed:) forControlEvents:UIControlEventTouchUpInside];
    [view addSubview:btnVolume];
    
    if ([[UIDevice currentDevice] userInterfaceIdiom] == UIUserInterfaceIdiomPad) {
        [self.imageViewTop setFrame:CGRectMake(0, 0, self.view.frame.size.width, 250)];
        [view setFrame:CGRectMake(0, 250, self.view.frame.size.width, 150)];
        [imageViewBar setFrame:CGRectMake(0, 0, self.view.frame.size.width, 150)];
        
        [slider setFrame:       CGRectMake(30, 22, 708, 34)];
        [btnPlayPause setFrame: CGRectMake(355, 72, 58, 58)];
        [btnVolume setFrame:    CGRectMake(650, 72, 58, 58)];
        
    }

}

- (void)setTopPhoto {
    
    NSDictionary *dic = _arrAudioFile[_selectedIndex];
    NSString *imageName = [dic objectForKey:@"mapImage"];
    UIImage *image = [UIImage imageNamed:imageName];
    [_imageViewTop setImage: image ? image : [UIImage imageNamed:@"no-photo.png"]];
}

- (void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
    
}

- (void)viewWillDisappear:(BOOL)animated {
    [super viewWillDisappear:animated];
}

- (UIStatusBarStyle)preferredStatusBarStyle
{
    return UIStatusBarStyleLightContent;
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (IBAction)backBtnPressed:(id)sender {
    
    [self audioStopped];
    [self.navigationController popViewControllerAnimated:YES];
}

- (IBAction)fullscreenBtnPressed:(id)sender {
    
}

- (IBAction)previousBtnPressed:(id)sender {
    
    if (_selectedIndex > 0) {
        
        [[[Singleton sharedSingleton] audioPlayer] stop];
        _selectedIndex -= 1;
        [self playBtnPressed:btnPlayPause];
    }
}

- (IBAction)playBtnPressed:(id)sender {
    
    if (![self canPlay]) {
        
        SIAlertView *alertView = [[SIAlertView alloc] initWithTitle:nil andMessage:@"Audio is already Running"];
        [alertView addButtonWithTitle:@"OK"
                                 type:SIAlertViewButtonTypeDefault
                              handler:^(SIAlertView *alertView) {
                              }];
        
        [alertView show];
        
        return;
    }
    [self setTopPhoto];
    UIButton *btn = (UIButton *)sender;
    
    if ([[Singleton sharedSingleton] audioPlayer] && [[[Singleton sharedSingleton] audioPlayer] isPlaying]) {
        [btn setSelected:NO];
        [[[Singleton sharedSingleton] audioPlayer] pause];
        isPause = YES;
    }
    else if ([[Singleton sharedSingleton] audioPlayer] && isPause) {
        [btn setSelected:YES];
        isPause = NO;
        [[[Singleton sharedSingleton] audioPlayer] play];
    }
    else {
        [btn setSelected:YES];
        isPause = NO;
        [slider setMinimumValue:0.0];
        [slider setValue:0.0];
        
        NSString *strResourchPath = [[NSBundle mainBundle] resourcePath];
        
        [self setLabelFrame];
        
        NSURL *url = [NSURL fileURLWithPath:[NSString stringWithFormat:@"%@/%@", strResourchPath, [_arrAudioFile[_selectedIndex] objectForKey:@"fileName"]]];
        
        NSError *error;
        [Singleton sharedSingleton].audioPlayer = [[AVAudioPlayer alloc] initWithContentsOfURL:url error:&error];
        //_audioPlayer.numberOfLoops = -1;
        [[[Singleton sharedSingleton] audioPlayer] setDelegate:self];
        if (isVolumeOn) {
            [[[Singleton sharedSingleton] audioPlayer] setVolume:1.0];
        }
        else {
            [[[Singleton sharedSingleton] audioPlayer] setVolume:0.0];
        }
        if ([[Singleton sharedSingleton] audioPlayer] == nil)
            NSLog(@"%@", [error description]);
        else
            [[[Singleton sharedSingleton] audioPlayer] play];
        updateTimer = [NSTimer scheduledTimerWithTimeInterval:.01 target:self selector:@selector(updateCurrentTime) userInfo:[[Singleton sharedSingleton] audioPlayer] repeats:YES];
        
        [slider setMaximumValue:[[[Singleton sharedSingleton] audioPlayer] duration]];
        
    }
}


- (BOOL)canPlay {
    
    if ([[Singleton sharedSingleton] hAudioRunning] || [[Singleton sharedSingleton] wAudioRunning] || [[Singleton sharedSingleton] tAudioRunning] || [[Singleton sharedSingleton] aAudioRunning] ) {
        return NO;
    }
    return YES;
}

- (void)audioStopped {
    
    [updateTimer invalidate];
    [[[Singleton sharedSingleton] audioPlayer] stop];
    [Singleton sharedSingleton].audioPlayer = nil;
}

- (IBAction)nextBtnPressed:(id)sender {
    
    if ([_arrAudioFile count] > _selectedIndex+1) {
        isPause = NO;
        [[[Singleton sharedSingleton] audioPlayer] stop];
        _selectedIndex += 1;
        [self playBtnPressed:btnPlayPause];
    }
}

- (IBAction)volumeBtnPressed:(id)sender {
    
    UIButton *btn = (UIButton *)sender;
    
    if (isVolumeOn) {
        [btn setSelected:YES];
        [[[Singleton sharedSingleton] audioPlayer] setVolume:0.0];
    }
    else {
        [btn setSelected:NO];
        [[[Singleton sharedSingleton] audioPlayer] setVolume:1.0];
    }
    isVolumeOn = !isVolumeOn;
    
}

- (IBAction)sliderSlided:(id)sender {
    
    [[[Singleton sharedSingleton] audioPlayer] setCurrentTime:slider.value];
    
    //_audioPlayer set
}

- (void)updateCurrentTime
{
	[self updateCurrentTimeForPlayer:[[Singleton sharedSingleton] audioPlayer]];
}

-(void)updateCurrentTimeForPlayer:(AVAudioPlayer *)p
{
	//currentTime.text = [NSString stringWithFormat:@"%d:%02d", (int)p.currentTime / 60, (int)p.currentTime % 60, nil];
	slider.value = p.currentTime;
}

- (void)_layoutLabel {
    if ([[UIDevice currentDevice] userInterfaceIdiom] == UIUserInterfaceIdiomPad) {
        CGSize size = [_lblText sizeThatFits:CGSizeMake(718, CGFLOAT_MAX)];
        _lblText.frame = CGRectMake(25, 420, 718, size.height);
        _scrollView.contentSize = CGSizeMake(748, size.height+420);
    }
    else {
        CGSize size = [_lblText sizeThatFits:CGSizeMake(290, CGFLOAT_MAX)];
        _lblText.frame = CGRectMake(15, 280, 290, size.height);
        _scrollView.contentSize = CGSizeMake(300, size.height+280);
    }
}

- (void)setLabelFrame {
    
    //    UIFont *font = [UIFont boldSystemFontOfSize:15];
    //NSString *str = [[_arrAudioFile[_selectedIndex] objectForKey:@"imageLink"] objectAtIndex:0];
    
    [_lblHeading setText:[_arrAudioFile[_selectedIndex] objectForKey:@"heading"]];
    
    [_lblText setText:[_arrAudioFile[_selectedIndex] objectForKey:@"text"]];
    
    NSArray *array = [_arrAudioFile[_selectedIndex] objectForKey:@"imageLink"];
    
    for (NSDictionary *aDic in array) {
        
        NSString *imageName = [NSString stringWithFormat:@"____%@",[aDic objectForKey:@"title"]];
        NSString *combineImageName = [[aDic objectForKey:@"imageName"] stringByAppendingString:imageName];
        NSString *data = [aDic objectForKey:@"data"];
        
        if (data != nil) {
            
            [_lblText addLink:[NSURL fileURLWithPath:combineImageName] range:[_lblText.text rangeOfString:data]];
        }
    }
    
    [self _layoutLabel];
    
    //    CGRect labelFrame = _lblText.frame;
    //    labelFrame.size = [_lblText.text sizeWithFont:_lblText.font constrainedToSize:CGSizeMake(300, CGFLOAT_MAX) lineBreakMode:_lblText.lineBreakMode];
    //    labelFrame.origin = CGPointMake(0, 0);
    //    _lblText.frame = labelFrame;
    //    [_lblText sizeToFit];
    //    [_scrollView setContentOffset:CGPointMake(0, 0)];
    //    [_scrollView setContentSize:CGSizeMake(300, labelFrame.size.height)];
}


#pragma mark - NIAttributedLabelDelegate

- (void)attributedLabel:(NIAttributedLabel *)attributedLabel didSelectTextCheckingResult:(NSTextCheckingResult *)result atPoint:(CGPoint)point {
    
    NSString *strImageData = [result.URL.relativeString stringByReplacingPercentEscapesUsingEncoding:NSUTF8StringEncoding];
    
    NSArray *arrImageData = [strImageData componentsSeparatedByString:@"____"];
    
    if ([strImageData length]) {
        
        SelectedImageViewController *selectedImageViewController = [self.storyboard instantiateViewControllerWithIdentifier:@"SelectedImageViewController"];
        [selectedImageViewController setStrImages:arrImageData[0]];
        [selectedImageViewController setStrHeading:arrImageData[1]];
        [self.navigationController pushViewController:selectedImageViewController animated:YES];
    }
    else {
        
        SIAlertView *alertView = [[SIAlertView alloc] initWithTitle:nil andMessage:@"Image not found"];
        [alertView addButtonWithTitle:@"OK"
                                 type:SIAlertViewButtonTypeDefault
                              handler:^(SIAlertView *alertView) {
                              }];
        
        [alertView show];
    }
}

#pragma mark --
#pragma mark -- AVAudioPlayer Delegates

- (void)audioPlayerDidFinishPlaying:(AVAudioPlayer *)player successfully:(BOOL)flag {
    
    [btnPlayPause setSelected:NO];
    [self audioStopped];
}

- (void)audioPlayerDecodeErrorDidOccur:(AVAudioPlayer *)player error:(NSError *)error {
    
    [btnPlayPause setSelected:NO];
    [self audioStopped];
}

- (IBAction)cameraBtnPressed:(id)sender {
    
    SIAlertView *alertView = [[SIAlertView alloc] initWithTitle:nil andMessage:@"Choose type"];
    [alertView addButtonWithTitle:@"Take Picture"
                             type:SIAlertViewButtonTypeDefault
                          handler:^(SIAlertView *alertView) {
                              
                              PickerViewController *pickerViewController = [self.storyboard instantiateViewControllerWithIdentifier:@"PickerViewController"];
                              [pickerViewController setIsMediaTypeMovie:NO];
                              [self presentViewController:pickerViewController animated:YES completion:nil];
                          }];
    [alertView addButtonWithTitle:@"Make Video"
                             type:SIAlertViewButtonTypeDefault
                          handler:^(SIAlertView *alertView) {
                              
                              PickerViewController *pickerViewController = [self.storyboard instantiateViewControllerWithIdentifier:@"PickerViewController"];
                              [pickerViewController setIsMediaTypeMovie:YES];
                              [self presentViewController:pickerViewController animated:YES completion:nil];
                          }];
    [alertView addButtonWithTitle:@"Cancel"
                             type:SIAlertViewButtonTypeCancel
                          handler:^(SIAlertView *alertView) {
                          }];
    
    [alertView show];
}

@end
